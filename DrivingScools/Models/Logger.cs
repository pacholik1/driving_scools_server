﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace DrivingScools.Models
{
    public static class Logger
    {
        private static string filename = AppDomain.CurrentDomain.BaseDirectory + "log";

        public static void Log(string message)
        {
            FileStream fs = new FileStream(filename, FileMode.Append);
            StreamWriter sw = new StreamWriter(fs);
            sw.WriteLine(DateTime.Now.ToString() + " " + message);
            sw.Close();
            fs.Close();
        }
    }
}