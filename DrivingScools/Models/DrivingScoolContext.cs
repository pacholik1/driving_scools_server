#region Auto-generated classes for drivinscools database on 2013-11-25 21:46:49Z

//
//  ____  _     __  __      _        _
// |  _ \| |__ |  \/  | ___| |_ __ _| |
// | | | | '_ \| |\/| |/ _ \ __/ _` | |
// | |_| | |_) | |  | |  __/ || (_| | |
// |____/|_.__/|_|  |_|\___|\__\__,_|_|
//
// Auto-generated from drivinscools on 2013-11-25 21:46:49Z
// Please visit http://linq.to/db for more information

#endregion

using System;
using System.Data;
using System.Data.Linq.Mapping;
using System.Diagnostics;
using System.Reflection;
using DbLinq.Data.Linq;
using DbLinq.Vendor;
using System.ComponentModel;

namespace DrivingScools.Models
{    
	public partial class DrivingScoolContext : DataContext
	{
		public DrivingScoolContext(IDbConnection connection)
		: base(connection, new DbLinq.MySql.MySqlVendor())
		{
		}

		public DrivingScoolContext(IDbConnection connection, IVendor vendor)
		: base(connection, vendor)
		{
		}

		public Table<Autoskoly> Autoskoly { get { return GetTable<Autoskoly>(); } }
		public Table<Dochazka> Dochazka { get { return GetTable<Dochazka>(); } }
		public Table<DokumentyUcitelu> DokumentyUcitelu { get { return GetTable<DokumentyUcitelu>(); } }
		public Table<DokumentyVozidel> DokumentyVozidel { get { return GetTable<DokumentyVozidel>(); } }
		public Table<Jizdy> Jizdy { get { return GetTable<Jizdy>(); } }
		public Table<Kurzy> Kurzy { get { return GetTable<Kurzy>(); } }		
		public Table<Role> Role { get { return GetTable<Role>(); } }
		public Table<StavyKurzu> StavuKurzu { get { return GetTable<StavyKurzu>(); } }
		public Table<Studenti> Studenti { get { return GetTable<Studenti>(); } }
		public Table<Teorie> Teorie { get { return GetTable<Teorie>(); } }
		public Table<TypyKurzu> TypyKurzu { get { return GetTable<TypyKurzu>(); } }
		public Table<TypyTeorie> TypyTeorie { get { return GetTable<TypyTeorie>(); } }
		public Table<TypyVozidel> TypyVozidel { get { return GetTable<TypyVozidel>(); } }
		public Table<Ucitele> Ucitele { get { return GetTable<Ucitele>(); } }
		public Table<Uzivatele> Uzivatele { get { return GetTable<Uzivatele>(); } }
		public Table<Vozidla> Vozidla { get { return GetTable<Vozidla>(); } }
		public Table<Vyplatnice> Vyplatnice { get { return GetTable<Vyplatnice>(); } }

	}

	[Table(Name = "drivinscools.autoskoly")]
	public partial class Autoskoly : INotifyPropertyChanged
	{
		#region INotifyPropertyChanged handling

		public event PropertyChangedEventHandler PropertyChanged;

		protected virtual void OnPropertyChanged(string propertyName)
		{
			if (PropertyChanged != null)
			{
				PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
			}
		}

		#endregion

		#region int AdReSaCP

		private int _adReSaCp;
		[DebuggerNonUserCode]
		[Column(Storage = "_adReSaCp", Name = "adresa_cp", DbType = "int", CanBeNull = false)]
		public int AdresaCP
		{
			get
			{
				return _adReSaCp;
			}
			set
			{
				if (value != _adReSaCp)
				{
					_adReSaCp = value;
					OnPropertyChanged("AdresaCP");
				}
			}
		}

		#endregion

		#region string AdReSaMEstO

		private string _adReSaMeStO;
		[DebuggerNonUserCode]
		[Column(Storage = "_adReSaMeStO", Name = "adresa_mesto", DbType = "varchar(33)", CanBeNull = false)]
		public string AdresaMesto
		{
			get
			{
				return _adReSaMeStO;
			}
			set
			{
				if (value != _adReSaMeStO)
				{
					_adReSaMeStO = value;
                    OnPropertyChanged("AdresaMesto");
				}
			}
		}

		#endregion

		#region int AdReSaPsC

		private int _adReSaPsC;
		[DebuggerNonUserCode]
		[Column(Storage = "_adReSaPsC", Name = "adresa_psc", DbType = "int", CanBeNull = false)]
		public int AdresaPSC
		{
			get
			{
				return _adReSaPsC;
			}
			set
			{
				if (value != _adReSaPsC)
				{
					_adReSaPsC = value;
                    OnPropertyChanged("AdresaPSC");
				}
			}
		}

		#endregion

		#region string AdReSaUlIce

		private string _adReSaUlIce;
		[DebuggerNonUserCode]
		[Column(Storage = "_adReSaUlIce", Name = "adresa_ulice", DbType = "varchar(37)", CanBeNull = false)]
		public string AdresaUlice
		{
			get
			{
				return _adReSaUlIce;
			}
			set
			{
				if (value != _adReSaUlIce)
				{
					_adReSaUlIce = value;
                    OnPropertyChanged("AdresaUlice");
				}
			}
		}

		#endregion

		#region uint AutosKolaID

		private uint _autosKolaID;
		[DebuggerNonUserCode]
		[Column(Storage = "_autosKolaID", Name = "autoskola_id", DbType = "int unsigned", IsPrimaryKey = true, IsDbGenerated = true, CanBeNull = false)]
		public uint AutoskolaID
		{
			get
			{
				return _autosKolaID;
			}
			set
			{
				if (value != _autosKolaID)
				{
					_autosKolaID = value;
                    OnPropertyChanged("AutoskolaID");
				}
			}
		}

		#endregion

		#region string NAzEV

		private string _naZEv;
		[DebuggerNonUserCode]
		[Column(Storage = "_naZEv", Name = "nazev", DbType = "varchar(30)", CanBeNull = false)]
		public string Nazev
		{
			get
			{
				return _naZEv;
			}
			set
			{
				if (value != _naZEv)
				{
					_naZEv = value;
                    OnPropertyChanged("Nazev");
				}
			}
		}

		#endregion

		#region uint UziVatELID

		private uint _uziVatElid;
		[DebuggerNonUserCode]
		[Column(Storage = "_uziVatElid", Name = "uzivatel_id", DbType = "int unsigned", CanBeNull = false)]
		public uint UzivatelID
		{
			get
			{
				return _uziVatElid;
			}
			set
			{
				if (value != _uziVatElid)
				{
					_uziVatElid = value;
                    OnPropertyChanged("UzivatelID");
				}
			}
		}

		#endregion

		#region Children

		private EntitySet<Kurzy> _kuRzy;
		[Association(Storage = "_kuRzy", OtherKey = "AutoskolaID", Name = "KURZY_AUTOSKOLY_FK")]
		[DebuggerNonUserCode]
		public EntitySet<Kurzy> Kurzy
		{
			get
			{
				return _kuRzy;
			}
			set
			{
				_kuRzy = value;
			}
		}

		private EntitySet<Ucitele> _ucIteLe;
		[Association(Storage = "_ucIteLe", OtherKey = "AutoskolaID", Name = "UCITELE_AUTOSKOLY_FK")]
		[DebuggerNonUserCode]
		public EntitySet<Ucitele> Ucitele
		{
			get
			{
				return _ucIteLe;
			}
			set
			{
				_ucIteLe = value;
			}
		}

		private EntitySet<Vozidla> _voZIdlA;
		[Association(Storage = "_voZIdlA", OtherKey = "AutoskolaID", Name = "VOZIDLA_AUTOSKOLY_FK")]
		[DebuggerNonUserCode]
		public EntitySet<Vozidla> Vozidla
		{
			get
			{
				return _voZIdlA;
			}
			set
			{
				_voZIdlA = value;
			}
		}


		#endregion

		#region Parents

		private EntityRef<Uzivatele> _uziVatEle;
		[Association(Storage = "_uziVatEle", ThisKey = "UzivatelID", Name = "AUTOSKOLA_UZIVATELE_FK", IsForeignKey = true)]
		[DebuggerNonUserCode]
		public Uzivatele Uzivatele
		{
			get
			{
				return _uziVatEle.Entity;
			}
			set
			{
				if (value != _uziVatEle.Entity)
				{
					if (_uziVatEle.Entity != null)
					{
						var previousUziVatELE = _uziVatEle.Entity;
						_uziVatEle.Entity = null;
						previousUziVatELE.Autoskoly.Remove(this);
					}
					_uziVatEle.Entity = value;
					if (value != null)
					{
						value.Autoskoly.Add(this);
						_uziVatElid = value.UzivatelID;
					}
					else
					{
						_uziVatElid = default(uint);
					}
				}
			}
		}


		#endregion

		#region Attachement handlers

		private void KuRZY_Attach(Kurzy entity)
		{
			entity.Autoskoly = this;
		}

		private void KuRZY_Detach(Kurzy entity)
		{
			entity.Autoskoly = null;
		}

		private void UCiteLE_Attach(Ucitele entity)
		{
			entity.Autoskoly = this;
		}

		private void UCiteLE_Detach(Ucitele entity)
		{
			entity.Autoskoly = null;
		}

		private void VOzIDLa_Attach(Vozidla entity)
		{
			entity.Autoskoly = this;
		}

		private void VOzIDLa_Detach(Vozidla entity)
		{
			entity.Autoskoly = null;
		}


		#endregion

		#region ctor

		public Autoskoly()
		{
			_kuRzy = new EntitySet<Kurzy>(KuRZY_Attach, KuRZY_Detach);
			_ucIteLe = new EntitySet<Ucitele>(UCiteLE_Attach, UCiteLE_Detach);
			_voZIdlA = new EntitySet<Vozidla>(VOzIDLa_Attach, VOzIDLa_Detach);
			_uziVatEle = new EntityRef<Uzivatele>();
		}

		#endregion

	}

	[Table(Name = "drivinscools.dochazka")]
	public partial class Dochazka : INotifyPropertyChanged
	{
		#region INotifyPropertyChanged handling

		public event PropertyChangedEventHandler PropertyChanged;

		protected virtual void OnPropertyChanged(string propertyName)
		{
			if (PropertyChanged != null)
			{
				PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
			}
		}

		#endregion

		#region uint StudentID

		private uint _studentID;
		[DebuggerNonUserCode]
		[Column(Storage = "_studentID", Name = "student_id", DbType = "int unsigned", IsPrimaryKey = true, CanBeNull = false)]
		public uint StudentID
		{
			get
			{
				return _studentID;
			}
			set
			{
				if (value != _studentID)
				{
					_studentID = value;
					OnPropertyChanged("StudentID");
				}
			}
		}

		#endregion

		#region uint TeOrIeID

		private uint _teOrIeID;
		[DebuggerNonUserCode]
		[Column(Storage = "_teOrIeID", Name = "teorie_id", DbType = "int unsigned", IsPrimaryKey = true, CanBeNull = false)]
		public uint TeorieID
		{
			get
			{
				return _teOrIeID;
			}
			set
			{
				if (value != _teOrIeID)
				{
					_teOrIeID = value;
                    OnPropertyChanged("TeorieID");
				}
			}
		}

		#endregion

		#region Parents

		private EntityRef<Studenti> _studentI;
		[Association(Storage = "_studentI", ThisKey = "StudentID", Name = "DOCHAZKA_STUDENTI_FK", IsForeignKey = true)]
		[DebuggerNonUserCode]
		public Studenti Studenti
		{
			get
			{
				return _studentI.Entity;
			}
			set
			{
				if (value != _studentI.Entity)
				{
					if (_studentI.Entity != null)
					{
						var previousStudentI = _studentI.Entity;
						_studentI.Entity = null;
						previousStudentI.Dochazka.Remove(this);
					}
					_studentI.Entity = value;
					if (value != null)
					{
						value.Dochazka.Add(this);
						_studentID = value.StudentID;
					}
					else
					{
						_studentID = default(uint);
					}
				}
			}
		}

		private EntityRef<Teorie> _teOrIe;
		[Association(Storage = "_teOrIe", ThisKey = "TeorieID", Name = "DOCHAZKA_TEORIE_FK", IsForeignKey = true)]
		[DebuggerNonUserCode]
		public Teorie Teorie
		{
			get
			{
				return _teOrIe.Entity;
			}
			set
			{
				if (value != _teOrIe.Entity)
				{
					if (_teOrIe.Entity != null)
					{
						var previousTeOrIe = _teOrIe.Entity;
						_teOrIe.Entity = null;
						previousTeOrIe.Dochazka.Remove(this);
					}
					_teOrIe.Entity = value;
					if (value != null)
					{
						value.Dochazka.Add(this);
						_teOrIeID = value.TeorieID;
					}
					else
					{
						_teOrIeID = default(uint);
					}
				}
			}
		}


		#endregion

		#region ctor

		public Dochazka()
		{
			_studentI = new EntityRef<Studenti>();
			_teOrIe = new EntityRef<Teorie>();
		}

		#endregion

	}

	[Table(Name = "drivinscools.dokumenty_ucitelu")]
	public partial class DokumentyUcitelu : INotifyPropertyChanged
	{
		#region INotifyPropertyChanged handling

		public event PropertyChangedEventHandler PropertyChanged;

		protected virtual void OnPropertyChanged(string propertyName)
		{
			if (PropertyChanged != null)
			{
				PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
			}
		}

		#endregion

		#region DateTime DatumVLoZenI

		private DateTime _datumVlOZenI;
		[DebuggerNonUserCode]
		[Column(Storage = "_datumVlOZenI", Name = "datum_vlozeni", DbType = "date", CanBeNull = false)]
		public DateTime DatumVlozeni
		{
			get
			{
				return _datumVlOZenI;
			}
			set
			{
				if (value != _datumVlOZenI)
				{
					_datumVlOZenI = value;
                    OnPropertyChanged("DatumVlozeni");
				}
			}
		}

		#endregion

		#region Byte[] DoKuMenT

		private Byte[] _doKuMenT;
		[DebuggerNonUserCode]
		[Column(Storage = "_doKuMenT", Name = "dokument", DbType = "longblob", CanBeNull = false)]
		public Byte[] Dokument
		{
			get
			{
				return _doKuMenT;
			}
			set
			{
				if (value != _doKuMenT)
				{
					_doKuMenT = value;
					OnPropertyChanged("Dokument");
				}
			}
		}

		#endregion

		#region uint DoKuMenTID

		private uint _doKuMenTid;
		[DebuggerNonUserCode]
		[Column(Storage = "_doKuMenTid", Name = "dokument_id", DbType = "int unsigned", IsPrimaryKey = true, IsDbGenerated = true, CanBeNull = false)]
		public uint DokumentID
		{
			get
			{
				return _doKuMenTid;
			}
			set
			{
				if (value != _doKuMenTid)
				{
					_doKuMenTid = value;
                    OnPropertyChanged("DokumentID");
				}
			}
		}

		#endregion

		#region string DoKuMenTNAzEV

		private string _doKuMenTnaZEv;
		[DebuggerNonUserCode]
		[Column(Storage = "_doKuMenTnaZEv", Name = "dokument_nazev", DbType = "varchar(20)", CanBeNull = false)]
		public string DokumentNazev
		{
			get
			{
				return _doKuMenTnaZEv;
			}
			set
			{
				if (value != _doKuMenTnaZEv)
				{
					_doKuMenTnaZEv = value;
                    OnPropertyChanged("DokumentNazev");
				}
			}
		}

		#endregion

		#region uint UCiteLID

		private uint _ucIteLid;
		[DebuggerNonUserCode]
		[Column(Storage = "_ucIteLid", Name = "ucitel_id", DbType = "int unsigned", CanBeNull = false)]
		public uint UcitelID
		{
			get
			{
				return _ucIteLid;
			}
			set
			{
				if (value != _ucIteLid)
				{
					_ucIteLid = value;
                    OnPropertyChanged("UcitelID");
				}
			}
		}

		#endregion

		#region Parents

		private EntityRef<Ucitele> _ucIteLe;
		[Association(Storage = "_ucIteLe", ThisKey = "UcitelID", Name = "UCITELE_DOKUMENTY_UCITELU_FK", IsForeignKey = true)]
		[DebuggerNonUserCode]
		public Ucitele Ucitele
		{
			get
			{
				return _ucIteLe.Entity;
			}
			set
			{
				if (value != _ucIteLe.Entity)
				{
					if (_ucIteLe.Entity != null)
					{
						var previousUCiteLE = _ucIteLe.Entity;
						_ucIteLe.Entity = null;
						previousUCiteLE.DokumentyUcitelu.Remove(this);
					}
					_ucIteLe.Entity = value;
					if (value != null)
					{
						value.DokumentyUcitelu.Add(this);
						_ucIteLid = value.UcitelID;
					}
					else
					{
						_ucIteLid = default(uint);
					}
				}
			}
		}


		#endregion

		#region ctor

		public DokumentyUcitelu()
		{
			_ucIteLe = new EntityRef<Ucitele>();
		}

		#endregion

	}

	[Table(Name = "drivinscools.dokumenty_vozidel")]
	public partial class DokumentyVozidel : INotifyPropertyChanged
	{
		#region INotifyPropertyChanged handling

		public event PropertyChangedEventHandler PropertyChanged;

		protected virtual void OnPropertyChanged(string propertyName)
		{
			if (PropertyChanged != null)
			{
				PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
			}
		}

		#endregion

		#region DateTime DatumVLoZenI

		private DateTime _datumVlOZenI;
		[DebuggerNonUserCode]
		[Column(Storage = "_datumVlOZenI", Name = "datum_vlozeni", DbType = "datetime", CanBeNull = false)]
		public DateTime DatumVlozeni
		{
			get
			{
				return _datumVlOZenI;
			}
			set
			{
				if (value != _datumVlOZenI)
				{
					_datumVlOZenI = value;
                    OnPropertyChanged("DatumVlozeni");
				}
			}
		}

		#endregion

		#region Byte[] DoKuMenT

		private Byte[] _doKuMenT;
		[DebuggerNonUserCode]
		[Column(Storage = "_doKuMenT", Name = "dokument", DbType = "longblob", CanBeNull = false)]
		public Byte[] Dokument
		{
			get
			{
				return _doKuMenT;
			}
			set
			{
				if (value != _doKuMenT)
				{
					_doKuMenT = value;
                    OnPropertyChanged("Dokument");
				}
			}
		}

		#endregion

		#region uint DoKuMenTID

		private uint _doKuMenTid;
		[DebuggerNonUserCode]
		[Column(Storage = "_doKuMenTid", Name = "dokument_id", DbType = "int unsigned", IsPrimaryKey = true, IsDbGenerated = true, CanBeNull = false)]
		public uint DokumentID
		{
			get
			{
				return _doKuMenTid;
			}
			set
			{
				if (value != _doKuMenTid)
				{
					_doKuMenTid = value;
                    OnPropertyChanged("DokumentID");
				}
			}
		}

		#endregion

		#region string DoKuMenTNAzEV

		private string _doKuMenTnaZEv;
		[DebuggerNonUserCode]
		[Column(Storage = "_doKuMenTnaZEv", Name = "dokument_nazev", DbType = "varchar(20)", CanBeNull = false)]
		public string DokumentNazev
		{
			get
			{
				return _doKuMenTnaZEv;
			}
			set
			{
				if (value != _doKuMenTnaZEv)
				{
					_doKuMenTnaZEv = value;
                    OnPropertyChanged("DokumentNazev");
				}
			}
		}

		#endregion

		#region uint VOzIDLoID

		private uint _voZIdlOID;
		[DebuggerNonUserCode]
		[Column(Storage = "_voZIdlOID", Name = "vozidlo_id", DbType = "int unsigned", CanBeNull = false)]
		public uint VozidloID
		{
			get
			{
				return _voZIdlOID;
			}
			set
			{
				if (value != _voZIdlOID)
				{
					_voZIdlOID = value;
                    OnPropertyChanged("VozidloID");
				}
			}
		}

		#endregion

		#region Parents

		private EntityRef<Vozidla> _voZIdlA;
		[Association(Storage = "_voZIdlA", ThisKey = "VozidloID", Name = "DOKUMENTY_VOZIDEL_VOZIDLA_FK", IsForeignKey = true)]
		[DebuggerNonUserCode]
		public Vozidla Vozidla
		{
			get
			{
				return _voZIdlA.Entity;
			}
			set
			{
				if (value != _voZIdlA.Entity)
				{
					if (_voZIdlA.Entity != null)
					{
						var previousVOzIDLa = _voZIdlA.Entity;
						_voZIdlA.Entity = null;
						previousVOzIDLa.DokumentyVozidel.Remove(this);
					}
					_voZIdlA.Entity = value;
					if (value != null)
					{
						value.DokumentyVozidel.Add(this);
						_voZIdlOID = value.VozidloID;
					}
					else
					{
						_voZIdlOID = default(uint);
					}
				}
			}
		}


		#endregion

		#region ctor

		public DokumentyVozidel()
		{
			_voZIdlA = new EntityRef<Vozidla>();
		}

		#endregion

	}

	[Table(Name = "drivinscools.jizdy")]
	public partial class Jizdy : INotifyPropertyChanged
	{
		#region INotifyPropertyChanged handling

		public event PropertyChangedEventHandler PropertyChanged;

		protected virtual void OnPropertyChanged(string propertyName)
		{
			if (PropertyChanged != null)
			{
				PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
			}
		}

		#endregion

		#region DateTime Do

		private DateTime _do;
		[DebuggerNonUserCode]
		[Column(Storage = "_do", Name = "do", DbType = "datetime", CanBeNull = false)]
		public DateTime Do
		{
			get
			{
				return _do;
			}
			set
			{
				if (value != _do)
				{
					_do = value;
					OnPropertyChanged("Do");
				}
			}
		}

		#endregion

		#region uint JIZDaID

		private uint _jizdAID;
		[DebuggerNonUserCode]
		[Column(Storage = "_jizdAID", Name = "jizda_id", DbType = "int unsigned", IsPrimaryKey = true, IsDbGenerated = true, CanBeNull = false)]
		public uint JizdaID
		{
			get
			{
				return _jizdAID;
			}
			set
			{
				if (value != _jizdAID)
				{
					_jizdAID = value;
                    OnPropertyChanged("JizdaID");
				}
			}
		}

		#endregion

		#region uint KuRZID

		private uint _kuRzid;
		[DebuggerNonUserCode]
		[Column(Storage = "_kuRzid", Name = "kurz_id", DbType = "int unsigned", CanBeNull = false)]
		public uint KurzID
		{
			get
			{
				return _kuRzid;
			}
			set
			{
				if (value != _kuRzid)
				{
					_kuRzid = value;
					OnPropertyChanged("KurzID");
				}
			}
		}

		#endregion

		#region double NAJetO

		private double _najEtO;
		[DebuggerNonUserCode]
		[Column(Storage = "_najEtO", Name = "najeto", DbType = "double", CanBeNull = false)]
		public double Najeto
		{
			get
			{
				return _najEtO;
			}
			set
			{
				if (value != _najEtO)
				{
					_najEtO = value;
                    OnPropertyChanged("Najeto");
				}
			}
		}

		#endregion

		#region DateTime Od

		private DateTime _od;
		[DebuggerNonUserCode]
		[Column(Storage = "_od", Name = "od", DbType = "datetime", CanBeNull = false)]
		public DateTime Od
		{
			get
			{
				return _od;
			}
			set
			{
				if (value != _od)
				{
					_od = value;
					OnPropertyChanged("Od");
				}
			}
		}

		#endregion

		#region double SpOtReBa

		private double _spOtReBa;
		[DebuggerNonUserCode]
		[Column(Storage = "_spOtReBa", Name = "spotreba", DbType = "double", CanBeNull = false)]
		public double Spotreba
		{
			get
			{
				return _spOtReBa;
			}
			set
			{
				if (value != _spOtReBa)
				{
					_spOtReBa = value;
                    OnPropertyChanged("Spotreba");
				}
			}
		}

		#endregion

		#region uint StudentID

		private uint _studentID;
		[DebuggerNonUserCode]
		[Column(Storage = "_studentID", Name = "student_id", DbType = "int unsigned", CanBeNull = false)]
		public uint StudentID
		{
			get
			{
				return _studentID;
			}
			set
			{
				if (value != _studentID)
				{
					_studentID = value;
					OnPropertyChanged("StudentID");
				}
			}
		}

		#endregion

		#region uint UCiteLID

		private uint _ucIteLid;
		[DebuggerNonUserCode]
		[Column(Storage = "_ucIteLid", Name = "ucitel_id", DbType = "int unsigned", CanBeNull = false)]
		public uint UcitelID
		{
			get
			{
				return _ucIteLid;
			}
			set
			{
				if (value != _ucIteLid)
				{
					_ucIteLid = value;
                    OnPropertyChanged("UcitelID");
				}
			}
		}

		#endregion

		#region uint VOzIDLoID

		private uint _voZIdlOID;
		[DebuggerNonUserCode]
		[Column(Storage = "_voZIdlOID", Name = "vozidlo_id", DbType = "int unsigned", CanBeNull = false)]
		public uint VozidloID
		{
			get
			{
				return _voZIdlOID;
			}
			set
			{
				if (value != _voZIdlOID)
				{
					_voZIdlOID = value;
                    OnPropertyChanged("VozidloID");
				}
			}
		}

		#endregion

		#region Parents

		private EntityRef<Kurzy> _kuRzy;
		[Association(Storage = "_kuRzy", ThisKey = "KurzID", Name = "JIZDY_KURZY_FK", IsForeignKey = true)]
		[DebuggerNonUserCode]
		public Kurzy Kurzy
		{
			get
			{
				return _kuRzy.Entity;
			}
			set
			{
				if (value != _kuRzy.Entity)
				{
					if (_kuRzy.Entity != null)
					{
						var previousKuRZY = _kuRzy.Entity;
						_kuRzy.Entity = null;
						previousKuRZY.Jizdy.Remove(this);
					}
					_kuRzy.Entity = value;
					if (value != null)
					{
						value.Jizdy.Add(this);
						_kuRzid = value.KurzID;
					}
					else
					{
						_kuRzid = default(uint);
					}
				}
			}
		}

		private EntityRef<Studenti> _studentI;
		[Association(Storage = "_studentI", ThisKey = "StudentID", Name = "JIZDY_STUDENTI_FK", IsForeignKey = true)]
		[DebuggerNonUserCode]
		public Studenti Studenti
		{
			get
			{
				return _studentI.Entity;
			}
			set
			{
				if (value != _studentI.Entity)
				{
					if (_studentI.Entity != null)
					{
						var previousStudentI = _studentI.Entity;
						_studentI.Entity = null;
						previousStudentI.Jizdy.Remove(this);
					}
					_studentI.Entity = value;
					if (value != null)
					{
						value.Jizdy.Add(this);
						_studentID = value.StudentID;
					}
					else
					{
						_studentID = default(uint);
					}
				}
			}
		}

		private EntityRef<Ucitele> _ucIteLe;
		[Association(Storage = "_ucIteLe", ThisKey = "UcitelID", Name = "JIZDY_UCITELE_FK", IsForeignKey = true)]
		[DebuggerNonUserCode]
		public Ucitele Ucitele
		{
			get
			{
				return _ucIteLe.Entity;
			}
			set
			{
				if (value != _ucIteLe.Entity)
				{
					if (_ucIteLe.Entity != null)
					{
						var previousUCiteLE = _ucIteLe.Entity;
						_ucIteLe.Entity = null;
						previousUCiteLE.Jizdy.Remove(this);
					}
					_ucIteLe.Entity = value;
					if (value != null)
					{
						value.Jizdy.Add(this);
						_ucIteLid = value.UcitelID;
					}
					else
					{
						_ucIteLid = default(uint);
					}
				}
			}
		}

		private EntityRef<Vozidla> _voZIdlA;
		[Association(Storage = "_voZIdlA", ThisKey = "VozidloID", Name = "JIZDY_VOZIDLA_FK", IsForeignKey = true)]
		[DebuggerNonUserCode]
		public Vozidla Vozidla
		{
			get
			{
				return _voZIdlA.Entity;
			}
			set
			{
				if (value != _voZIdlA.Entity)
				{
					if (_voZIdlA.Entity != null)
					{
						var previousVOzIDLa = _voZIdlA.Entity;
						_voZIdlA.Entity = null;
						previousVOzIDLa.Jizdy.Remove(this);
					}
					_voZIdlA.Entity = value;
					if (value != null)
					{
						value.Jizdy.Add(this);
						_voZIdlOID = value.VozidloID;
					}
					else
					{
						_voZIdlOID = default(uint);
					}
				}
			}
		}


		#endregion

		#region ctor

		public Jizdy()
		{
			_kuRzy = new EntityRef<Kurzy>();
			_studentI = new EntityRef<Studenti>();
			_ucIteLe = new EntityRef<Ucitele>();
			_voZIdlA = new EntityRef<Vozidla>();
		}

		#endregion

	}

	[Table(Name = "drivinscools.kurzy")]
	public partial class Kurzy : INotifyPropertyChanged
	{
		#region INotifyPropertyChanged handling

		public event PropertyChangedEventHandler PropertyChanged;

		protected virtual void OnPropertyChanged(string propertyName)
		{
			if (PropertyChanged != null)
			{
				PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
			}
		}

		#endregion

		#region uint AutosKolaID

		private uint _autosKolaID;
		[DebuggerNonUserCode]
		[Column(Storage = "_autosKolaID", Name = "autoskola_id", DbType = "int unsigned", CanBeNull = false)]
		public uint AutoskolaID
		{
			get
			{
				return _autosKolaID;
			}
			set
			{
				if (value != _autosKolaID)
				{
					_autosKolaID = value;
					OnPropertyChanged("AutoskolaID");
				}
			}
		}

		#endregion

		#region DateTime DatumDo

		private DateTime _datumDo;
		[DebuggerNonUserCode]
		[Column(Storage = "_datumDo", Name = "datum_do", DbType = "date", CanBeNull = false)]
		public DateTime DatumDo
		{
			get
			{
				return _datumDo;
			}
			set
			{
				if (value != _datumDo)
				{
					_datumDo = value;
					OnPropertyChanged("DatumDo");
				}
			}
		}

		#endregion

		#region DateTime DatumOd

		private DateTime _datumOd;
		[DebuggerNonUserCode]
		[Column(Storage = "_datumOd", Name = "datum_od", DbType = "date", CanBeNull = false)]
		public DateTime DatumOd
		{
			get
			{
				return _datumOd;
			}
			set
			{
				if (value != _datumOd)
				{
					_datumOd = value;
					OnPropertyChanged("DatumOd");
				}
			}
		}

		#endregion

		#region string IDentIfIKAcNiCIsLo

		private string _idEntIfIkaCNiCiSLo;
		[DebuggerNonUserCode]
		[Column(Storage = "_idEntIfIkaCNiCiSLo", Name = "identifikacni_cislo", DbType = "varchar(7)", CanBeNull = false)]
		public string IdentifikacniCislo
		{
			get
			{
				return _idEntIfIkaCNiCiSLo;
			}
			set
			{
				if (value != _idEntIfIkaCNiCiSLo)
				{
					_idEntIfIkaCNiCiSLo = value;
                    OnPropertyChanged("IdentifikacniCislo");
				}
			}
		}

		#endregion

		#region uint KuRZID

		private uint _kuRzid;
		[DebuggerNonUserCode]
		[Column(Storage = "_kuRzid", Name = "kurz_id", DbType = "int unsigned", IsPrimaryKey = true, IsDbGenerated = true, CanBeNull = false)]
		public uint KurzID
		{
			get
			{
				return _kuRzid;
			}
			set
			{
				if (value != _kuRzid)
				{
					_kuRzid = value;
                    OnPropertyChanged("KurzID");
				}
			}
		}

		#endregion

		#region string STAv

		private string _staV;
		[DebuggerNonUserCode]
		[Column(Storage = "_staV", Name = "stav", DbType = "varchar(15)", CanBeNull = false)]
		public string Stav
		{
			get
			{
				return _staV;
			}
			set
			{
				if (value != _staV)
				{
					_staV = value;
                    OnPropertyChanged("Stav");
				}
			}
		}

		#endregion

		#region string TYP

		private string _typ;
		[DebuggerNonUserCode]
		[Column(Storage = "_typ", Name = "typ", DbType = "varchar(1)", CanBeNull = false)]
		public string Typ
		{
			get
			{
				return _typ;
			}
			set
			{
				if (value != _typ)
				{
					_typ = value;
					OnPropertyChanged("Typ");
				}
			}
		}

		#endregion

		#region Children

		private EntitySet<Jizdy> _jizdY;
		[Association(Storage = "_jizdY", OtherKey = "KurzID", Name = "JIZDY_KURZY_FK")]
		[DebuggerNonUserCode]
		public EntitySet<Jizdy> Jizdy
		{
			get
			{
				return _jizdY;
			}
			set
			{
				_jizdY = value;
			}
		}

		private EntitySet<Studenti> _studentI;
		[Association(Storage = "_studentI", OtherKey = "KurzID", Name = "STUDENTI_KURZY_FK")]
		[DebuggerNonUserCode]
		public EntitySet<Studenti> Studenti
		{
			get
			{
				return _studentI;
			}
			set
			{
				_studentI = value;
			}
		}

		private EntitySet<Teorie> _teOrIe;
		[Association(Storage = "_teOrIe", OtherKey = "KurzID", Name = "TEORIE_KURZY_FK")]
		[DebuggerNonUserCode]
		public EntitySet<Teorie> Teorie
		{
			get
			{
				return _teOrIe;
			}
			set
			{
				_teOrIe = value;
			}
		}


		#endregion

		#region Parents

		private EntityRef<Autoskoly> _autosKoLy;
		[Association(Storage = "_autosKoLy", ThisKey = "AutoskolaID", Name = "KURZY_AUTOSKOLY_FK", IsForeignKey = true)]
		[DebuggerNonUserCode]
		public Autoskoly Autoskoly
		{
			get
			{
				return _autosKoLy.Entity;
			}
			set
			{
				if (value != _autosKoLy.Entity)
				{
					if (_autosKoLy.Entity != null)
					{
						var previousAutosKoLY = _autosKoLy.Entity;
						_autosKoLy.Entity = null;
						previousAutosKoLY.Kurzy.Remove(this);
					}
					_autosKoLy.Entity = value;
					if (value != null)
					{
						value.Kurzy.Add(this);
						_autosKolaID = value.AutoskolaID;
					}
					else
					{
						_autosKolaID = default(uint);
					}
				}
			}
		}

		private EntityRef<StavyKurzu> _staVYkURzu;
		[Association(Storage = "_staVYkURzu", ThisKey = "Stav", Name = "KURZY_STAVY_KURZU_FK", IsForeignKey = true)]
		[DebuggerNonUserCode]
		public StavyKurzu StavyKurzu
		{
			get
			{
				return _staVYkURzu.Entity;
			}
			set
			{
				if (value != _staVYkURzu.Entity)
				{
					if (_staVYkURzu.Entity != null)
					{
						var previousSTAvYKuRZU = _staVYkURzu.Entity;
						_staVYkURzu.Entity = null;
						previousSTAvYKuRZU.Kurzy.Remove(this);
					}
					_staVYkURzu.Entity = value;
					if (value != null)
					{
						value.Kurzy.Add(this);
						_staV = value.Nazev;
					}
					else
					{
						_staV = default(string);
					}
				}
			}
		}

		private EntityRef<TypyKurzu> _typykURzu;
		[Association(Storage = "_typykURzu", ThisKey = "Typ", Name = "KURZY_TYPY_KURZU_FK", IsForeignKey = true)]
		[DebuggerNonUserCode]
		public TypyKurzu TypyKurzu
		{
			get
			{
				return _typykURzu.Entity;
			}
			set
			{
				if (value != _typykURzu.Entity)
				{
					if (_typykURzu.Entity != null)
					{
						var previousTYPYKuRZU = _typykURzu.Entity;
						_typykURzu.Entity = null;
						previousTYPYKuRZU.Kurzy.Remove(this);
					}
					_typykURzu.Entity = value;
					if (value != null)
					{
						value.Kurzy.Add(this);
						_typ = value.Nazev;
					}
					else
					{
						_typ = default(string);
					}
				}
			}
		}


		#endregion

		#region Attachement handlers

		private void JIZDy_Attach(Jizdy entity)
		{
			entity.Kurzy = this;
		}

		private void JIZDy_Detach(Jizdy entity)
		{
			entity.Kurzy = null;
		}

		private void StudentI_Attach(Studenti entity)
		{
			entity.Kurzy = this;
		}

		private void StudentI_Detach(Studenti entity)
		{
			entity.Kurzy = null;
		}

		private void TeOrIe_Attach(Teorie entity)
		{
			entity.Kurzy = this;
		}

		private void TeOrIe_Detach(Teorie entity)
		{
			entity.Kurzy = null;
		}


		#endregion

		#region ctor

		public Kurzy()
		{
			_jizdY = new EntitySet<Jizdy>(JIZDy_Attach, JIZDy_Detach);
			_studentI = new EntitySet<Studenti>(StudentI_Attach, StudentI_Detach);
			_teOrIe = new EntitySet<Teorie>(TeOrIe_Attach, TeOrIe_Detach);
			_autosKoLy = new EntityRef<Autoskoly>();
			_staVYkURzu = new EntityRef<StavyKurzu>();
			_typykURzu = new EntityRef<TypyKurzu>();
		}

		#endregion

	}

	[Table(Name = "drivinscools.role")]
	public partial class Role : INotifyPropertyChanged
	{
		#region INotifyPropertyChanged handling

		public event PropertyChangedEventHandler PropertyChanged;

		protected virtual void OnPropertyChanged(string propertyName)
		{
			if (PropertyChanged != null)
			{
				PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
			}
		}

		#endregion

		#region string NAzEV

		private string _naZEv;
		[DebuggerNonUserCode]
		[Column(Storage = "_naZEv", Name = "nazev", DbType = "varchar(18)", IsPrimaryKey = true, CanBeNull = false)]
		public string Nazev
		{
			get
			{
				return _naZEv;
			}
			set
			{
				if (value != _naZEv)
				{
					_naZEv = value;
                    OnPropertyChanged("Nazev");
				}
			}
		}

		#endregion

		#region Children

		private EntitySet<Uzivatele> _uziVatEle;
		[Association(Storage = "_uziVatEle", OtherKey = "Role", Name = "UZIVATELE_ROLE_FK")]
		[DebuggerNonUserCode]
		public EntitySet<Uzivatele> Uzivatele
		{
			get
			{
				return _uziVatEle;
			}
			set
			{
				_uziVatEle = value;
			}
		}


		#endregion

		#region Attachement handlers

		private void UziVatELE_Attach(Uzivatele entity)
		{
			entity.RoleRole = this;
		}

		private void UziVatELE_Detach(Uzivatele entity)
		{
			entity.RoleRole = null;
		}


		#endregion

		#region ctor

		public Role()
		{
			_uziVatEle = new EntitySet<Uzivatele>(UziVatELE_Attach, UziVatELE_Detach);
		}

		#endregion

	}

	[Table(Name = "drivinscools.stavy_kurzu")]
	public partial class StavyKurzu : INotifyPropertyChanged
	{
		#region INotifyPropertyChanged handling

		public event PropertyChangedEventHandler PropertyChanged;

		protected virtual void OnPropertyChanged(string propertyName)
		{
			if (PropertyChanged != null)
			{
				PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
			}
		}

		#endregion

		#region string NAzEV

		private string _naZEv;
		[DebuggerNonUserCode]
		[Column(Storage = "_naZEv", Name = "nazev", DbType = "varchar(15)", IsPrimaryKey = true, CanBeNull = false)]
		public string Nazev
		{
			get
			{
				return _naZEv;
			}
			set
			{
				if (value != _naZEv)
				{
					_naZEv = value;
                    OnPropertyChanged("Nazev");
				}
			}
		}

		#endregion

		#region Children

		private EntitySet<Kurzy> _kuRzy;
		[Association(Storage = "_kuRzy", OtherKey = "Stav", Name = "KURZY_STAVY_KURZU_FK")]
		[DebuggerNonUserCode]
		public EntitySet<Kurzy> Kurzy
		{
			get
			{
				return _kuRzy;
			}
			set
			{
				_kuRzy = value;
			}
		}


		#endregion

		#region Attachement handlers

		private void KuRZY_Attach(Kurzy entity)
		{
			entity.StavyKurzu = this;
		}

		private void KuRZY_Detach(Kurzy entity)
		{
			entity.StavyKurzu = null;
		}


		#endregion

		#region ctor

		public StavyKurzu()
		{
			_kuRzy = new EntitySet<Kurzy>(KuRZY_Attach, KuRZY_Detach);
		}

		#endregion

	}

	[Table(Name = "drivinscools.studenti")]
	public partial class Studenti : INotifyPropertyChanged
	{
		#region INotifyPropertyChanged handling

		public event PropertyChangedEventHandler PropertyChanged;

		protected virtual void OnPropertyChanged(string propertyName)
		{
			if (PropertyChanged != null)
			{
				PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
			}
		}

		#endregion

		#region string JMenO

		private string _jmEnO;
		[DebuggerNonUserCode]
		[Column(Storage = "_jmEnO", Name = "jmeno", DbType = "varchar(30)", CanBeNull = false)]
		public string Jmeno
		{
			get
			{
				return _jmEnO;
			}
			set
			{
				if (value != _jmEnO)
				{
					_jmEnO = value;
                    OnPropertyChanged("Jmeno");
				}
			}
		}

		#endregion

		#region uint KuRZID

		private uint _kuRzid;
		[DebuggerNonUserCode]
		[Column(Storage = "_kuRzid", Name = "kurz_id", DbType = "int unsigned", CanBeNull = false)]
		public uint KurzID
		{
			get
			{
				return _kuRzid;
			}
			set
			{
				if (value != _kuRzid)
				{
					_kuRzid = value;
                    OnPropertyChanged("KurzID");
				}
			}
		}

		#endregion

		#region string MatRiCNiCIsLo

		private string _matRiCnICiSLo;
		[DebuggerNonUserCode]
		[Column(Storage = "_matRiCnICiSLo", Name = "matricni_cislo", DbType = "varchar(30)", CanBeNull = false)]
		public string MatricniCislo
		{
			get
			{
				return _matRiCnICiSLo;
			}
			set
			{
				if (value != _matRiCnICiSLo)
				{
					_matRiCnICiSLo = value;
                    OnPropertyChanged("MatricniCislo");
				}
			}
		}

		#endregion

		#region string PRiJMenI

		private string _prIJmEnI;
		[DebuggerNonUserCode]
		[Column(Storage = "_prIJmEnI", Name = "prijmeni", DbType = "varchar(30)", CanBeNull = false)]
		public string Prijmeni
		{
			get
			{
				return _prIJmEnI;
			}
			set
			{
				if (value != _prIJmEnI)
				{
					_prIJmEnI = value;
                    OnPropertyChanged("Prijmeni");
				}
			}
		}

		#endregion

		#region uint StudentID

		private uint _studentID;
		[DebuggerNonUserCode]
		[Column(Storage = "_studentID", Name = "student_id", DbType = "int unsigned", IsPrimaryKey = true, IsDbGenerated = true, CanBeNull = false)]
		public uint StudentID
		{
			get
			{
				return _studentID;
			}
			set
			{
				if (value != _studentID)
				{
					_studentID = value;
					OnPropertyChanged("StudentID");
				}
			}
		}

		#endregion

		#region Children

		private EntitySet<Dochazka> _doChAZka;
		[Association(Storage = "_doChAZka", OtherKey = "StudentID", Name = "DOCHAZKA_STUDENTI_FK")]
		[DebuggerNonUserCode]
		public EntitySet<Dochazka> Dochazka
		{
			get
			{
				return _doChAZka;
			}
			set
			{
				_doChAZka = value;
			}
		}

		private EntitySet<Jizdy> _jizdY;
		[Association(Storage = "_jizdY", OtherKey = "StudentID", Name = "JIZDY_STUDENTI_FK")]
		[DebuggerNonUserCode]
		public EntitySet<Jizdy> Jizdy
		{
			get
			{
				return _jizdY;
			}
			set
			{
				_jizdY = value;
			}
		}


		#endregion

		#region Parents

		private EntityRef<Kurzy> _kuRzy;
		[Association(Storage = "_kuRzy", ThisKey = "KurzID", Name = "STUDENTI_KURZY_FK", IsForeignKey = true)]
		[DebuggerNonUserCode]
		public Kurzy Kurzy
		{
			get
			{
				return _kuRzy.Entity;
			}
			set
			{
				if (value != _kuRzy.Entity)
				{
					if (_kuRzy.Entity != null)
					{
						var previousKuRZY = _kuRzy.Entity;
						_kuRzy.Entity = null;
						previousKuRZY.Studenti.Remove(this);
					}
					_kuRzy.Entity = value;
					if (value != null)
					{
						value.Studenti.Add(this);
						_kuRzid = value.KurzID;
					}
					else
					{
						_kuRzid = default(uint);
					}
				}
			}
		}


		#endregion

		#region Attachement handlers

		private void DoCHaZKA_Attach(Dochazka entity)
		{
			entity.Studenti = this;
		}

		private void DoCHaZKA_Detach(Dochazka entity)
		{
			entity.Studenti = null;
		}

		private void JIZDy_Attach(Jizdy entity)
		{
			entity.Studenti = this;
		}

		private void JIZDy_Detach(Jizdy entity)
		{
			entity.Studenti = null;
		}


		#endregion

		#region ctor

		public Studenti()
		{
			_doChAZka = new EntitySet<Dochazka>(DoCHaZKA_Attach, DoCHaZKA_Detach);
			_jizdY = new EntitySet<Jizdy>(JIZDy_Attach, JIZDy_Detach);
			_kuRzy = new EntityRef<Kurzy>();
		}

		#endregion

	}

	[Table(Name = "drivinscools.teorie")]
	public partial class Teorie : INotifyPropertyChanged
	{
		#region INotifyPropertyChanged handling

		public event PropertyChangedEventHandler PropertyChanged;

		protected virtual void OnPropertyChanged(string propertyName)
		{
			if (PropertyChanged != null)
			{
				PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
			}
		}

		#endregion

		#region DateTime Do

		private DateTime _do;
		[DebuggerNonUserCode]
		[Column(Storage = "_do", Name = "do", DbType = "datetime", CanBeNull = false)]
		public DateTime Do
		{
			get
			{
				return _do;
			}
			set
			{
				if (value != _do)
				{
					_do = value;
					OnPropertyChanged("Do");
				}
			}
		}

		#endregion

		#region uint KuRZID

		private uint _kuRzid;
		[DebuggerNonUserCode]
		[Column(Storage = "_kuRzid", Name = "kurz_id", DbType = "int unsigned", CanBeNull = false)]
		public uint KurzID
		{
			get
			{
				return _kuRzid;
			}
			set
			{
				if (value != _kuRzid)
				{
					_kuRzid = value;
					OnPropertyChanged("KurzID");
				}
			}
		}

		#endregion

		#region DateTime Od

		private DateTime _od;
		[DebuggerNonUserCode]
		[Column(Storage = "_od", Name = "od", DbType = "datetime", CanBeNull = false)]
		public DateTime Od
		{
			get
			{
				return _od;
			}
			set
			{
				if (value != _od)
				{
					_od = value;
					OnPropertyChanged("Od");
				}
			}
		}

		#endregion

		#region uint TeOrIeID

		private uint _teOrIeID;
		[DebuggerNonUserCode]
		[Column(Storage = "_teOrIeID", Name = "teorie_id", DbType = "int unsigned", IsPrimaryKey = true, IsDbGenerated = true, CanBeNull = false)]
		public uint TeorieID
		{
			get
			{
				return _teOrIeID;
			}
			set
			{
				if (value != _teOrIeID)
				{
					_teOrIeID = value;
                    OnPropertyChanged("TeorieID");
				}
			}
		}

		#endregion

		#region string TYP

		private string _typ;
		[DebuggerNonUserCode]
		[Column(Storage = "_typ", Name = "typ", DbType = "varchar(30)", CanBeNull = false)]
		public string Typ
		{
			get
			{
				return _typ;
			}
			set
			{
				if (value != _typ)
				{
					_typ = value;
					OnPropertyChanged("Typ");
				}
			}
		}

		#endregion

		#region uint UCiteLID

		private uint _ucIteLid;
		[DebuggerNonUserCode]
		[Column(Storage = "_ucIteLid", Name = "ucitel_id", DbType = "int unsigned", CanBeNull = false)]
		public uint UcitelID
		{
			get
			{
				return _ucIteLid;
			}
			set
			{
				if (value != _ucIteLid)
				{
					_ucIteLid = value;
                    OnPropertyChanged("UcitelID");
				}
			}
		}

		#endregion

		#region Children

		private EntitySet<Dochazka> _doChAZka;
		[Association(Storage = "_doChAZka", OtherKey = "TeorieID", Name = "DOCHAZKA_TEORIE_FK")]
		[DebuggerNonUserCode]
		public EntitySet<Dochazka> Dochazka
		{
			get
			{
				return _doChAZka;
			}
			set
			{
				_doChAZka = value;
			}
		}


		#endregion

		#region Parents

		private EntityRef<Kurzy> _kuRzy;
		[Association(Storage = "_kuRzy", ThisKey = "KurzID", Name = "TEORIE_KURZY_FK", IsForeignKey = true)]
		[DebuggerNonUserCode]
		public Kurzy Kurzy
		{
			get
			{
				return _kuRzy.Entity;
			}
			set
			{
				if (value != _kuRzy.Entity)
				{
					if (_kuRzy.Entity != null)
					{
						var previousKuRZY = _kuRzy.Entity;
						_kuRzy.Entity = null;
						previousKuRZY.Teorie.Remove(this);
					}
					_kuRzy.Entity = value;
					if (value != null)
					{
						value.Teorie.Add(this);
						_kuRzid = value.KurzID;
					}
					else
					{
						_kuRzid = default(uint);
					}
				}
			}
		}

		private EntityRef<TypyTeorie> _typytEOrIe;
		[Association(Storage = "_typytEOrIe", ThisKey = "Typ", Name = "TEORIE_TYPY_TEORIE_FK", IsForeignKey = true)]
		[DebuggerNonUserCode]
		public TypyTeorie TypyTeorie
		{
			get
			{
				return _typytEOrIe.Entity;
			}
			set
			{
				if (value != _typytEOrIe.Entity)
				{
					if (_typytEOrIe.Entity != null)
					{
						var previousTYPYTeOrIe = _typytEOrIe.Entity;
						_typytEOrIe.Entity = null;
						previousTYPYTeOrIe.Teorie.Remove(this);
					}
					_typytEOrIe.Entity = value;
					if (value != null)
					{
						value.Teorie.Add(this);
						_typ = value.Nazev;
					}
					else
					{
						_typ = default(string);
					}
				}
			}
		}

		private EntityRef<Ucitele> _ucIteLe;
		[Association(Storage = "_ucIteLe", ThisKey = "UcitelID", Name = "TEORIE_UCITELE_FK", IsForeignKey = true)]
		[DebuggerNonUserCode]
		public Ucitele Ucitele
		{
			get
			{
				return _ucIteLe.Entity;
			}
			set
			{
				if (value != _ucIteLe.Entity)
				{
					if (_ucIteLe.Entity != null)
					{
						var previousUCiteLE = _ucIteLe.Entity;
						_ucIteLe.Entity = null;
						previousUCiteLE.Teorie.Remove(this);
					}
					_ucIteLe.Entity = value;
					if (value != null)
					{
						value.Teorie.Add(this);
						_ucIteLid = value.UcitelID;
					}
					else
					{
						_ucIteLid = default(uint);
					}
				}
			}
		}


		#endregion

		#region Attachement handlers

		private void DoCHaZKA_Attach(Dochazka entity)
		{
			entity.Teorie = this;
		}

		private void DoCHaZKA_Detach(Dochazka entity)
		{
			entity.Teorie = null;
		}


		#endregion

		#region ctor

		public Teorie()
		{
			_doChAZka = new EntitySet<Dochazka>(DoCHaZKA_Attach, DoCHaZKA_Detach);
			_kuRzy = new EntityRef<Kurzy>();
			_typytEOrIe = new EntityRef<TypyTeorie>();
			_ucIteLe = new EntityRef<Ucitele>();
		}

		#endregion

	}

	[Table(Name = "drivinscools.typy_kurzu")]
	public partial class TypyKurzu : INotifyPropertyChanged
	{
		#region INotifyPropertyChanged handling

		public event PropertyChangedEventHandler PropertyChanged;

		protected virtual void OnPropertyChanged(string propertyName)
		{
			if (PropertyChanged != null)
			{
				PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
			}
		}

		#endregion

		#region string NAzEV

		private string _naZEv;
		[DebuggerNonUserCode]
		[Column(Storage = "_naZEv", Name = "nazev", DbType = "varchar(1)", IsPrimaryKey = true, CanBeNull = false)]
		public string Nazev
		{
			get
			{
				return _naZEv;
			}
			set
			{
				if (value != _naZEv)
				{
					_naZEv = value;
                    OnPropertyChanged("Nazev");
				}
			}
		}

		#endregion

		#region Children

		private EntitySet<Kurzy> _kuRzy;
		[Association(Storage = "_kuRzy", OtherKey = "Typ", Name = "KURZY_TYPY_KURZU_FK")]
		[DebuggerNonUserCode]
		public EntitySet<Kurzy> Kurzy
		{
			get
			{
				return _kuRzy;
			}
			set
			{
				_kuRzy = value;
			}
		}


		#endregion

		#region Attachement handlers

		private void KuRZY_Attach(Kurzy entity)
		{
			entity.TypyKurzu = this;
		}

		private void KuRZY_Detach(Kurzy entity)
		{
			entity.TypyKurzu = null;
		}


		#endregion

		#region ctor

		public TypyKurzu()
		{
			_kuRzy = new EntitySet<Kurzy>(KuRZY_Attach, KuRZY_Detach);
		}

		#endregion

	}

	[Table(Name = "drivinscools.typy_teorie")]
	public partial class TypyTeorie : INotifyPropertyChanged
	{
		#region INotifyPropertyChanged handling

		public event PropertyChangedEventHandler PropertyChanged;

		protected virtual void OnPropertyChanged(string propertyName)
		{
			if (PropertyChanged != null)
			{
				PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
			}
		}

		#endregion

		#region string NAzEV

		private string _naZEv;
		[DebuggerNonUserCode]
		[Column(Storage = "_naZEv", Name = "nazev", DbType = "varchar(30)", IsPrimaryKey = true, CanBeNull = false)]
		public string Nazev
		{
			get
			{
				return _naZEv;
			}
			set
			{
				if (value != _naZEv)
				{
					_naZEv = value;
                    OnPropertyChanged("Nazev");
				}
			}
		}

		#endregion

		#region Children

		private EntitySet<Teorie> _teOrIe;
		[Association(Storage = "_teOrIe", OtherKey = "Typ", Name = "TEORIE_TYPY_TEORIE_FK")]
		[DebuggerNonUserCode]
		public EntitySet<Teorie> Teorie
		{
			get
			{
				return _teOrIe;
			}
			set
			{
				_teOrIe = value;
			}
		}


		#endregion

		#region Attachement handlers

		private void TeOrIe_Attach(Teorie entity)
		{
			entity.TypyTeorie = this;
		}

		private void TeOrIe_Detach(Teorie entity)
		{
			entity.TypyTeorie = null;
		}


		#endregion

		#region ctor

		public TypyTeorie()
		{
			_teOrIe = new EntitySet<Teorie>(TeOrIe_Attach, TeOrIe_Detach);
		}

		#endregion

	}

	[Table(Name = "drivinscools.typy_vozidel")]
	public partial class TypyVozidel : INotifyPropertyChanged
	{
		#region INotifyPropertyChanged handling

		public event PropertyChangedEventHandler PropertyChanged;

		protected virtual void OnPropertyChanged(string propertyName)
		{
			if (PropertyChanged != null)
			{
				PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
			}
		}

		#endregion

		#region string NAzEV

		private string _naZEv;
		[DebuggerNonUserCode]
		[Column(Storage = "_naZEv", Name = "nazev", DbType = "varchar(10)", IsPrimaryKey = true, CanBeNull = false)]
		public string Nazev
		{
			get
			{
				return _naZEv;
			}
			set
			{
				if (value != _naZEv)
				{
					_naZEv = value;
                    OnPropertyChanged("Nazev");
				}
			}
		}

		#endregion

		#region Children

		private EntitySet<Vozidla> _voZIdlA;
		[Association(Storage = "_voZIdlA", OtherKey = "VozidloTyp", Name = "VOZIDLA_TYPY_VOZIDEL_FK")]
		[DebuggerNonUserCode]
		public EntitySet<Vozidla> Vozidla
		{
			get
			{
				return _voZIdlA;
			}
			set
			{
				_voZIdlA = value;
			}
		}


		#endregion

		#region Attachement handlers

		private void VOzIDLa_Attach(Vozidla entity)
		{
			entity.TypyVozidel = this;
		}

		private void VOzIDLa_Detach(Vozidla entity)
		{
			entity.TypyVozidel = null;
		}


		#endregion

		#region ctor

		public TypyVozidel()
		{
			_voZIdlA = new EntitySet<Vozidla>(VOzIDLa_Attach, VOzIDLa_Detach);
		}

		#endregion

	}

	[Table(Name = "drivinscools.ucitele")]
	public partial class Ucitele : INotifyPropertyChanged
	{
		#region INotifyPropertyChanged handling

		public event PropertyChangedEventHandler PropertyChanged;

		protected virtual void OnPropertyChanged(string propertyName)
		{
			if (PropertyChanged != null)
			{
				PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
			}
		}

		#endregion

		#region int AdReSaCP

		private int _adReSaCp;
		[DebuggerNonUserCode]
		[Column(Storage = "_adReSaCp", Name = "adresa_cp", DbType = "int", CanBeNull = false)]
		public int AdresaCP
		{
			get
			{
				return _adReSaCp;
			}
			set
			{
				if (value != _adReSaCp)
				{
					_adReSaCp = value;
                    OnPropertyChanged("AdresaCP");
				}
			}
		}

		#endregion

		#region string AdReSaMEstO

		private string _adReSaMeStO;
		[DebuggerNonUserCode]
		[Column(Storage = "_adReSaMeStO", Name = "adresa_mesto", DbType = "varchar(33)", CanBeNull = false)]
		public string AdresaMesto
		{
			get
			{
				return _adReSaMeStO;
			}
			set
			{
				if (value != _adReSaMeStO)
				{
					_adReSaMeStO = value;
                    OnPropertyChanged("AdresaMesto");
				}
			}
		}

		#endregion

		#region int AdReSaPsC

		private int _adReSaPsC;
		[DebuggerNonUserCode]
		[Column(Storage = "_adReSaPsC", Name = "adresa_psc", DbType = "int", CanBeNull = false)]
		public int AdresaPSC
		{
			get
			{
				return _adReSaPsC;
			}
			set
			{
				if (value != _adReSaPsC)
				{
					_adReSaPsC = value;
                    OnPropertyChanged("AdresaPSC");
				}
			}
		}

		#endregion

		#region string AdReSaUlIce

		private string _adReSaUlIce;
		[DebuggerNonUserCode]
		[Column(Storage = "_adReSaUlIce", Name = "adresa_ulice", DbType = "varchar(37)", CanBeNull = false)]
		public string AdresaUlice
		{
			get
			{
				return _adReSaUlIce;
			}
			set
			{
				if (value != _adReSaUlIce)
				{
					_adReSaUlIce = value;
                    OnPropertyChanged("AdresaUlice");
				}
			}
		}

		#endregion

		#region uint AutosKolaID

		private uint _autosKolaID;
		[DebuggerNonUserCode]
		[Column(Storage = "_autosKolaID", Name = "autoskola_id", DbType = "int unsigned", CanBeNull = false)]
		public uint AutoskolaID
		{
			get
			{
				return _autosKolaID;
			}
			set
			{
				if (value != _autosKolaID)
				{
					_autosKolaID = value;
                    OnPropertyChanged("AutoskolaID");
				}
			}
		}

		#endregion

		#region double HodInOvaMZDa

		private double _hodInOvaMzdA;
		[DebuggerNonUserCode]
		[Column(Storage = "_hodInOvaMzdA", Name = "hodinova_mzda", DbType = "double", CanBeNull = false)]
		public double HodinovaMzda
		{
			get
			{
				return _hodInOvaMzdA;
			}
			set
			{
				if (value != _hodInOvaMzdA)
				{
					_hodInOvaMzdA = value;
                    OnPropertyChanged("HodinovaMzda");
				}
			}
		}

		#endregion

		#region string JMenO

		private string _jmEnO;
		[DebuggerNonUserCode]
		[Column(Storage = "_jmEnO", Name = "jmeno", DbType = "varchar(30)", CanBeNull = false)]
		public string Jmeno
		{
			get
			{
				return _jmEnO;
			}
			set
			{
				if (value != _jmEnO)
				{
					_jmEnO = value;
                    OnPropertyChanged("Jmeno");
				}
			}
		}

		#endregion

		#region string OSobNiCIsLo

		private string _osObNiCiSLo;
		[DebuggerNonUserCode]
		[Column(Storage = "_osObNiCiSLo", Name = "osobni_cislo", DbType = "varchar(6)", CanBeNull = false)]
		public string OsobniCislo
		{
			get
			{
				return _osObNiCiSLo;
			}
			set
			{
				if (value != _osObNiCiSLo)
				{
					_osObNiCiSLo = value;
                    OnPropertyChanged("OsobniCislo");
				}
			}
		}

		#endregion

		#region int POCeTDETI

		private int _pocETdeti;
		[DebuggerNonUserCode]
		[Column(Storage = "_pocETdeti", Name = "pocet_deti", DbType = "int", CanBeNull = false)]
		public int PocetDeti
		{
			get
			{
				return _pocETdeti;
			}
			set
			{
				if (value != _pocETdeti)
				{
					_pocETdeti = value;
                    OnPropertyChanged("PocetDeti");
				}
			}
		}

		#endregion

		#region string PRiJMenI

		private string _prIJmEnI;
		[DebuggerNonUserCode]
		[Column(Storage = "_prIJmEnI", Name = "prijmeni", DbType = "varchar(30)", CanBeNull = false)]
		public string Prijmeni
		{
			get
			{
				return _prIJmEnI;
			}
			set
			{
				if (value != _prIJmEnI)
				{
					_prIJmEnI = value;
                    OnPropertyChanged("Prijmeni");
				}
			}
		}

		#endregion

		#region uint UCiteLID

		private uint _ucIteLid;
		[DebuggerNonUserCode]
		[Column(Storage = "_ucIteLid", Name = "ucitel_id", DbType = "int unsigned", IsPrimaryKey = true, IsDbGenerated = true, CanBeNull = false)]
		public uint UcitelID
		{
			get
			{
				return _ucIteLid;
			}
			set
			{
				if (value != _ucIteLid)
				{
					_ucIteLid = value;
                    OnPropertyChanged("UcitelID");
				}
			}
		}

		#endregion

        #region sbyte OprAvNeNiJIZDyA

        private sbyte _oprAvNeNiJizdYA;
        [DebuggerNonUserCode]
        [Column(Storage = "_oprAvNeNiJizdYA", Name = "opravneni_jizdy_a", DbType = "tinyint(1)", CanBeNull = false)]
        public bool OpravneniJizdyA
        {
            get
            {
                return _oprAvNeNiJizdYA == 0 ? false : true;
            }
            set
            {                
                sbyte val = value ? (sbyte)1 : (sbyte)0;
                if (val != _oprAvNeNiJizdYA)
                {
                    _oprAvNeNiJizdYA = val;
                    OnPropertyChanged("OpravneniJizdyA");
                }
            }
        }

        #endregion

        #region sbyte OprAvNeNiJIZDyB

        private sbyte _oprAvNeNiJizdYB;
        [DebuggerNonUserCode]
        [Column(Storage = "_oprAvNeNiJizdYB", Name = "opravneni_jizdy_b", DbType = "tinyint(1)", CanBeNull = false)]
        public bool OpravneniJizdyB
        {
            get
            {
                return _oprAvNeNiJizdYB == 0 ? false : true;
            }
            set
            {
                sbyte val = value ? (sbyte)1 : (sbyte)0;
                if (val != _oprAvNeNiJizdYB)
                {
                    _oprAvNeNiJizdYB = val;
                    OnPropertyChanged("OpravneniJizdyB");
                }
            }
        }

        #endregion

        #region sbyte OprAvNeNiJIZDyC

        private sbyte _oprAvNeNiJizdYC;
        [DebuggerNonUserCode]
        [Column(Storage = "_oprAvNeNiJizdYC", Name = "opravneni_jizdy_c", DbType = "tinyint(1)", CanBeNull = false)]
        public bool OpravneniJizdyC
        {
            get
            {
                return _oprAvNeNiJizdYC == 0 ? false : true;
            }
            set
            {
                sbyte val = value ? (sbyte)1 : (sbyte)0;
                if (val != _oprAvNeNiJizdYC)
                {
                    _oprAvNeNiJizdYC = val;
                    OnPropertyChanged("OpravneniJizdyC");
                }
            }
        }

        #endregion

        #region sbyte OprAvNeNiJIZDyD

        private sbyte _oprAvNeNiJizdYD;
        [DebuggerNonUserCode]
        [Column(Storage = "_oprAvNeNiJizdYD", Name = "opravneni_jizdy_d", DbType = "tinyint(1)", CanBeNull = false)]
        public bool OpravneniJizdyD
        {
            get
            {
                return _oprAvNeNiJizdYD == 0 ? false : true;
            }
            set
            {
                sbyte val = value ? (sbyte)1 : (sbyte)0;
                if (val != _oprAvNeNiJizdYD)
                {
                    _oprAvNeNiJizdYD = val;
                    OnPropertyChanged("OpravneniJizdyD");
                }
            }
        }

        #endregion

        #region sbyte OprAvNeNiJIZDyT

        private sbyte _oprAvNeNiJizdYT;
        [DebuggerNonUserCode]
        [Column(Storage = "_oprAvNeNiJizdYT", Name = "opravneni_jizdy_t", DbType = "tinyint(1)", CanBeNull = false)]
        public bool OpravneniJizdyT
        {
            get
            {
                return _oprAvNeNiJizdYT == 0 ? false : true;
            }
            set
            {
                sbyte val = value ? (sbyte)1 : (sbyte)0;
                if (val != _oprAvNeNiJizdYT)
                {
                    _oprAvNeNiJizdYT = val;
                    OnPropertyChanged("OpravneniJizdyT");
                }
            }
        }

        #endregion

        #region sbyte OprAvNeNiPRAvidLa

        private sbyte _oprAvNeNiPraVidLa;
        [DebuggerNonUserCode]
        [Column(Storage = "_oprAvNeNiPraVidLa", Name = "opravneni_pravidla", DbType = "tinyint(1)", CanBeNull = false)]
        public bool OpravneniPravidla
        {
            get
            {
                return _oprAvNeNiPraVidLa == 0 ? false : true;
            }
            set
            {
                sbyte val = value ? (sbyte)1 : (sbyte)0;
                if (val != _oprAvNeNiPraVidLa)
                {
                    _oprAvNeNiPraVidLa = val;
                    OnPropertyChanged("OpravneniPravidla");
                }
            }
        }

        #endregion

        #region sbyte OprAvNeNiUDrZBa

        private sbyte _oprAvNeNiUdRZbA;
        [DebuggerNonUserCode]
        [Column(Storage = "_oprAvNeNiUdRZbA", Name = "opravneni_udrzba", DbType = "tinyint(1)", CanBeNull = false)]
        public bool OpravneniUdrzba
        {
            get
            {
                return _oprAvNeNiUdRZbA == 0 ? false : true;
            }
            set
            {
                sbyte val = value ? (sbyte)1 : (sbyte)0;
                if (val != _oprAvNeNiUdRZbA)
                {
                    _oprAvNeNiUdRZbA = val;
                    OnPropertyChanged("OpravneniUdrzba");
                }
            }
        }

        #endregion

        #region sbyte OprAvNeNiZDrAvOVeda

        private sbyte _oprAvNeNiZdRAvOvEda;
        [DebuggerNonUserCode]
        [Column(Storage = "_oprAvNeNiZdRAvOvEda", Name = "opravneni_zdravoveda", DbType = "tinyint(1)", CanBeNull = false)]
        public bool OpravneniZdravoveda
        {
            get
            {
                return _oprAvNeNiZdRAvOvEda == 0 ? false : true;
            }
            set
            {
                sbyte val = value ? (sbyte)1 : (sbyte)0;
                if (val != _oprAvNeNiZdRAvOvEda)
                {
                    _oprAvNeNiZdRAvOvEda = val;
                    OnPropertyChanged("OpravneniZdravoveda");
                }
            }
        }

        #endregion

        #region DateTime? PlatNoSTOprAvNeNi

        private DateTime? _platNoStoPrAvNeNi;
        [DebuggerNonUserCode]
        [Column(Storage = "_platNoStoPrAvNeNi", Name = "platnost_opravneni", DbType = "date")]
        public DateTime? PlatnostOpravneni
        {
            get
            {
                return _platNoStoPrAvNeNi;
            }
            set
            {
                if (value != _platNoStoPrAvNeNi)
                {
                    _platNoStoPrAvNeNi = value;
                    OnPropertyChanged("PlatNoSTOprAvNeNi");
                }
            }
        }

        #endregion        

        #region DateTime? POsLedNiProHLidKA

        private DateTime? _poSLedNiProHlIDKa;
        [DebuggerNonUserCode]
        [Column(Storage = "_poSLedNiProHlIDKa", Name = "posledni_prohlidka", DbType = "date")]
        public DateTime? PosledniProhlidka
        {
            get
            {
                return _poSLedNiProHlIDKa;
            }
            set
            {
                if (value != _poSLedNiProHlIDKa)
                {
                    _poSLedNiProHlIDKa = value;
                    OnPropertyChanged("POsLedNiProHLidKA");
                }
            }
        }

        #endregion

		#region Children

		private EntitySet<Jizdy> _jizdY;
		[Association(Storage = "_jizdY", OtherKey = "UcitelID", Name = "JIZDY_UCITELE_FK")]
		[DebuggerNonUserCode]
		public EntitySet<Jizdy> Jizdy
		{
			get
			{
				return _jizdY;
			}
			set
			{
				_jizdY = value;
			}
		}

		private EntitySet<Teorie> _teOrIe;
		[Association(Storage = "_teOrIe", OtherKey = "UcitelID", Name = "TEORIE_UCITELE_FK")]
		[DebuggerNonUserCode]
		public EntitySet<Teorie> Teorie
		{
			get
			{
				return _teOrIe;
			}
			set
			{
				_teOrIe = value;
			}
		}

		private EntitySet<DokumentyUcitelu> _doKuMenTyucIteLu;
		[Association(Storage = "_doKuMenTyucIteLu", OtherKey = "UcitelID", Name = "UCITELE_DOKUMENTY_UCITELU_FK")]
		[DebuggerNonUserCode]
		public EntitySet<DokumentyUcitelu> DokumentyUcitelu
		{
			get
			{
				return _doKuMenTyucIteLu;
			}
			set
			{
				_doKuMenTyucIteLu = value;
			}
		}

		private EntitySet<Vyplatnice> _vypLatNice;
		[Association(Storage = "_vypLatNice", OtherKey = "UcitelID", Name = "VYPLATNICE_UCITELE_FK")]
		[DebuggerNonUserCode]
		public EntitySet<Vyplatnice> Vyplatnice
		{
			get
			{
				return _vypLatNice;
			}
			set
			{
				_vypLatNice = value;
			}
		}


		#endregion

		#region Parents

		private EntityRef<Autoskoly> _autosKoLy;
		[Association(Storage = "_autosKoLy", ThisKey = "AutoskolaID", Name = "UCITELE_AUTOSKOLY_FK", IsForeignKey = true)]
		[DebuggerNonUserCode]
		public Autoskoly Autoskoly
		{
			get
			{
				return _autosKoLy.Entity;
			}
			set
			{
				if (value != _autosKoLy.Entity)
				{
					if (_autosKoLy.Entity != null)
					{
						var previousAutosKoLY = _autosKoLy.Entity;
						_autosKoLy.Entity = null;
						previousAutosKoLY.Ucitele.Remove(this);
					}
					_autosKoLy.Entity = value;
					if (value != null)
					{
						value.Ucitele.Add(this);
						_autosKolaID = value.AutoskolaID;
					}
					else
					{
						_autosKolaID = default(uint);
					}
				}
			}
		}


		#endregion

		#region Attachement handlers

		private void JIZDy_Attach(Jizdy entity)
		{
			entity.Ucitele = this;
		}

		private void JIZDy_Detach(Jizdy entity)
		{
			entity.Ucitele = null;
		}

		private void TeOrIe_Attach(Teorie entity)
		{
			entity.Ucitele = this;
		}

		private void TeOrIe_Detach(Teorie entity)
		{
			entity.Ucitele = null;
		}

		private void DoKuMenTYUCiteLu_Attach(DokumentyUcitelu entity)
		{
			entity.Ucitele = this;
		}

		private void DoKuMenTYUCiteLu_Detach(DokumentyUcitelu entity)
		{
			entity.Ucitele = null;
		}

		private void VYPlatNice_Attach(Vyplatnice entity)
		{
			entity.Ucitele = this;
		}

		private void VYPlatNice_Detach(Vyplatnice entity)
		{
			entity.Ucitele = null;
		}


		#endregion

		#region ctor

		public Ucitele()
		{
			_jizdY = new EntitySet<Jizdy>(JIZDy_Attach, JIZDy_Detach);			
			_teOrIe = new EntitySet<Teorie>(TeOrIe_Attach, TeOrIe_Detach);
			_doKuMenTyucIteLu = new EntitySet<DokumentyUcitelu>(DoKuMenTYUCiteLu_Attach, DoKuMenTYUCiteLu_Detach);
			_vypLatNice = new EntitySet<Vyplatnice>(VYPlatNice_Attach, VYPlatNice_Detach);
			_autosKoLy = new EntityRef<Autoskoly>();
		}

		#endregion

	}

	[Table(Name = "drivinscools.uzivatele")]
	public partial class Uzivatele : INotifyPropertyChanged
	{
		#region INotifyPropertyChanged handling

		public event PropertyChangedEventHandler PropertyChanged;

		protected virtual void OnPropertyChanged(string propertyName)
		{
			if (PropertyChanged != null)
			{
				PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
			}
		}

		#endregion

		#region string HeSlO

		private string _heSlO;
		[DebuggerNonUserCode]
		[Column(Storage = "_heSlO", Name = "heslo", DbType = "varchar(30)", CanBeNull = false)]
		public string Heslo
		{
			get
			{
				return _heSlO;
			}
			set
			{
				if (value != _heSlO)
				{
					_heSlO = value;
                    OnPropertyChanged("Heslo");
				}
			}
		}

		#endregion

		#region string Role

		private string _role;
		[DebuggerNonUserCode]
		[Column(Storage = "_role", Name = "role", DbType = "varchar(18)", CanBeNull = false)]
		public string Role
		{
			get
			{
				return _role;
			}
			set
			{
				if (value != _role)
				{
					_role = value;
					OnPropertyChanged("Role");
				}
			}
		}

		#endregion

		#region uint UziVatELID

		private uint _uziVatElid;
		[DebuggerNonUserCode]
		[Column(Storage = "_uziVatElid", Name = "uzivatel_id", DbType = "int unsigned", IsPrimaryKey = true, IsDbGenerated = true, CanBeNull = false)]
		public uint UzivatelID
		{
			get
			{
				return _uziVatElid;
			}
			set
			{
				if (value != _uziVatElid)
				{
					_uziVatElid = value;
                    OnPropertyChanged("UzivatelID");
				}
			}
		}

		#endregion

		#region string UziVatELSkEJMenO

		private string _uziVatElsKEjmEnO;
		[DebuggerNonUserCode]
		[Column(Storage = "_uziVatElsKEjmEnO", Name = "uzivatelske_jmeno", DbType = "varchar(15)", CanBeNull = false)]
		public string UzivatelskeJmeno
		{
			get
			{
				return _uziVatElsKEjmEnO;
			}
			set
			{
				if (value != _uziVatElsKEjmEnO)
				{
					_uziVatElsKEjmEnO = value;
                    OnPropertyChanged("UzivatelskeJmeno");
				}
			}
		}

		#endregion

		#region Children

		private EntitySet<Autoskoly> _autosKoLy;
		[Association(Storage = "_autosKoLy", OtherKey = "UzivatelID", Name = "AUTOSKOLA_UZIVATELE_FK")]
		[DebuggerNonUserCode]
		public EntitySet<Autoskoly> Autoskoly
		{
			get
			{
				return _autosKoLy;
			}
			set
			{
				_autosKoLy = value;
			}
		}


		#endregion

		#region Parents

		private EntityRef<Role> _roleRole;
		[Association(Storage = "_roleRole", ThisKey = "Role", Name = "UZIVATELE_ROLE_FK", IsForeignKey = true)]
		[DebuggerNonUserCode]
		public Role RoleRole
		{
			get
			{
				return _roleRole.Entity;
			}
			set
			{
				if (value != _roleRole.Entity)
				{
					if (_roleRole.Entity != null)
					{
						var previousRole = _roleRole.Entity;
						_roleRole.Entity = null;
						previousRole.Uzivatele.Remove(this);
					}
					_roleRole.Entity = value;
					if (value != null)
					{
						value.Uzivatele.Add(this);
						_role = value.Nazev;
					}
					else
					{
						_role = default(string);
					}
				}
			}
		}


		#endregion

		#region Attachement handlers

		private void AutosKoLY_Attach(Autoskoly entity)
		{
			entity.Uzivatele = this;
		}

		private void AutosKoLY_Detach(Autoskoly entity)
		{
			entity.Uzivatele = null;
		}


		#endregion

		#region ctor

		public Uzivatele()
		{
			_autosKoLy = new EntitySet<Autoskoly>(AutosKoLY_Attach, AutosKoLY_Detach);
			_roleRole = new EntityRef<Role>();
		}

		#endregion

	}

	[Table(Name = "drivinscools.vozidla")]
	public partial class Vozidla : INotifyPropertyChanged
	{
		#region INotifyPropertyChanged handling

		public event PropertyChangedEventHandler PropertyChanged;

		protected virtual void OnPropertyChanged(string propertyName)
		{
			if (PropertyChanged != null)
			{
				PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
			}
		}

		#endregion

		#region uint AutosKolaID

		private uint _autosKolaID;
		[DebuggerNonUserCode]
		[Column(Storage = "_autosKolaID", Name = "autoskola_id", DbType = "int unsigned", CanBeNull = false)]
		public uint AutoskolaID
		{
			get
			{
				return _autosKolaID;
			}
			set
			{
				if (value != _autosKolaID)
				{
					_autosKolaID = value;
                    OnPropertyChanged("AutoskolaID");
				}
			}
		}

		#endregion

		#region string Model

		private string _model;
		[DebuggerNonUserCode]
		[Column(Storage = "_model", Name = "model", DbType = "varchar(15)", CanBeNull = false)]
		public string Model
		{
			get
			{
				return _model;
			}
			set
			{
				if (value != _model)
				{
					_model = value;
					OnPropertyChanged("Model");
				}
			}
		}

		#endregion

		#region uint POCatEcNiSTAvKM

		private uint _pocAtEcNiStaVKm;
		[DebuggerNonUserCode]
		[Column(Storage = "_pocAtEcNiStaVKm", Name = "pocatecni_stav_km", DbType = "int unsigned", CanBeNull = false)]
		public uint PocatecniStavKm
		{
			get
			{
				return _pocAtEcNiStaVKm;
			}
			set
			{
				if (value != _pocAtEcNiStaVKm)
				{
					_pocAtEcNiStaVKm = value;
                    OnPropertyChanged("PocatecniStavKm");
				}
			}
		}

		#endregion

		#region DateTime POsLedNiSTK

		private DateTime _poSLedNiStk;
		[DebuggerNonUserCode]
		[Column(Storage = "_poSLedNiStk", Name = "posledni_stk", DbType = "date", CanBeNull = false)]
		public DateTime PosledniSTK
		{
			get
			{
				return _poSLedNiStk;
			}
			set
			{
				if (value != _poSLedNiStk)
				{
					_poSLedNiStk = value;
                    OnPropertyChanged("PosledniSTK");
				}
			}
		}

		#endregion

		#region uint ROkVYRobY

		private uint _roKVyrObY;
		[DebuggerNonUserCode]
		[Column(Storage = "_roKVyrObY", Name = "rok_vyroby", DbType = "int unsigned", CanBeNull = false)]
		public uint RokVyroby
		{
			get
			{
				return _roKVyrObY;
			}
			set
			{
				if (value != _roKVyrObY)
				{
					_roKVyrObY = value;
                    OnPropertyChanged("RokVyroby");
				}
			}
		}

		#endregion

		#region string SpZ

		private string _spZ;
		[DebuggerNonUserCode]
		[Column(Storage = "_spZ", Name = "spz", DbType = "varchar(8)", CanBeNull = false)]
		public string Spz
		{
			get
			{
				return _spZ;
			}
			set
			{
				if (value != _spZ)
				{
					_spZ = value;
					OnPropertyChanged("Spz");
				}
			}
		}

		#endregion

		#region string VIn

		private string _viN;
		[DebuggerNonUserCode]
		[Column(Storage = "_viN", Name = "vin", DbType = "varchar(17)", CanBeNull = false)]
		public string Vin
		{
			get
			{
				return _viN;
			}
			set
			{
				if (value != _viN)
				{
					_viN = value;
					OnPropertyChanged("Vin");
				}
			}
		}

		#endregion

		#region uint VOzIDLoID

		private uint _voZIdlOID;
		[DebuggerNonUserCode]
		[Column(Storage = "_voZIdlOID", Name = "vozidlo_id", DbType = "int unsigned", IsPrimaryKey = true, IsDbGenerated = true, CanBeNull = false)]
		public uint VozidloID
		{
			get
			{
				return _voZIdlOID;
			}
			set
			{
				if (value != _voZIdlOID)
				{
					_voZIdlOID = value;
                    OnPropertyChanged("VozidloID");
				}
			}
		}

		#endregion

		#region string VOzIDLoTYP

		private string _voZIdlOTyp;
		[DebuggerNonUserCode]
		[Column(Storage = "_voZIdlOTyp", Name = "vozidlo_typ", DbType = "varchar(10)", CanBeNull = false)]
		public string VozidloTyp
		{
			get
			{
				return _voZIdlOTyp;
			}
			set
			{
				if (value != _voZIdlOTyp)
				{
					_voZIdlOTyp = value;
                    OnPropertyChanged("VozidloTyp");
				}
			}
		}

		#endregion

		#region string ZnAcKA

		private string _znAcKa;
		[DebuggerNonUserCode]
		[Column(Storage = "_znAcKa", Name = "znacka", DbType = "varchar(15)", CanBeNull = false)]
		public string Znacka
		{
			get
			{
				return _znAcKa;
			}
			set
			{
				if (value != _znAcKa)
				{
					_znAcKa = value;
                    OnPropertyChanged("Znacka");
				}
			}
		}

		#endregion

		#region Children

		private EntitySet<DokumentyVozidel> _doKuMenTyvoZIDEl;
		[Association(Storage = "_doKuMenTyvoZIDEl", OtherKey = "VozidloID", Name = "DOKUMENTY_VOZIDEL_VOZIDLA_FK")]
		[DebuggerNonUserCode]
		public EntitySet<DokumentyVozidel> DokumentyVozidel
		{
			get
			{
				return _doKuMenTyvoZIDEl;
			}
			set
			{
				_doKuMenTyvoZIDEl = value;
			}
		}

		private EntitySet<Jizdy> _jizdY;
		[Association(Storage = "_jizdY", OtherKey = "VozidloID", Name = "JIZDY_VOZIDLA_FK")]
		[DebuggerNonUserCode]
		public EntitySet<Jizdy> Jizdy
		{
			get
			{
				return _jizdY;
			}
			set
			{
				_jizdY = value;
			}
		}


		#endregion

		#region Parents

		private EntityRef<Autoskoly> _autosKoLy;
		[Association(Storage = "_autosKoLy", ThisKey = "AutoskolaID", Name = "VOZIDLA_AUTOSKOLY_FK", IsForeignKey = true)]
		[DebuggerNonUserCode]
		public Autoskoly Autoskoly
		{
			get
			{
				return _autosKoLy.Entity;
			}
			set
			{
				if (value != _autosKoLy.Entity)
				{
					if (_autosKoLy.Entity != null)
					{
						var previousAutosKoLY = _autosKoLy.Entity;
						_autosKoLy.Entity = null;
						previousAutosKoLY.Vozidla.Remove(this);
					}
					_autosKoLy.Entity = value;
					if (value != null)
					{
						value.Vozidla.Add(this);
						_autosKolaID = value.AutoskolaID;
					}
					else
					{
						_autosKolaID = default(uint);
					}
				}
			}
		}

		private EntityRef<TypyVozidel> _typyvoZIDEl;
		[Association(Storage = "_typyvoZIDEl", ThisKey = "VozidloTyp", Name = "VOZIDLA_TYPY_VOZIDEL_FK", IsForeignKey = true)]
		[DebuggerNonUserCode]
		public TypyVozidel TypyVozidel
		{
			get
			{
				return _typyvoZIDEl.Entity;
			}
			set
			{
				if (value != _typyvoZIDEl.Entity)
				{
					if (_typyvoZIDEl.Entity != null)
					{
						var previousTYPYVOzIDel = _typyvoZIDEl.Entity;
						_typyvoZIDEl.Entity = null;
						previousTYPYVOzIDel.Vozidla.Remove(this);
					}
					_typyvoZIDEl.Entity = value;
					if (value != null)
					{
						value.Vozidla.Add(this);
						_voZIdlOTyp = value.Nazev;
					}
					else
					{
						_voZIdlOTyp = default(string);
					}
				}
			}
		}


		#endregion

		#region Attachement handlers

		private void DoKuMenTYVOzIDel_Attach(DokumentyVozidel entity)
		{
			entity.Vozidla = this;
		}

		private void DoKuMenTYVOzIDel_Detach(DokumentyVozidel entity)
		{
			entity.Vozidla = null;
		}

		private void JIZDy_Attach(Jizdy entity)
		{
			entity.Vozidla = this;
		}

		private void JIZDy_Detach(Jizdy entity)
		{
			entity.Vozidla = null;
		}


		#endregion

		#region ctor

		public Vozidla()
		{
			_doKuMenTyvoZIDEl = new EntitySet<DokumentyVozidel>(DoKuMenTYVOzIDel_Attach, DoKuMenTYVOzIDel_Detach);
			_jizdY = new EntitySet<Jizdy>(JIZDy_Attach, JIZDy_Detach);
			_autosKoLy = new EntityRef<Autoskoly>();
			_typyvoZIDEl = new EntityRef<TypyVozidel>();
		}

		#endregion

	}

	[Table(Name = "drivinscools.vyplatnice")]
	public partial class Vyplatnice : INotifyPropertyChanged
	{
		#region INotifyPropertyChanged handling

		public event PropertyChangedEventHandler PropertyChanged;

		protected virtual void OnPropertyChanged(string propertyName)
		{
			if (PropertyChanged != null)
			{
				PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
			}
		}

		#endregion

		#region double CIsTAMZDa

		private double _ciSTamzdA;
		[DebuggerNonUserCode]
		[Column(Storage = "_ciSTamzdA", Name = "cista_mzda", DbType = "double", CanBeNull = false)]
		public double CistaMzda
		{
			get
			{
				return _ciSTamzdA;
			}
			set
			{
				if (value != _ciSTamzdA)
				{
					_ciSTamzdA = value;
                    OnPropertyChanged("CistaMzda");
				}
			}
		}

		#endregion

		#region double DanZPRiJMu

		private double _danZprIJmU;
		[DebuggerNonUserCode]
		[Column(Storage = "_danZprIJmU", Name = "dan_z_prijmu", DbType = "double", CanBeNull = false)]
		public double DanZPrijmu
		{
			get
			{
				return _danZprIJmU;
			}
			set
			{
				if (value != _danZprIJmU)
				{
					_danZprIJmU = value;
                    OnPropertyChanged("DanZPrijmu");
				}
			}
		}

		#endregion

		#region double HodInOvaMZDa

		private double _hodInOvaMzdA;
		[DebuggerNonUserCode]
		[Column(Storage = "_hodInOvaMzdA", Name = "hodinova_mzda", DbType = "double", CanBeNull = false)]
		public double HodinovaMzda
		{
			get
			{
				return _hodInOvaMzdA;
			}
			set
			{
				if (value != _hodInOvaMzdA)
				{
					_hodInOvaMzdA = value;
                    OnPropertyChanged("HodinovaMzda");
				}
			}
		}

		#endregion

		#region double HRubAMZDa

		private double _hrUbAmzdA;
		[DebuggerNonUserCode]
		[Column(Storage = "_hrUbAmzdA", Name = "hruba_mzda", DbType = "double", CanBeNull = false)]
		public double HrubaMzda
		{
			get
			{
				return _hrUbAmzdA;
			}
			set
			{
				if (value != _hrUbAmzdA)
				{
					_hrUbAmzdA = value;
                    OnPropertyChanged("HrubaMzda");
				}
			}
		}

		#endregion

		#region int MeSic

		private int _meSic;
		[DebuggerNonUserCode]
		[Column(Storage = "_meSic", Name = "mesic", DbType = "int", CanBeNull = false)]
		public int Mesic
		{
			get
			{
				return _meSic;
			}
			set
			{
				if (value != _meSic)
				{
					_meSic = value;
                    OnPropertyChanged("Mesic");
				}
			}
		}

		#endregion

		#region int OdUCeNycHHodIn

		private int _odUcENycHhOdIn;
		[DebuggerNonUserCode]
		[Column(Storage = "_odUcENycHhOdIn", Name = "oducenych_hodin", DbType = "int", CanBeNull = false)]
		public int OducenychHodin
		{
			get
			{
				return _odUcENycHhOdIn;
			}
			set
			{
				if (value != _odUcENycHhOdIn)
				{
					_odUcENycHhOdIn = value;
                    OnPropertyChanged("OducenychHodin");
				}
			}
		}

		#endregion

		#region int POCeTDETI

		private int _pocETdeti;
		[DebuggerNonUserCode]
		[Column(Storage = "_pocETdeti", Name = "pocet_deti", DbType = "int", CanBeNull = false)]
		public int PocetDeti
		{
			get
			{
				return _pocETdeti;
			}
			set
			{
				if (value != _pocETdeti)
				{
					_pocETdeti = value;
                    OnPropertyChanged("PocetDeti");
				}
			}
		}

		#endregion

		#region int ROk

		private int _roK;
		[DebuggerNonUserCode]
		[Column(Storage = "_roK", Name = "rok", DbType = "int", CanBeNull = false)]
		public int Rok
		{
			get
			{
				return _roK;
			}
			set
			{
				if (value != _roK)
				{
					_roK = value;
					OnPropertyChanged("Rok");
				}
			}
		}

		#endregion

		#region double SlEvaNADanINADiTe

		private double _slEvaNadAnInadITe;
		[DebuggerNonUserCode]
		[Column(Storage = "_slEvaNadAnInadITe", Name = "sleva_na_dani_na_dite", DbType = "double", CanBeNull = false)]
		public double SlevaNaDaniNaDite
		{
			get
			{
				return _slEvaNadAnInadITe;
			}
			set
			{
				if (value != _slEvaNadAnInadITe)
				{
					_slEvaNadAnInadITe = value;
                    OnPropertyChanged("SlevaNaDaniNaDite");
				}
			}
		}

		#endregion

		#region double SlEvaNADanINAPopLaTnIKA

		private double _slEvaNadAnInapOpLaTnIka;
		[DebuggerNonUserCode]
		[Column(Storage = "_slEvaNadAnInapOpLaTnIka", Name = "sleva_na_dani_na_poplatnika", DbType = "double", CanBeNull = false)]
		public double SlevaNaDaniNaPoplatnika
		{
			get
			{
				return _slEvaNadAnInapOpLaTnIka;
			}
			set
			{
				if (value != _slEvaNadAnInapOpLaTnIka)
				{
					_slEvaNadAnInapOpLaTnIka = value;
                    OnPropertyChanged("SlevaNaDaniNaPoplatnika");
				}
			}
		}

		#endregion

		#region double SocialNi

		private double _socialNi;
		[DebuggerNonUserCode]
		[Column(Storage = "_socialNi", Name = "socialni", DbType = "double", CanBeNull = false)]
		public double Socialni
		{
			get
			{
				return _socialNi;
			}
			set
			{
				if (value != _socialNi)
				{
					_socialNi = value;
                    OnPropertyChanged("Socialni");
				}
			}
		}

		#endregion

		#region uint? UCiteLID

		private uint? _ucIteLid;
		[DebuggerNonUserCode]
		[Column(Storage = "_ucIteLid", Name = "ucitel_id", DbType = "int unsigned")]
		public uint? UcitelID
		{
			get
			{
				return _ucIteLid;
			}
			set
			{
				if (value != _ucIteLid)
				{
					_ucIteLid = value;
                    OnPropertyChanged("UcitelID");
				}
			}
		}

		#endregion

		#region int VYPlatNiceID

		private int _vypLatNiceID;
		[DebuggerNonUserCode]
		[Column(Storage = "_vypLatNiceID", Name = "vyplatnice_id", DbType = "int", IsPrimaryKey = true, CanBeNull = false)]
		public int VyplatniceID
		{
			get
			{
				return _vypLatNiceID;
			}
			set
			{
				if (value != _vypLatNiceID)
				{
					_vypLatNiceID = value;
                    OnPropertyChanged("VyplatniceID");
				}
			}
		}

		#endregion

		#region double ZDrAvOtNi

		private double _zdRAvOtNi;
		[DebuggerNonUserCode]
		[Column(Storage = "_zdRAvOtNi", Name = "zdravotni", DbType = "double", CanBeNull = false)]
		public double Zdravotni
		{
			get
			{
				return _zdRAvOtNi;
			}
			set
			{
				if (value != _zdRAvOtNi)
				{
					_zdRAvOtNi = value;
                    OnPropertyChanged("Zdravotni");
				}
			}
		}

		#endregion

		#region Parents

		private EntityRef<Ucitele> _ucIteLe;
		[Association(Storage = "_ucIteLe", ThisKey = "UcitelID", Name = "VYPLATNICE_UCITELE_FK", IsForeignKey = true)]
		[DebuggerNonUserCode]
		public Ucitele Ucitele
		{
			get
			{
				return _ucIteLe.Entity;
			}
			set
			{
				if (value != _ucIteLe.Entity)
				{
					if (_ucIteLe.Entity != null)
					{
						var previousUCiteLE = _ucIteLe.Entity;
						_ucIteLe.Entity = null;
						previousUCiteLE.Vyplatnice.Remove(this);
					}
					_ucIteLe.Entity = value;
					if (value != null)
					{
						value.Vyplatnice.Add(this);
						_ucIteLid = value.UcitelID;
					}
					else
					{
						_ucIteLid = null;
					}
				}
			}
		}


		#endregion

		#region ctor

		public Vyplatnice()
		{
			_ucIteLe = new EntityRef<Ucitele>();
		}

		#endregion

	}
}
