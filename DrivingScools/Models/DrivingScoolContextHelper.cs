﻿using System;
using System.Data;
using System.Configuration;
using System.Data.Linq.Mapping;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using DbLinq.Data.Linq;
using DbLinq.MySql;
using MySql.Data.MySqlClient;
using MySql.Data;
using System.Threading;

namespace DrivingScools.Models
{
    public class DrivingScoolContextHelper
    {
        private const string ConnectionString = 
            "server=localhost;User Id=root;database=drivinscools;Password=aaa";
        
        private static DrivingScoolContext db = new DrivingScoolContext(new MySqlConnection(
            //"Database=driving_scool; Data Source=192.168.0.153; User Id=pacholik; Password="));
            //"Database=drivingscools; Data Source=localhost; User Id=root; Password=aaaa"));
            "server=localhost;User Id=root;database=drivinscools;Password=aaaa"));
        public static DrivingScoolContext DataContext { get { return db; } }
        

        public static DrivingScoolContext CreateDataContext_()
        {
            //return new DrivingScoolContext(
            //    "server=localhost;User Id=root;database=drivinscools;Password=aaaa");
            return GetThreadScopedDataContextInternal(null, ConnectionString);
        }


        public static DrivingScoolContext CreateDataContext2()
        {
            return new DrivingScoolContext(new MySqlConnection(
                //"Database=driving_scool; Data Source=192.168.0.153; User Id=pacholik; Password="));
                //"Database=drivingscools; Data Source=localhost; User Id=root; Password=aaaa"));
            "server=localhost;User Id=root;database=drivinscools;Password=aaaa"));
        }

        static DrivingScoolContext GetThreadScopedDataContextInternal(string key, string ConnectionString)
        {
            if (key == null)
                key = "__WRSCDC_" + Thread.CurrentContext.ContextID.ToString();

            LocalDataStoreSlot threadData = Thread.GetNamedDataSlot(key);
            object context = null;
            if (threadData != null)
                context = Thread.GetData(threadData);

            if (context == null)
            {
                context = Activator.CreateInstance(typeof(DrivingScoolContext), ConnectionString);

                if (context != null)
                {
                    if (threadData == null)
                        threadData = Thread.AllocateNamedDataSlot(key);

                    Thread.SetData(threadData, context);
                }
            }

            return (DrivingScoolContext)context;
        }
    }

}