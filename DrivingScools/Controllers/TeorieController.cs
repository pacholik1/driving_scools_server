﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using DrivingScools.Models;
using MySql.Data.MySqlClient;
using Newtonsoft.Json.Linq;

namespace DrivingScools.Controllers
{
    public class TeorieController : ApiController
    {        
        //private DrivingScoolContext db = DrivingScoolContextHelper.CreateDataContext();
        //private DrivingScoolContext db = DrivingScoolContextHelper.DataContext;

        //
        // GET: /Studenti/

        public JObject Get(int? student_id = null, int? kurz_id = null, int? ucitel_id = null, int? id = null)
        {
            //Logger.Log("GET /Teorie student_id=" + student_id + ", kurz_id=" + kurz_id + ", ucitel_id=" + ucitel_id + ", id=" + id);
            using (var db = DrivingScoolContextHelper.CreateDataContext2())
            {
                try
                {
                    if (id != null)
                    {
                        if (student_id != null)
                        {
                            return JObject.FromObject(JArray.FromObject(

                            //from s in db.Studenti
                                //where (student_id == null || s.StudentID == student_id)
                                //join k in db.Kurzy on s.KurzID equals k.KurzID into jsk
                                //from sk in jsk
                                //join skt in db.Teorie on sk.KurzID equals skt.KurzID into jskt
                                //from t in jskt
                                //where (kurz_id == null || t.KurzID == kurz_id) &&
                                //      (ucitel_id == null || t.UcitelID == ucitel_id) &&
                                //      (id == null || t.TeorieID == id)

                                from s in db.Studenti
                                join k in db.Kurzy on s.KurzID equals k.KurzID
                                join t in db.Teorie on k.KurzID equals t.KurzID
                                where (kurz_id == null || t.KurzID == kurz_id) &&
                                      (ucitel_id == null || t.UcitelID == ucitel_id) &&
                                      (student_id == null || s.StudentID == student_id) &&
                                      (id == null || t.TeorieID == id)
                                select new
                                {
                                    teorie_id = t.TeorieID,
                                    kurz_id = t.KurzID,
                                    //datum = 
                                    cas_od = t.Od,
                                    cas_do = t.Do,
                                    ucitel_id = t.UcitelID,
                                    typ = t.Typ
                                })[0]);
                        }
                        else
                        {                                                        
                            return JObject.FromObject(JArray.FromObject(
                                from k in db.Kurzy
                                join t in db.Teorie on k.KurzID equals t.KurzID
                                where (kurz_id == null || t.KurzID == kurz_id) &&
                                      (ucitel_id == null || t.UcitelID == ucitel_id) &&
                                      (id == null || t.TeorieID == id)
                                select new
                                {
                                    teorie_id = t.TeorieID,
                                    kurz_id = t.KurzID,
                                    //datum = 
                                    cas_od = t.Od,
                                    cas_do = t.Do,
                                    ucitel_id = t.UcitelID,
                                    typ = t.Typ
                                })[0]);
                        }
                    }
                    else
                    {
                        if (student_id != null)
                            return JObject.FromObject(new
                            {
                                teorie =
                                    (from d in db.Dochazka                                    
                                    join t in db.Teorie on d.TeorieID equals t.TeorieID
                                    where (kurz_id == null || t.KurzID == kurz_id) &&
                                          (ucitel_id == null || t.UcitelID == ucitel_id) &&
                                          (student_id == null || d.StudentID == student_id) &&
                                          (id == null || t.TeorieID == id)
                                    select new
                                    {
                                        teorie_id = t.TeorieID,
                                        kurz_id = t.KurzID,
                                        //datum = 
                                        cas_od = t.Od,
                                        cas_do = t.Do,
                                        ucitel_id = t.UcitelID,
                                        typ = t.Typ
                                    }).Distinct()
                            });
                        else
                            return JObject.FromObject(new
                            {
                                teorie =
                                    (from k in db.Kurzy
                                     join t in db.Teorie on k.KurzID equals t.KurzID
                                     where (kurz_id == null || t.KurzID == kurz_id) &&
                                           (ucitel_id == null || t.UcitelID == ucitel_id) &&                                           
                                           (id == null || t.TeorieID == id)
                                     select new
                                     {
                                         teorie_id = t.TeorieID,
                                         kurz_id = t.KurzID,
                                         //datum = 
                                         cas_od = t.Od,
                                         cas_do = t.Do,
                                         ucitel_id = t.UcitelID,
                                         typ = t.Typ
                                     }).Distinct()
                            });
                    }
                }
                catch (ArgumentOutOfRangeException)
                {
                    return new JObject();
                }
                catch (MySqlException)
                {
                    throw new HttpResponseException(HttpStatusCode.InternalServerError);
                }
            }
        }

        [System.Web.Http.HttpPost]
        public JObject Post(JObject o)
        {            
            Teorie t;

            using (var db = DrivingScoolContextHelper.CreateDataContext2())
            {
                try
                {
                    db.Teorie.InsertOnSubmit(t = new Teorie()
                    {                        
                        //TeorieID = o.Value<uint>("teorie_id"),
                        KurzID = o.Value<uint>("kurz_id"),
                        Od = o.Value<DateTime>("cas_od"),
                        Do = o.Value<DateTime>("cas_do"),
                        UcitelID = o.Value<uint>("ucitel_id"),
                        Typ = o.Value<string>("typ")
                    });
                    db.SubmitChanges();
                }
                catch (MySqlException)
                {
                    throw new HttpResponseException(HttpStatusCode.InternalServerError);
                }
            }

            return Get(null, null, null, (int?)t.TeorieID);
        }

        public HttpResponseMessage Delete(uint id)
        {
            using (var db = DrivingScoolContextHelper.CreateDataContext2())
            {
                try
                {
                    db.Teorie.DeleteAllOnSubmit(
                        from u in db.Teorie
                        where (u.TeorieID == id)
                        select u
                        );
                    db.SubmitChanges();
                }
                catch (MySqlException)
                {
                    throw new HttpResponseException(HttpStatusCode.InternalServerError);
                }
            }

            HttpResponseMessage response = Request.CreateResponse();
            return response;
        }


    }
}
