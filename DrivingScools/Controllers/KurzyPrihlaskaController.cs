﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Web;
using System.Web.Http;
using DrivingScools.Models;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.pdf.draw;
using MySql.Data.MySqlClient;
using Newtonsoft.Json.Linq;

namespace DrivingScools.Controllers
{
    public class KurzyPrihlaskaController : ApiController
    {
        private Kurzy retrieveKurz(int kurz_id)
        {
            using (var db = DrivingScoolContextHelper.CreateDataContext2())
            {
                try
                {
                    return
                        (from k in db.Kurzy
                            where (k.KurzID == kurz_id)
                            select k).First();
                }
                catch (ArgumentOutOfRangeException)
                {
                    throw new HttpResponseException(HttpStatusCode.NoContent);
                }
                catch (MySqlException)
                {
                    throw new HttpResponseException(HttpStatusCode.BadRequest);
                }
            }
        }

        private IEnumerable<Studenti> retrieveStudenti(int kurz_id)
        {
            using (var db = DrivingScoolContextHelper.CreateDataContext2())
            {
                try
                {
                    return
                        from s in db.Studenti
                        where (s.KurzID == kurz_id) 
                        select s;
                }
                catch (ArgumentOutOfRangeException)
                {
                    throw new HttpResponseException(HttpStatusCode.NoContent);
                }
                catch (MySqlException)
                {
                    throw new HttpResponseException(HttpStatusCode.BadRequest);
                }
            }
        }



        public HttpResponseMessage Get(int kurz_id)
        {
            string path = Path.GetTempFileName();

            var document = new Document(PageSize.A4, 50, 50, 25, 25);

            var output = new FileStream(path, FileMode.Create);
            var writer = PdfWriter.GetInstance(document, output);

            document.Open();

            BaseFont bf = BaseFont.CreateFont(@"C:\WINDOWS\Fonts\TIMES.TTF", BaseFont.IDENTITY_H, true);
            iTextSharp.text.Font fonttitle = new iTextSharp.text.Font(bf, 20);
            iTextSharp.text.Font font = new iTextSharp.text.Font(bf, 10);
            iTextSharp.text.Font fontsubtitle = new iTextSharp.text.Font(bf, 12);

            document.Add(new Paragraph(new Chunk("Přihláška", fonttitle)));
            document.Add(new Paragraph(" "));
            document.Add(new LineSeparator());

            Kurzy kurz = retrieveKurz(kurz_id);
            document.Add(new Paragraph(new Chunk("Typ: " + kurz.Typ, font)));
            document.Add(new Paragraph(new Chunk("ID: " + kurz.IdentifikacniCislo, font)));
            document.Add(new Paragraph(new Chunk("Stav: " + kurz.Stav, font)));
            document.Add(new Paragraph(new Chunk("Od: " + kurz.DatumOd, font)));
            document.Add(new Paragraph(new Chunk("Do: " + kurz.DatumDo, font)));

            document.Add(new Paragraph(" "));
            document.Add(new LineSeparator());
            document.Add(new Paragraph(new Chunk("Zapsaní studenti", fontsubtitle)));

            foreach (Studenti student in retrieveStudenti(kurz_id))
            {
                string line = string.Format("{0} {1}, matriční číslo {2}", student.Jmeno, student.Prijmeni, student.MatricniCislo);
                document.Add(new Paragraph(line, font));
                /*
                document.Add(new Paragraph(new Chunk("Jméno: " + student.Jmeno, font)));
                document.Add(new Paragraph(new Chunk("Příjmení: " + student.Prijmeni, font)));
                document.Add(new Paragraph(new Chunk("Matriční číslo: " + student.MatricniCislo, font)));
                 */
            }

            document.Close();

            HttpResponseMessage result = new HttpResponseMessage(HttpStatusCode.OK);
            var stream = new FileStream(path, FileMode.Open);
            result.Content = new StreamContent(stream);
            //result.Content.Headers.Add("Content-Disposition", "attachment; filename=" + Path.GetFileName(path));
            result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/pdf");
            return result;
        }
    }
}
