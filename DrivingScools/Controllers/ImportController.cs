﻿using DrivingScools.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;

namespace DrivingScools.Controllers
{
    public class ImportController : ApiController
    {
        public HttpResponseMessage Post(uint? autoskola_id = null)
        {
            HttpResponseMessage result = null;
            var httpRequest = HttpContext.Current.Request;
            if (autoskola_id != null && httpRequest.Files.Count == 1)
            {

                var postedFile = httpRequest.Files.Get(0);
                string path = Path.GetTempPath() + postedFile.FileName;
                postedFile.SaveAs(path);

                using (StreamReader file = File.OpenText(path))
                using (JsonTextReader reader = new JsonTextReader(file))
                {                    
                    using (var db = DrivingScoolContextHelper.CreateDataContext2())
                    {                                                
                        try
                        {
                            var autoskola =
                                from a in db.Autoskoly
                                 where a.AutoskolaID == autoskola_id
                                 select a;
                            if (autoskola.Count() == 0)
                                return new HttpResponseMessage(HttpStatusCode.BadRequest);

                            JObject importFile = (JObject)JToken.ReadFrom(reader);

                            #region remove dependent tables
                            db.Jizdy.DeleteAllOnSubmit(
                                    from j in db.Jizdy
                                    select j
                                    );
                            db.DokumentyVozidel.DeleteAllOnSubmit(
                                    from d in db.DokumentyVozidel
                                    select d
                                    );
                            db.DokumentyUcitelu.DeleteAllOnSubmit(
                                    from d in db.DokumentyUcitelu
                                    select d
                                    );
                            db.Dochazka.DeleteAllOnSubmit(
                                    from d in db.Dochazka
                                    select d
                                    );                                                       
                            db.Vyplatnice.DeleteAllOnSubmit(
                                    from v in db.Vyplatnice
                                    select v
                                    );

                            db.Teorie.DeleteAllOnSubmit(
                                    from t in db.Teorie
                                    select t
                                    );
                            db.Studenti.DeleteAllOnSubmit(
                                    from s in db.Studenti
                                    select s
                                    );

                            db.Kurzy.DeleteAllOnSubmit(
                                    from k in db.Kurzy
                                    select k
                                    );
                            db.Ucitele.DeleteAllOnSubmit(
                                    from k in db.Ucitele
                                    select k
                                    );
                            db.Vozidla.DeleteAllOnSubmit(
                                    from v in db.Vozidla
                                    select v
                                    );
                            #endregion

                            importVozidel(importFile, db, (uint)autoskola_id);
                            importUcitele(importFile, db, (uint)autoskola_id);
                            importKurzy(importFile, db, (uint)autoskola_id);                           
                        }
                        catch (AggregateException)
                        {
                            throw new HttpResponseException(HttpStatusCode.InternalServerError);
                        }
                    }
                }
                result = Request.CreateResponse(HttpStatusCode.OK);
            }
            else
            {
                result = Request.CreateResponse(HttpStatusCode.BadRequest);
            }
            return result;
        }

        private void importVozidel(JObject importFile, DrivingScoolContext db, uint autoskola_id)
        {
            var vo = importFile["VOZIDLA"].Children();
            foreach (var o in vo)
            {
                Vozidla v = new Vozidla()
                {
                    //VozidloID = o.Value<uint>("vozidlo_id"),
                    AutoskolaID = autoskola_id,
                    Znacka = o.Value<string>("znacka"),
                    Model = o.Value<string>("model"),
                    Vin = o.Value<string>("vin"),
                    PosledniSTK = o.Value<DateTime>("posledni_stk"),
                    PocatecniStavKm = o.Value<uint>("pocatecni_stav_km"),
                    VozidloTyp = o.Value<string>("vozidlo_typ"),
                    RokVyroby = o.Value<uint>("rok_vyroby"),
                    Spz = o.Value<string>("spz")
                };                
                db.Vozidla.InsertOnSubmit(v);
            }
            db.SubmitChanges();
        }

        private void importUcitele(JObject importFile, DrivingScoolContext db, uint autoskola_id)
        {
            var vo = importFile["UCITELE"].Children();
            List<Ucitele> importedUcitele = new List<Ucitele>();
            foreach (var o in vo)
            {
                Ucitele u = new Ucitele()
                {
                    //UcitelID = o.Value<uint>("ucitel_id"),
                    OsobniCislo = "!TEMP!",
                    Jmeno = o.Value<string>("jmeno"),
                    Prijmeni = o.Value<string>("prijmeni"),
                    AdresaMesto = o.Value<string>("adresa_mesto"),
                    AdresaUlice = o.Value<string>("adresa_ulice"),
                    AdresaCP = o.Value<int>("adresa_cp"),
                    AdresaPSC = o.Value<int>("adresa_psc"),
                    AutoskolaID = autoskola_id,
                    PlatnostOpravneni = o.Value<DateTime?>("platnost_opravneni"),
                    PosledniProhlidka = o.Value<DateTime?>("posledni_prohlidka"),
                    OpravneniPravidla = o.Value<bool>("opravneni_pravidla"),
                    OpravneniZdravoveda = o.Value<bool>("opravneni_zdravoveda"),
                    OpravneniUdrzba = o.Value<bool>("opravneni_udrzba"),
                    OpravneniJizdyA = o.Value<bool>("opravneni_jizdy_a"),
                    OpravneniJizdyB = o.Value<bool>("opravneni_jizdy_b"),
                    OpravneniJizdyC = o.Value<bool>("opravneni_jizdy_c"),
                    OpravneniJizdyD = o.Value<bool>("opravneni_jizdy_d"),
                    OpravneniJizdyT = o.Value<bool>("opravneni_jizdy_t"),
                    PocetDeti = o.Value<int>("pocet_deti"),
                    HodinovaMzda = o.Value<double>("hodinova_mzda")
                };
                importedUcitele.Add(u);
                db.Ucitele.InsertOnSubmit(u);
            }
            db.SubmitChanges();
            foreach (var u in importedUcitele)
            {
                u.OsobniCislo = u.UcitelID.ToString();
            }
            db.SubmitChanges();
        }

        private void importKurzy(JObject importFile, DrivingScoolContext db, uint autoskola_id)
        {
            var vo = importFile["KURZY"].Children();
            List<Kurzy> importedKurzy = new List<Kurzy>();
            foreach (var o in vo)
            {
                Kurzy k = new Kurzy()
                {                    
                    IdentifikacniCislo = "0000000",
                    Typ = o.Value<string>("typ"),
                    AutoskolaID = autoskola_id,
                    Stav = o.Value<string>("stav"),
                    DatumOd = o.Value<DateTime>("datum_od"),
                    DatumDo = o.Value<DateTime>("datum_do")                                                                                
                };
                importedKurzy.Add(k);
                db.Kurzy.InsertOnSubmit(k);
            }
            db.SubmitChanges();
            foreach (var k in importedKurzy)
            {
                k.IdentifikacniCislo = k.Typ + k.DatumOd.Year + (k.KurzID % 100).ToString("D2");
            }
            db.SubmitChanges();   
        }
    }
}
