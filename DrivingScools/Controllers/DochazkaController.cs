﻿using DrivingScools.Models;
using MySql.Data.MySqlClient;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace DrivingScools.Controllers
{
    public class DochazkaController : ApiController
    {
        public Object Get(int? kurz_id = null, int? id = null)
        {            
            using (var db = DrivingScoolContextHelper.CreateDataContext2())
            {
                try
                {
                    if (id != null)
                    {
                        #region /Dochazka/{id}
                        var dochazka =
                            from d in db.Dochazka
                            where d.TeorieID == id                            
                            select d;

                        List<int> result = new List<int>();
                        foreach (var item in dochazka)
                        {
                            result.Add((int)item.StudentID);
                        }
                        return JArray.FromObject(result.ToArray<int>());
                        #endregion
                    }
                    else if (kurz_id != null)
                    {
                        #region /Dochazka?kurz_id={id}
                        Dictionary<uint, int> dictJizdy = new Dictionary<uint, int>();
                        Dictionary<uint, int> dictPravidla = new Dictionary<uint, int>();
                        Dictionary<uint, int> dictUdrzba = new Dictionary<uint, int>();
                        Dictionary<uint, int> dictZdravoveda = new Dictionary<uint, int>();

                        var hodinJizd =
                            (from s in db.Studenti
                            where s.KurzID == (uint)kurz_id
                            join j in db.Jizdy on s.StudentID equals j.StudentID
                            group ((int?)(j.Do.Hour - j.Od.Hour) ?? 0) by j.StudentID into g
                            select new { stId = g.Key, hodin = g.Sum()}).Distinct();
                        foreach (var item in hodinJizd)
                        {
                            dictJizdy.Add(item.stId, item.hodin);                            
                        }

                        var hodinTeorie =
                            (from s in db.Studenti
                             where s.KurzID == (uint)kurz_id
                             join d in db.Dochazka on s.StudentID equals d.StudentID
                             join t in db.Teorie on d.TeorieID equals t.TeorieID
                             group ((int?)(t.Do.Hour - t.Od.Hour) ?? 0) by new { s.StudentID, t.Typ} into g
                             select new { stId = g.Key, hodin = g.Sum() }).Distinct();
                        foreach (var item in hodinTeorie)
                        {
                            switch (item.stId.Typ)
                            {
                                case "pravidla_provozu":
                                    dictPravidla.Add(item.stId.StudentID, item.hodin);
                                    break;
                                case "udrzba":
                                    dictUdrzba.Add(item.stId.StudentID, item.hodin);
                                    break;
                                case "zdravoveda":
                                    dictZdravoveda.Add(item.stId.StudentID, item.hodin);
                                    break;
                                default:
                                    break;
                            }
                        }                                                                        

                        JArray a = new JArray();
                        foreach (var item in hodinJizd)
	                    {
		                    a.Add(new JObject(
                                new JProperty("student_id", item.stId),
                                new JProperty("pravidla_provozu", dictPravidla.ContainsKey(item.stId) ? dictPravidla[item.stId] : 0),
                                new JProperty("zdravoveda", dictZdravoveda.ContainsKey(item.stId) ? dictZdravoveda[item.stId] : 0),
                                new JProperty("udrzba", dictUdrzba.ContainsKey(item.stId) ? dictUdrzba[item.stId] : 0),
                                new JProperty("jizdy", dictJizdy.ContainsKey(item.stId) ? dictJizdy[item.stId] : 0)
                                ));                            
	                    }
                        return new JObject(new JProperty("dochazka",a));
                        #endregion
                    }
                    else
                    {
                        return null;
                        //throw new HttpResponseException(HttpStatusCode.BadRequest);
                    }
                }
                catch (ArgumentOutOfRangeException)                {                    
                    return JArray.FromObject(new int[]{});

                }
                catch (MySqlException)
                {
                    throw new HttpResponseException(HttpStatusCode.InternalServerError);
                }
            }                        
        }              
        
        public Object Put(int? id)
        {            
            if (id == null)
                return new int[] { };
            else
            {
                string str = Request.Content.ReadAsStringAsync().Result;                
                if (str != "")
                {
                    str = str.Substring(1, str.Length - 2);
                    if (str != "")
                    {
                        string[] tokens = str.Split(',');
                        int[] arrayOfStudentId = Array.ConvertAll<string, int>(tokens, int.Parse);
                        deleteAllDochazkaByTeorieId(id);
                        insertIntoDochazka(id, arrayOfStudentId);
                    }
                    else
                    {
                        deleteAllDochazkaByTeorieId(id);                    
                    }
                }
                return Get(id);
            }            
        }

        private void insertIntoDochazka(int? id, int[] arrayOfStudentId)
        {
            using (var db = DrivingScoolContextHelper.CreateDataContext2())
            {
                try
                {
                    foreach (var studentID in arrayOfStudentId)
	                {
		                db.Dochazka.InsertOnSubmit(new Dochazka()
                        {
                            //AutoskolaID = o.Value<uint>("autoskola_id"),
                            TeorieID = (uint)id,
                            StudentID = (uint)studentID                        
                        });
	                }                    
                    db.SubmitChanges();

                }
                catch (SqlException)
                {
                    throw new HttpResponseException(HttpStatusCode.InternalServerError);
                }
            }            
        }

        private void deleteAllDochazkaByTeorieId(int? teorie_id)
        {
            using (var db = DrivingScoolContextHelper.CreateDataContext2())
            {
                try
                {                    
                    db.Dochazka.DeleteAllOnSubmit(
                        from d in db.Dochazka
                        where d.TeorieID == teorie_id                            
                        select d
                        );
                    db.SubmitChanges();
                }
                catch (MySqlException)
                {
                    throw new HttpResponseException(HttpStatusCode.InternalServerError);
                }
            }
        }
    }
}
