using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Mvc.Ajax;
using DrivingScools.Models;
using MySql.Data.MySqlClient;
using Newtonsoft.Json.Linq;

namespace DrivingScools.Controllers
{
    public class StudentiController : ApiController
    {
        //private DrivingScoolContext db = DrivingScoolContextHelper.CreateDataContext();
        //private DrivingScoolContext db = DrivingScoolContextHelper.DataContext;

        //
        // GET: /Studenti/

        public JObject Get(int? autoskola_id = null, int? id = null)
        {
            using (var db = DrivingScoolContextHelper.CreateDataContext2())
            {
                try
                {
                    if (id != null)
                    {
                        return JObject.FromObject(JArray.FromObject(
                            from s in db.Studenti join k in db.Kurzy
                            on s.KurzID equals k.KurzID into joined
                            from j in joined
                            where (autoskola_id == null || j.AutoskolaID == autoskola_id) &&
                                  (id == null || s.StudentID == id)
                            select new
                            {
                                student_id = s.StudentID,
                                jmeno = s.Jmeno,
                                prijmeni = s.Prijmeni,
                                kurz_id = s.KurzID,
                                matricni_cislo = s.MatricniCislo
                            })[0]);
                    }
                    else
                    {
                        return JObject.FromObject(new
                        {
                            studenti =
                            from s in db.Studenti
                            join k in db.Kurzy
                                on s.KurzID equals k.KurzID into joined
                            from j in joined
                            where (autoskola_id == null || j.AutoskolaID == autoskola_id) &&
                                  (id == null || s.StudentID == id)
                            select new
                            {
                                    student_id = s.StudentID,
                                    jmeno = s.Jmeno,
                                    prijmeni = s.Prijmeni,
                                    kurz_id = s.KurzID,
                                    matricni_cislo = s.MatricniCislo
                                }
                        });
                    }
                }
                catch (ArgumentOutOfRangeException)
                {
                    return new JObject();
                }
                catch (MySqlException)
                {
                    throw new HttpResponseException(HttpStatusCode.InternalServerError);
                }

            }
        }

        [System.Web.Http.HttpPost]
        public JObject Post(JObject o)
        {
            Studenti s;

            using (var db = DrivingScoolContextHelper.CreateDataContext2())
            {                
                try
                {
                    db.Studenti.InsertOnSubmit(s = new Studenti()
                    {
                        MatricniCislo = o.Value<string>("matricni_cislo"),
                        Jmeno = o.Value<string>("jmeno"),
                        Prijmeni = o.Value<string>("prijmeni"),
                        KurzID = o.Value<uint>("kurz_id")
                    });                    
                    db.SubmitChanges();                    
                }
                catch (MySqlException)
                {
                    throw new HttpResponseException(HttpStatusCode.InternalServerError);
                }
            }            
            return Get(null, (int?) s.StudentID);
        }

        public JObject Put(JObject o)
        {
            uint id;

            using (var db = DrivingScoolContextHelper.CreateDataContext2())
            {
                id = o.Value<uint>("student_id");
                IQueryable<Studenti> studenti;

                try
                {
                    studenti =
                        from s in db.Studenti
                        where (s.StudentID == id)
                        select s;
                }
                catch (MySqlException)
                {
                    throw new HttpResponseException(HttpStatusCode.InternalServerError);
                }

                studenti.First().StudentID = o.Value<uint>("student_id");
                studenti.First().Jmeno = o.Value<string>("jmeno");
                studenti.First().Prijmeni = o.Value<string>("prijmeni");
                studenti.First().KurzID = o.Value<uint>("kurz_id");
                studenti.First().MatricniCislo = o.Value<string>("matricni_cislo");

                db.SubmitChanges();
            }

            return Get(null, (int?) id);
        }

        public HttpResponseMessage Delete(uint id)
        {
            using (var db = DrivingScoolContextHelper.CreateDataContext2())
            {
                try
                {
                    db.Studenti.DeleteAllOnSubmit(
                        from u in db.Studenti
                        where (u.StudentID == id)
                        select u
                        );
                    db.SubmitChanges();

                }
                catch (MySqlException)
                {
                    throw new HttpResponseException(HttpStatusCode.InternalServerError);
                }
            }
            HttpResponseMessage response = Request.CreateResponse();
            return response;
        }

    }
}
