using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Mvc.Ajax;
using System.Web.UI.WebControls;
using DrivingScools.Models;
using MySql.Data.MySqlClient;
using Newtonsoft.Json.Linq;

namespace DrivingScools.Controllers
{
    public class UciteleController : ApiController
    {
        //private DrivingScoolContext db = DrivingScoolContextHelper.CreateDataContext();
        //private DrivingScoolContext db = DrivingScoolContextHelper.DataContext;

        // GET api/values
        public JObject Get(int? autoskola_id = null, int? id = null)
        {
            using (var db = DrivingScoolContextHelper.CreateDataContext2())
            {
                try
                {
                    if (id != null)
                    {
                        return JObject.FromObject(JArray.FromObject(
                            from u in db.Ucitele
                            where (autoskola_id == null || u.AutoskolaID == autoskola_id) &&
                                  (id == null || u.UcitelID == id)
                            select new
                            {
                                ucitel_id = u.UcitelID,
                                osobni_cislo = u.OsobniCislo,
                                jmeno = u.Jmeno,
                                prijmeni = u.Prijmeni,
                                adresa_mesto = u.AdresaMesto,
                                adresa_ulice = u.AdresaUlice,
                                adresa_cp = u.AdresaCP,
                                adresa_psc = u.AdresaPSC,
                                autoskola_id = u.AutoskolaID,
                                platnost_opravneni = u.PlatnostOpravneni,
                                posledni_prohlidka = u.PosledniProhlidka,
                                opravneni_pravidla = u.OpravneniPravidla,
                                opravneni_zdravoveda = u.OpravneniZdravoveda,
                                opravneni_udrzba = u.OpravneniUdrzba,
                                opravneni_jizdy_a = u.OpravneniJizdyA,
                                opravneni_jizdy_b = u.OpravneniJizdyB,
                                opravneni_jizdy_c = u.OpravneniJizdyC,
                                opravneni_jizdy_d = u.OpravneniJizdyD,
                                opravneni_jizdy_t = u.OpravneniJizdyT,
                                pocet_deti = u.PocetDeti,
                                hodinova_mzda = u.HodinovaMzda                     
                            })[0]);
                    }
                    else
                    {
                        return JObject.FromObject(new
                        {
                            ucitele =
                                from u in db.Ucitele
                                where (autoskola_id == null || u.AutoskolaID == autoskola_id) &&
                                      (id == null || u.UcitelID == id)
                                select new
                                {
                                    ucitel_id = u.UcitelID,
                                    osobni_cislo = u.OsobniCislo,
                                    jmeno = u.Jmeno,
                                    prijmeni = u.Prijmeni,
                                    adresa_mesto = u.AdresaMesto,
                                    adresa_ulice = u.AdresaUlice,
                                    adresa_cp = u.AdresaCP,
                                    adresa_psc = u.AdresaPSC,
                                    autoskola_id = u.AutoskolaID,
                                    platnost_opravneni = u.PlatnostOpravneni,
                                    posledni_prohlidka = u.PosledniProhlidka,
                                    opravneni_pravidla = u.OpravneniPravidla,
                                    opravneni_zdravoveda = u.OpravneniZdravoveda,
                                    opravneni_udrzba = u.OpravneniUdrzba,
                                    opravneni_jizdy_a = u.OpravneniJizdyA,
                                    opravneni_jizdy_b = u.OpravneniJizdyB,
                                    opravneni_jizdy_c = u.OpravneniJizdyC,
                                    opravneni_jizdy_d = u.OpravneniJizdyD,
                                    opravneni_jizdy_t = u.OpravneniJizdyT,
                                    pocet_deti = u.PocetDeti,
                                    hodinova_mzda = u.HodinovaMzda
                                }
                        });
                    }

                }
                catch (ArgumentOutOfRangeException)
                {
                    return new JObject();
                }
                catch (MySqlException)
                {
                    throw new HttpResponseException(HttpStatusCode.InternalServerError);
                }
            }
        }

        [System.Web.Http.HttpPost]
        public JObject Post(JObject o)
        {
            Ucitele u;

            using (var db = DrivingScoolContextHelper.CreateDataContext2())
            {
                try
                {
                    db.Ucitele.InsertOnSubmit(u = new Ucitele()
                    {
                        //UcitelID = o.Value<uint>("ucitel_id"),
                        OsobniCislo = "!TEMP!",
                        Jmeno = o.Value<string>("jmeno"),
                        Prijmeni = o.Value<string>("prijmeni"),
                        AdresaMesto = o.Value<string>("adresa_mesto"),
                        AdresaUlice = o.Value<string>("adresa_ulice"),
                        AdresaCP = o.Value<int>("adresa_cp"),
                        AdresaPSC = o.Value<int>("adresa_psc"),
                        AutoskolaID = o.Value<uint>("autoskola_id"),
                        PlatnostOpravneni = o.Value<DateTime?>("platnost_opravneni"),
                        PosledniProhlidka = o.Value<DateTime?>("posledni_prohlidka"),
                        OpravneniPravidla = o.Value<bool>("opravneni_pravidla"),
                        OpravneniZdravoveda = o.Value<bool>("opravneni_zdravoveda"),
                        OpravneniUdrzba = o.Value<bool>("opravneni_udrzba"),
                        OpravneniJizdyA = o.Value<bool>("opravneni_jizdy_a"),
                        OpravneniJizdyB = o.Value<bool>("opravneni_jizdy_b"),
                        OpravneniJizdyC = o.Value<bool>("opravneni_jizdy_c"),
                        OpravneniJizdyD = o.Value<bool>("opravneni_jizdy_d"),
                        OpravneniJizdyT = o.Value<bool>("opravneni_jizdy_t"),
                        PocetDeti = o.Value<int>("pocet_deti"),
                        HodinovaMzda = o.Value<double>("hodinova_mzda")
                    });
                    db.SubmitChanges();
                    u.OsobniCislo = u.UcitelID.ToString();
                    db.SubmitChanges();
                }
                catch (MySqlException)
                {
                    throw new HttpResponseException(HttpStatusCode.InternalServerError);

                }
            }

            return Get(null, (int?)u.UcitelID);
        }

        public JObject Put(JObject o)
        {
            uint id;

            using (var db = DrivingScoolContextHelper.CreateDataContext2())
            {
                id = o.Value<uint>("ucitel_id");
                IQueryable<Ucitele> ucitele;

                try
                {
                    ucitele =
                        from u in db.Ucitele
                        where (u.UcitelID == id)
                        select u;
                }
                catch (MySqlException)
                {
                    throw new HttpResponseException(HttpStatusCode.InternalServerError);
                }

                ucitele.First().OsobniCislo = o.Value<string>("osobni_cislo");
                ucitele.First().Jmeno = o.Value<string>("jmeno");
                ucitele.First().Prijmeni = o.Value<string>("prijmeni");
                ucitele.First().AdresaMesto = o.Value<string>("adresa_mesto");
                ucitele.First().AdresaUlice = o.Value<string>("adresa_ulice");
                ucitele.First().AdresaCP = o.Value<int>("adresa_cp");
                ucitele.First().AdresaPSC = o.Value<int>("adresa_psc");
                ucitele.First().AutoskolaID = o.Value<uint>("autoskola_id");
                ucitele.First().PlatnostOpravneni = o.Value<DateTime?>("platnost_opravneni");
                ucitele.First().PosledniProhlidka = o.Value<DateTime?>("posledni_prohlidka");
                ucitele.First().OpravneniPravidla = o.Value<bool>("opravneni_pravidla");
                ucitele.First().OpravneniZdravoveda = o.Value<bool>("opravneni_zdravoveda");
                ucitele.First().OpravneniUdrzba = o.Value<bool>("opravneni_udrzba");
                ucitele.First().OpravneniJizdyA = o.Value<bool>("opravneni_jizdy_a");
                ucitele.First().OpravneniJizdyB = o.Value<bool>("opravneni_jizdy_b");
                ucitele.First().OpravneniJizdyC = o.Value<bool>("opravneni_jizdy_c");
                ucitele.First().OpravneniJizdyD = o.Value<bool>("opravneni_jizdy_d");
                ucitele.First().OpravneniJizdyT = o.Value<bool>("opravneni_jizdy_t");
                ucitele.First().PocetDeti = o.Value<int>("pocet_deti");
                ucitele.First().HodinovaMzda = o.Value<double>("hodinova_mzda");

                db.SubmitChanges();
            }

            return Get(null, (int?)id);
        }

        public HttpResponseMessage Delete(uint id)
        {            
            using (var db = DrivingScoolContextHelper.CreateDataContext2())
            {
                try
                {
                    db.DokumentyUcitelu.DeleteAllOnSubmit(
                        from d in db.DokumentyUcitelu
                        where (d.UcitelID == id)
                        select d
                        );
                    db.Dochazka.DeleteAllOnSubmit(
                        from d in db.Dochazka
                        join t in db.Teorie on d.TeorieID equals t.TeorieID
                        where (t.UcitelID == id)
                        select d
                        );
                    db.Teorie.DeleteAllOnSubmit(
                        from t in db.Teorie
                        where (t.UcitelID == id)
                        select t
                        );
                    db.Vyplatnice.DeleteAllOnSubmit(
                        from v in db.Vyplatnice
                        where v.UcitelID == id
                        select v
                        );
                    db.Jizdy.DeleteAllOnSubmit(
                        from j in db.Jizdy
                        where (j.UcitelID == id)
                        select j
                        );
                    db.Ucitele.DeleteAllOnSubmit(
                        from u in db.Ucitele
                        where (u.UcitelID == id)
                        select u
                        );
                    db.SubmitChanges();
                }
                catch (MySqlException)
                {
                    throw new HttpResponseException(HttpStatusCode.InternalServerError);
                }
            }

            HttpResponseMessage response = Request.CreateResponse();
            return response;
        }
    }
}
