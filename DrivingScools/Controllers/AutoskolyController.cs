
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Ajax;
using MySql.Data.MySqlClient;
using Newtonsoft.Json.Linq;
using DrivingScools.Models;
using System.Web.Http;
using System.Net.Http;
using System.Net;

namespace DrivingScools.Controllers
{
    public class AutoskolyController : ApiController
    {
        //private DrivingScoolContext db = DrivingScoolContextHelper.CreateDataContext();
        //private DrivingScoolContext db = DrivingScoolContextHelper.DataContext;

        public JObject Get(int? id = null)
        {

            using (var db = DrivingScoolContextHelper.CreateDataContext2())
            {
                try
                {
                    if (id != null)
                    {
                        return JObject.FromObject(JArray.FromObject(
                            from a in db.Autoskoly
                            where (id == null || a.AutoskolaID == id)
                            select new
                            {
                                autoskola_id = a.AutoskolaID,
                                nazev = a.Nazev,
                                adresa_mesto = a.AdresaMesto,
                                adresa_ulice = a.AdresaUlice,
                                adresa_cp = a.AdresaCP,
                                adresa_psc = a.AdresaPSC,
                                uzivatel_id = a.UzivatelID
                            })[0]);
                    }
                    else
                    {
                        return JObject.FromObject(new
                        {
                            autoskoly =
                                from a in db.Autoskoly
                                where (id == null || a.AutoskolaID == id)
                                select new
                                {
                                    autoskola_id = a.AutoskolaID,
                                    nazev = a.Nazev,
                                    adresa_mesto = a.AdresaMesto,
                                    adresa_ulice = a.AdresaUlice,
                                    adresa_cp = a.AdresaCP,
                                    adresa_psc = a.AdresaPSC,
                                    uzivatel_id = a.UzivatelID
                                }
                        });
                    }
                }
                catch (ArgumentOutOfRangeException)
                {
                    return new JObject();
                }
                catch (MySqlException)
                {
                    throw new HttpResponseException(HttpStatusCode.InternalServerError);
                }
            }
        }

        [System.Web.Http.HttpPost]
        public JObject Post(JObject o)
        {
            Autoskoly a;

            using (var db = DrivingScoolContextHelper.CreateDataContext2())
            {
                try
                {                                        
                    db.Autoskoly.InsertOnSubmit(a = new Autoskoly()
                    {
                        //AutoskolaID = o.Value<uint>("autoskola_id"),
                        Nazev = o.Value<string>("nazev"),
                        AdresaMesto = o.Value<string>("adresa_mesto"),
                        AdresaUlice = o.Value<string>("adresa_ulice"),
                        AdresaCP = o.Value<int>("adresa_cp"),
                        AdresaPSC = o.Value<int>("adresa_psc"),
                        UzivatelID = (uint)1
                    });
                    db.SubmitChanges();

                }
                catch (SqlException)
                {
                    throw new HttpResponseException(HttpStatusCode.InternalServerError);
                }
            }

            return Get((int?)a.AutoskolaID);
        }
    }
}
