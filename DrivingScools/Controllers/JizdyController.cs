﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using DrivingScools.Models;
using MySql.Data.MySqlClient;
using Newtonsoft.Json.Linq;

namespace DrivingScools.Controllers
{
    public class JizdyController : ApiController
    {
        //private DrivingScoolContext db = DrivingScoolContextHelper.CreateDataContext();
        //private DrivingScoolContext db = DrivingScoolContextHelper.DataContext;

        //
        // GET: /Studenti/

        public JObject Get(int? ucitel_id = null, int? vozidlo_id = null, int? student_id = null, int? id = null)
        {
            //Logger.Log("GET /Jizdy student_id=" + student_id.ToString() + ", vozidlo_id=" + vozidlo_id.ToString() + ", ucitel_id=" + ucitel_id.ToString() + ", id=" + id.ToString());
            using (var db = DrivingScoolContextHelper.CreateDataContext2())
            {
                try
                {
                    if (id != null)
                    {
                        return JObject.FromObject(JArray.FromObject(
                            from j in db.Jizdy
                            where (vozidlo_id == null || j.VozidloID == vozidlo_id) &&
                                  (student_id == null || j.StudentID == student_id) &&
                                  (ucitel_id == null || j.UcitelID == ucitel_id) &&
                                  (id == null || j.JizdaID == id)
                            select new
                            {
                                jizda_id = j.JizdaID,
                                vozidlo_id = j.VozidloID,
                                //datum = j.da
                                ucitel_id = j.UcitelID,
                                student_id = j.StudentID,
                                cas_od = j.Od,
                                cas_do = j.Do,
                                najeto = j.Najeto,
                                spotreba = j.Spotreba                                
                            })[0]);
                    }
                    else
                    {
                        return JObject.FromObject(new
                        {
                            jizdy =
                                from j in db.Jizdy
                                where (vozidlo_id == null || j.VozidloID == vozidlo_id) &&
                                      (student_id == null || j.StudentID == student_id) &&
                                      (ucitel_id == null || j.UcitelID == ucitel_id) &&
                                      (id == null || j.JizdaID == id)
                                select new
                                {
                                    jizda_id = j.JizdaID,
                                    vozidlo_id = j.VozidloID,
                                    //datum = j.da
                                    ucitel_id = j.UcitelID,
                                    student_id = j.StudentID,
                                    cas_od = j.Od,
                                    cas_do = j.Do,
                                    najeto = j.Najeto,
                                    spotreba = j.Spotreba                                    
                                }
                        });
                    }
                }
                catch (ArgumentOutOfRangeException)
                {
                    return new JObject();
                }
                catch (MySqlException)
                {
                    throw new HttpResponseException(HttpStatusCode.InternalServerError);
                }
            }
        }

        [System.Web.Http.HttpPost]
        public JObject Post(JObject o)
        {
            Jizdy j;

            using (var db = DrivingScoolContextHelper.CreateDataContext2())
            {
                try
                {
                    db.Jizdy.InsertOnSubmit(j = new Jizdy()
                    {
                        //JizdaID = o.Value<uint>("jizda_id"),
                        VozidloID = o.Value<uint>("vozidlo_id"),
                        UcitelID = o.Value<uint>("ucitel_id"),
                        StudentID = o.Value<uint>("student_id"),
                        Od = o.Value<DateTime>("cas_od"),
                        Do = o.Value<DateTime>("cas_do"),
                        Najeto = o.Value<double>("najeto"),
                        Spotreba = o.Value<double>("spotreba"),
                        KurzID = o.Value<uint>("kurz_id")
                    });
                    db.SubmitChanges();
                }
                catch (AggregateException)
                {
                    throw new HttpResponseException(HttpStatusCode.InternalServerError);
                }
            }

            return Get(null, null, null, (int?)j.JizdaID);
        }

        public HttpResponseMessage Delete(uint id)
        {
            using (var db = DrivingScoolContextHelper.CreateDataContext2())
            {
                try
                {
                    db.Jizdy.DeleteAllOnSubmit(
                        from u in db.Jizdy
                        where (u.JizdaID == id)
                        select u
                        );
                    db.SubmitChanges();
                }
                catch (MySqlException)
                {
                    throw new HttpResponseException(HttpStatusCode.InternalServerError);
                }
            }

            HttpResponseMessage response = Request.CreateResponse();
            return response;
        }

    }
}
