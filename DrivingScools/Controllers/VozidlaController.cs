using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Ajax;
using DbLinq.Schema.Dbml;
using Newtonsoft.Json.Linq;
using DrivingScools.Models;
using System.Web.Http;
using System.Net.Http;
using System.Net;
using MySql.Data.MySqlClient;

namespace DrivingScools.Controllers
{
    public class VozidlaController : ApiController
    {
        //private DrivingScoolContext db = DrivingScoolContextHelper.CreateDataContext();
        //private DrivingScoolContext db = DrivingScoolContextHelper.DataContext;

        public JObject Get(int? autoskola_id = null, int? id = null)
        {
            using (var db = DrivingScoolContextHelper.CreateDataContext2())
            {
                try
                {
                    if (id != null)
                    {
                        return JObject.FromObject(JArray.FromObject(
                            from v in db.Vozidla join j in db.Jizdy
                            on v.VozidloID equals j.VozidloID into joined                            
                            //DefaultEmpty pro pripad ze auto nema zadne jizdy
                            from jv in joined.DefaultIfEmpty()
                            where (autoskola_id == null || v.AutoskolaID == autoskola_id) &&
                                  (id == null || v.VozidloID == id)
                            select new
                            {
                                vozidlo_id = v.VozidloID,
                                autoskola_id = v.AutoskolaID,
                                znacka = v.Znacka,
                                model = v.Model,
                                vin = v.Vin,
                                pocet_km = joined.Select(j => (double?)j.Najeto ?? 0).Sum() + v.PocatecniStavKm,                                
                                posledni_stk = v.PosledniSTK,
                                pocatecni_stav_km = v.PocatecniStavKm,
                                vozidlo_typ = v.VozidloTyp,
                                rok_vyroby = v.RokVyroby,
                                spz = v.Spz,
                                prumerna_spotreba = round(
                                    (joined.Select(j => (double?)j.Najeto*j.Spotreba).Sum() ?? 0)
                                    / (joined.Select(j => (double?)j.Najeto).Sum() ?? 1))
                            })[0]);                        
                    }
                    else
                    {                       
                        return JObject.FromObject(new
                        {
                            vozidla =
                                from v in db.Vozidla
                                where (autoskola_id == null || v.AutoskolaID == autoskola_id) &&
                                      (id == null || v.VozidloID == id)
                                select new
                                {
                                    vozidlo_id = v.VozidloID,
                                    autoskola_id = v.AutoskolaID,
                                    znacka = v.Znacka,
                                    model = v.Model,
                                    vin = v.Vin,
                                    //pocet_km = 
                                    posledni_stk = v.PosledniSTK,
                                    pocatecni_stav_km = v.PocatecniStavKm,
                                    vozidlo_typ = v.VozidloTyp,
                                    rok_vyroby = v.RokVyroby,
                                    spz = v.Spz
                                    //prumerna_spotreba = v.
                                }
                        });
                    }
                }
                catch (ArgumentOutOfRangeException)
                {
                    return new JObject();
                }
                catch (MySqlException)
                {
                    throw new HttpResponseException(HttpStatusCode.InternalServerError);
                }
            }
        }

        private object round(double p)
        {
            return Math.Round(p,2);
        }

        [System.Web.Http.HttpPost]
        public JObject Post(JObject o)
        {
            Vozidla v;

            using (var db = DrivingScoolContextHelper.CreateDataContext2())
            {
                try
                {
                    db.Vozidla.InsertOnSubmit(v = new Vozidla()
                    {
                        //VozidloID = o.Value<uint>("vozidlo_id"),
                        AutoskolaID = o.Value<uint>("autoskola_id"),
                        Znacka = o.Value<string>("znacka"),
                        Model = o.Value<string>("model"),
                        Vin = o.Value<string>("vin"),
                        PosledniSTK = o.Value<DateTime>("posledni_stk"),
                        PocatecniStavKm = o.Value<uint>("pocatecni_stav_km"),
                        VozidloTyp = o.Value<string>("vozidlo_typ"),
                        RokVyroby = o.Value<uint>("rok_vyroby"),
                        Spz = o.Value<string>("spz")
                    });
                    db.SubmitChanges();
                }
                catch (AggregateException)
                {
                    throw new HttpResponseException(HttpStatusCode.InternalServerError);
                }
            }

            return Get(null, (int?)v.VozidloID);
        }

        public JObject Put(JObject o)
        {
            uint id;

            using (var db = DrivingScoolContextHelper.CreateDataContext2())
            {
                id = o.Value<uint>("vozidlo_id");
                IQueryable<Vozidla> vozidla;

                try
                {
                    vozidla =
                        from v in db.Vozidla
                        where (v.VozidloID == id)
                        select v;
                }
                catch (MySqlException)
                {
                    throw new HttpResponseException(HttpStatusCode.InternalServerError);
                }

                //vozidla.First().VozidloID = o.Value<uint>("vozidlo_id");
                vozidla.First().AutoskolaID = o.Value<uint>("autoskola_id");
                vozidla.First().Znacka = o.Value<string>("znacka");
                vozidla.First().Model = o.Value<string>("model");
                vozidla.First().Vin = o.Value<string>("vin");
                vozidla.First().PosledniSTK = o.Value<DateTime>("posledni_stk");
                vozidla.First().PocatecniStavKm = o.Value<uint>("pocatecni_stav_km");
                vozidla.First().VozidloTyp = o.Value<string>("vozidlo_typ");
                vozidla.First().RokVyroby = o.Value<uint>("rok_vyroby");
                vozidla.First().Spz = o.Value<string>("spz");

                db.SubmitChanges();
            }

            return Get(null, (int?)id);
        }

        public HttpResponseMessage Delete(uint id)
        {            
            using (var db = DrivingScoolContextHelper.CreateDataContext2())
            {
                try
                {
                    db.Jizdy.DeleteAllOnSubmit(
                        from j in db.Jizdy
                        where j.VozidloID == id
                        select j
                        );
                    db.DokumentyVozidel.DeleteAllOnSubmit(
                        from d in db.DokumentyVozidel
                        where d.VozidloID == id
                        select d
                        );
                    db.Vozidla.DeleteAllOnSubmit(
                        from u in db.Vozidla
                        where (u.VozidloID == id)
                        select u
                        );
                    db.SubmitChanges();
                }
                catch (MySqlException)
                {
                    throw new HttpResponseException(HttpStatusCode.InternalServerError);
                }
            }

            HttpResponseMessage response = Request.CreateResponse();
            return response;
        }
    }
}
