﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using System.Web.Http;
using DrivingScools.Models;
using MySql.Data.MySqlClient;
using Newtonsoft.Json.Linq;

namespace DrivingScools.Controllers
{
    public class DokumentyUciteleController : ApiController
    {
        //private DrivingScoolContext db = DrivingScoolContextHelper.CreateDataContext();
        //private DrivingScoolContext db = DrivingScoolContextHelper.DataContext;

        private DokumentyUcitelu retrieveDokumentyUcitelu(int dokument_id)
        {
            using (var db = DrivingScoolContextHelper.CreateDataContext2())
            {
                try
                {
                    return
                        (from du in db.DokumentyUcitelu
                            where (du.DokumentID == dokument_id)
                            select du).First();
                }
                catch (InvalidOperationException)
                {
                    throw new HttpResponseException(HttpStatusCode.BadRequest);
                }
                catch (ArgumentOutOfRangeException)
                {
                    throw new HttpResponseException(HttpStatusCode.NoContent);
                }
                catch (MySqlException)
                {
                    throw new HttpResponseException(HttpStatusCode.InternalServerError);
                }
            }
        }

        public HttpResponseMessage Get(int? ucitel_id = null, int? id = null)
        {
            if (id != null)
            {
                DokumentyUcitelu du = retrieveDokumentyUcitelu((int) id);
                string path = Path.GetTempFileName();
                System.IO.File.WriteAllBytes(path, du.Dokument);

                HttpResponseMessage result = new HttpResponseMessage(HttpStatusCode.OK);
                var stream = new FileStream(path, FileMode.Open);
                result.Content = new StreamContent(stream);
                result.Content.Headers.Add("Content-Disposition", "attachment; filename=" + du.DokumentNazev);
                result.Content.Headers.ContentType = new MediaTypeHeaderValue("html/text");
                return result;
            }

            if (ucitel_id != null)
            {
                JObject o;
                using (var db = DrivingScoolContextHelper.CreateDataContext2())
                {
                    try
                    {
                        o = JObject.FromObject(new
                        {
                            dokumenty =
                                from dv in db.DokumentyUcitelu
                                join u in db.Ucitele
                                    on dv.UcitelID equals u.UcitelID into joined
                                from j in joined
                                where (j.UcitelID == ucitel_id)
                                select new
                                {
                                    dokument_id = dv.DokumentID,
                                    ucitel_id = j.UcitelID,
                                    dokument_nazev = dv.DokumentNazev,
                                    datum_vlozeni = dv.DatumVlozeni,
                                }
                        });
                    }
                    catch (ArgumentOutOfRangeException)
                    {
                        throw new HttpResponseException(HttpStatusCode.BadRequest);
                    }
                    catch (MySqlException)
                    {
                        throw new HttpResponseException(HttpStatusCode.InternalServerError);
                    }
                }
                HttpResponseMessage result = new HttpResponseMessage(HttpStatusCode.OK);
                result.Content = new StringContent(o.ToString());
                result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                return result;
            }
            throw new HttpResponseException(HttpStatusCode.BadRequest);
        }

        private void addFileToDb(uint ucitel_id, string path)
        {
            DokumentyUcitelu d;

            using (var db = DrivingScoolContextHelper.CreateDataContext2())
            {
                try
                {
                    db.DokumentyUcitelu.InsertOnSubmit(d = new DokumentyUcitelu() 
                    {
                        DatumVlozeni = DateTime.Now,
                        Dokument = System.IO.File.ReadAllBytes(path),
                        DokumentNazev = Path.GetFileName(path),
                        UcitelID = ucitel_id,
                    });
                    db.SubmitChanges();
                }
                catch (MySqlException)
                {
                    throw new HttpResponseException(HttpStatusCode.InternalServerError);
                }
            }


            //HttpResponseMessage response = Request.CreateResponse();
            //return response;

        }

        public HttpResponseMessage Post(uint ucitel_id)
        {
            HttpResponseMessage result = null;
            var httpRequest = HttpContext.Current.Request;
            if (httpRequest.Files.Count > 0)
            {
                var docfiles = new List<string>();
                foreach (string file in httpRequest.Files)
                {
                    var postedFile = httpRequest.Files[file];
                    string path = Path.GetTempPath() + postedFile.FileName;
                    //var filePath = HttpContext.Current.Server.MapPath("~/" + postedFile.FileName);
                    postedFile.SaveAs(path);

                    docfiles.Add(path);
                    addFileToDb(ucitel_id, path);
                }
                result = Request.CreateResponse(HttpStatusCode.Created, docfiles);
            }
            else
            {
                result = Request.CreateResponse(HttpStatusCode.BadRequest);
            }
            return result;
        }

        public HttpResponseMessage Delete(uint id)
        {            
            using (var db = DrivingScoolContextHelper.CreateDataContext2())
            {
                try
                {
                    db.DokumentyUcitelu.DeleteAllOnSubmit(
                        from u in db.DokumentyUcitelu
                        where (u.DokumentID == id)
                        select u
                        );
                    db.SubmitChanges();
                }
                catch (MySqlException)
                {
                    throw new HttpResponseException(HttpStatusCode.InternalServerError);
                }
            }

            HttpResponseMessage response = Request.CreateResponse();
            return response;
        }


    }
}
