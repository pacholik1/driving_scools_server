﻿using DrivingScools.Models;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.pdf.draw;
using MySql.Data.MySqlClient;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web.Http;

namespace DrivingScools.Controllers
{
    public class VyplatniceController : ApiController
    {
        public JObject Get(int? autoskola_id)
        {
            using (var db = DrivingScoolContextHelper.CreateDataContext2())
            {
                try
                {
                    return JObject.FromObject(new
                    {
                        vyplatnice =
                        (from u in db.Ucitele
                         where u.AutoskolaID == autoskola_id
                         join v in db.Vyplatnice on u.UcitelID equals v.UcitelID into j
                         from uv in j
                         select new { rok = uv.Rok, mesic = uv.Mesic }).Distinct()
                    });
                }
                catch (ArgumentOutOfRangeException)
                {
                    return new JObject();
                }
                catch (MySqlException)
                {
                    throw new HttpResponseException(HttpStatusCode.InternalServerError);
                }
            }
        }

        private void insert(Vyplatnice v)
        {
            using (var db = DrivingScoolContextHelper.CreateDataContext2())
            {
                try
                {
                    db.Vyplatnice.InsertOnSubmit(v);
                    db.SubmitChanges();
                }
                catch (MySqlException)
                {
                    throw new HttpResponseException(HttpStatusCode.BadRequest);
                }
            }
        }
        
        public HttpResponseMessage Get(int autoskola_id, int rok, int mesic, int? pregenerovat = null) 
        {
            HttpResponseMessage result;
            string path = Path.GetTempFileName();

            if (pregenerovat != null && pregenerovat != 0)
            {
                deleteLastMonth(autoskola_id);
                generateLastMonth(autoskola_id);
            }                        
            
            IEnumerable<VyplatniceUcitele> data = retrieveVyplatnice(autoskola_id, rok, mesic);            

            if (data.Count() > 0)
            {
                generateVyplatnicePdf(path, data);
                result = new HttpResponseMessage(HttpStatusCode.OK);
                var stream = new FileStream(path, FileMode.Open);
                result.Content = new StreamContent(stream);
                result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/pdf");
            }
            else
            {
                result = new HttpResponseMessage(HttpStatusCode.NotFound);
            }            
            return result;
        }

        private void generateVyplatnicePdf(string path, IEnumerable<VyplatniceUcitele> data)
        {
            var doc = new Document(PageSize.A4, 50, 50, 25, 25);

            var output = new FileStream(path, FileMode.Create);
            var writer = PdfWriter.GetInstance(doc, output);

            doc.Open();

            BaseFont bf = BaseFont.CreateFont(@"C:\WINDOWS\Fonts\TIMES.TTF", BaseFont.IDENTITY_H, true);
            iTextSharp.text.Font fonttitle = new iTextSharp.text.Font(bf, 20);
            iTextSharp.text.Font font = new iTextSharp.text.Font(bf, 10);
            iTextSharp.text.Font fontsubtitle = new iTextSharp.text.Font(bf, 12);

            string headline = String.Format("Výplatnice pro {0}. měsíc roku {1}", 
                data.First().vyplatnice.Mesic, data.First().vyplatnice.Rok);
            doc.Add(new Paragraph(new Chunk(headline, fonttitle)));
            doc.Add(new Paragraph(" "));
            doc.Add(new LineSeparator());
            
            foreach (VyplatniceUcitele vyplatnice in data)
            {
                doc.Add(new Paragraph(new Chunk("Jméno: " + vyplatnice.ucitel.Jmeno, font)));
                doc.Add(new Paragraph(new Chunk("Přijmení: " + vyplatnice.ucitel.Prijmeni, font)));
                doc.Add(new Paragraph(new Chunk("Osobní číslo: " + vyplatnice.ucitel.OsobniCislo, font)));
                doc.Add(new Paragraph(new Chunk("Odučených hodin: " + vyplatnice.vyplatnice.OducenychHodin, font)));
                doc.Add(new Paragraph(new Chunk("Hodinová mzda: " + vyplatnice.vyplatnice.HodinovaMzda, font)));
                doc.Add(new Paragraph(new Chunk("Hrubá mzda: " + vyplatnice.vyplatnice.HrubaMzda, font)));
                doc.Add(new Paragraph(new Chunk("Čistá mzda: " + vyplatnice.vyplatnice.CistaMzda, font)));
                doc.Add(new Paragraph(new Chunk("Sociální: " + vyplatnice.vyplatnice.Socialni, font)));
                doc.Add(new Paragraph(new Chunk("Zdravotní: " + vyplatnice.vyplatnice.Zdravotni, font)));
                doc.Add(new Paragraph(new Chunk("Daň z příjmu: " + vyplatnice.vyplatnice.DanZPrijmu, font)));
                doc.Add(new Paragraph(new Chunk("Sleva na poplatníka: " + vyplatnice.vyplatnice.SlevaNaDaniNaPoplatnika, font)));
                doc.Add(new Paragraph(new Chunk("Sleva na dítě: " + vyplatnice.vyplatnice.SlevaNaDaniNaDite, font)));
                doc.Add(new Paragraph(new Chunk("Počet dětí: " + vyplatnice.vyplatnice.PocetDeti, font)));
                doc.Add(new Paragraph(" "));
                doc.Add(new LineSeparator());                              
            }            

            doc.Close();
        }

        private IEnumerable<VyplatniceUcitele> retrieveVyplatnice(int autoskola_id, int rok, int mesic)
        {
            using (var db = DrivingScoolContextHelper.CreateDataContext2())
            {
                try
                {
                    var vyplatniceUcitelu =
                        from u in db.Ucitele
                        where u.AutoskolaID == autoskola_id
                        join v in db.Vyplatnice on u.UcitelID equals v.UcitelID into j
                        from uv in j
                        where uv.Rok == rok && uv.Mesic == mesic
                        select new {uv, u};


                    List<VyplatniceUcitele> list = new List<VyplatniceUcitele>();
                    foreach (var item in vyplatniceUcitelu)
                    {
                        Vyplatnice vy = item.uv;
                        Ucitele uc = item.u;
                        list.Add(new VyplatniceUcitele(vy, uc));
                    }                    
                    return list;                                       
                }
                catch (ArgumentOutOfRangeException)
                {
                    throw new HttpResponseException(HttpStatusCode.NoContent);
                }
                catch (MySqlException)
                {
                    throw new HttpResponseException(HttpStatusCode.BadRequest);
                }
            }
        }

        private class VyplatniceUcitele
        {
            public Vyplatnice vyplatnice;
            public Ucitele ucitel;

            public VyplatniceUcitele(Vyplatnice vyplatnice, Ucitele ucitel)
            {
                this.vyplatnice = vyplatnice;
                this.ucitel = ucitel;
            }
        }

        private void deleteLastMonth(int autoskola_id)
        {
            using (var db = DrivingScoolContextHelper.CreateDataContext2())
            {
                try
                {
                    var today = DateTime.Today;
                    var month = new DateTime(today.Year, today.Month, 1);
                    var first = month.AddMonths(-1);
                    var last = month.AddDays(-1);

                    db.Vyplatnice.DeleteAllOnSubmit(
                        from v in db.Vyplatnice
                        join u in db.Ucitele on v.UcitelID equals u.UcitelID
                        where
                            v.Rok == first.Year &&
                            v.Mesic == first.Month &&
                            u.AutoskolaID == autoskola_id
                        select v
                        );
                    db.SubmitChanges();
                }
                catch (MySqlException)
                {
                    throw new HttpResponseException(HttpStatusCode.InternalServerError);
                }
            }
        }

        private void generateLastMonth(int autoskola_id)
        {
            using (var db = DrivingScoolContextHelper.CreateDataContext2())
            {
                try
                {
                    var today = DateTime.Today;
                    var month = new DateTime(today.Year, today.Month, 1);
                    var first = month.AddMonths(-1);
                    var last = month.AddDays(-1);
                    
                    Dictionary<uint, int> dictTeorie = new Dictionary<uint, int>();
                    var hodinTeorie =
                        from t in db.Teorie
                        where t.Od.Date >= first && t.Do <= last
                        group ((int?)(t.Do.Hour - t.Od.Hour) ?? 0) by t.UcitelID into g
                        select new { ucitelID = g.Key, pocetHodin = g.Sum() };
                    foreach (var item in hodinTeorie)
                    {
                        dictTeorie.Add(item.ucitelID, item.pocetHodin);
                    }

                    var ucitele =
                        from j in db.Jizdy
                        where j.Od.Date >= first && j.Do <= last
                        group ((int?)(j.Do.Hour - j.Od.Hour) ?? 0) by j.UcitelID into g
                        select new { ucitelID = g.Key, pocetHodin = g.Sum() };                    

                    foreach (var ucitel in ucitele)
                    {
                        Ucitele ucitelInfo =
                            (from u in db.Ucitele
                             where u.UcitelID == ucitel.ucitelID
                             select u).First();
                        if (ucitelInfo.AutoskolaID == autoskola_id)
                        {
                            int odpracovanychHodin = ucitel.pocetHodin 
                                + (dictTeorie.ContainsKey(ucitel.ucitelID) ? dictTeorie[ucitel.ucitelID] : 0);
                            double mzdaZaHodinu = ucitelInfo.HodinovaMzda;
                            double hrubaMzda = odpracovanychHodin * mzdaZaHodinu;
                            double superHrubaMzda = hrubaMzda * 1.34;
                            superHrubaMzda = Math.Ceiling(superHrubaMzda / 100) * 100;
                            double dan = superHrubaMzda * 0.15;
                            int pocetDeti = ucitelInfo.PocetDeti;
                            dan = dan - 1970 - (1117 * pocetDeti);
                            double socialni = hrubaMzda * 0.065;
                            double zdravotni = hrubaMzda * 0.045;
                            double cistaMzda = hrubaMzda - socialni - zdravotni - dan;                            

                            insert(new Vyplatnice()
                            {
                                Rok = (int)first.Year,
                                Mesic = (int)first.Month,
                                OducenychHodin = (int)odpracovanychHodin,
                                HodinovaMzda = (double)mzdaZaHodinu,
                                PocetDeti = (int)pocetDeti,
                                HrubaMzda = (double)hrubaMzda,
                                CistaMzda = (double)cistaMzda,
                                Socialni = (double)socialni,
                                Zdravotni = (double)zdravotni,
                                DanZPrijmu = (double)dan,
                                SlevaNaDaniNaDite = (double)(967.0 * pocetDeti),
                                SlevaNaDaniNaPoplatnika = (double)1970.0,
                                UcitelID = (uint?)ucitel.ucitelID
                            });
                        }
                    }
                }
                catch (MySqlException)
                {
                    throw new HttpResponseException(HttpStatusCode.BadRequest);
                }
            }
        }
    }
}
