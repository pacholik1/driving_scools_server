using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Ajax;
using Newtonsoft.Json.Linq;
using DrivingScools.Models;
using System.Web.Http;
using System.Net.Http;
using MySql.Data.MySqlClient;
using System.Net;

namespace DrivingScools.Controllers
{
    public class KurzyController : ApiController
    {
        //private DrivingScoolContext db = DrivingScoolContextHelper.CreateDataContext();
        //private DrivingScoolContext db = DrivingScoolContextHelper.DataContext;

        public JObject Get(int? autoskola_id = null, int? id = null)
        {
            using (var db = DrivingScoolContextHelper.CreateDataContext2())
            {                
                try
                {
                    if (id != null)
                    {
                        return JObject.FromObject(JArray.FromObject(
                            from k in db.Kurzy
                            where (autoskola_id == null || k.AutoskolaID == autoskola_id) &&
                                  (id == null || k.KurzID == id)
                            select new
                            {
                                kurz_id = k.KurzID,
                                identifikacni_cislo = k.IdentifikacniCislo,
                                typ = k.Typ,
                                autoskola_id = k.AutoskolaID,
                                stav = k.Stav,
                                datum_od = k.DatumOd,
                                datum_do = k.DatumDo
                            })[0]);
                    }
                    else
                    {
                        return JObject.FromObject(new
                        {
                            kurzy =
                                from k in db.Kurzy
                                where (autoskola_id == null || k.AutoskolaID == autoskola_id) &&
                                      (id == null || k.KurzID == id)
                                select new
                                {
                                    kurz_id = k.KurzID,
                                    identifikacni_cislo = k.IdentifikacniCislo,
                                    typ = k.Typ,
                                    autoskola_id = k.AutoskolaID,
                                    stav = k.Stav,
                                    datum_od = k.DatumOd,
                                    datum_do = k.DatumDo
                                }
                        });
                    }
                }
                catch (ArgumentOutOfRangeException)
                {
                    return new JObject();
                    //throw new HttpResponseException(HttpStatusCode.NotFound);
                }
                catch (MySqlException)
                {
                    throw new HttpResponseException(HttpStatusCode.BadRequest);
                }
            }
        }

        [System.Web.Http.HttpPost]
        public JObject Post(JObject o)
        {
            Kurzy k;

            using (var db = DrivingScoolContextHelper.CreateDataContext2())
            {
                try
                {
                    db.Kurzy.InsertOnSubmit(k = new Kurzy()
                    {
                        IdentifikacniCislo = "0000000",
                        Typ = o.Value<string>("typ"),
                        AutoskolaID = o.Value<uint>("autoskola_id"),
                        Stav = o.Value<string>("stav"),
                        DatumOd = o.Value<DateTime>("datum_od"),
                        DatumDo = o.Value<DateTime>("datum_do")
                    });
                    db.SubmitChanges();
                    k.IdentifikacniCislo = k.Typ + k.DatumOd.Year + (k.KurzID%100).ToString("D2");
                    db.SubmitChanges();
                }
                catch (AggregateException)
                {
                    throw new HttpResponseException(HttpStatusCode.InternalServerError);
                }
            }

            return Get(null, (int?)k.KurzID);
        }

        public JObject Put(JObject o)
        {
            using (var db = DrivingScoolContextHelper.CreateDataContext2())
            {

                uint id = o.Value<uint>("kurz_id");
                IQueryable<Kurzy> kurzy;

                try
                {
                    kurzy =
                        from k in db.Kurzy
                        where (k.KurzID == id)
                        select k;
                }
                catch (MySqlException)
                {
                    throw new HttpResponseException(HttpStatusCode.InternalServerError);
                }

                //kurzy.First().KurzyID = o.Value<uint>("kurz_id");
                kurzy.First().IdentifikacniCislo = o.Value<string>("identifikacni_cislo");
                kurzy.First().Typ = o.Value<string>("typ");
                kurzy.First().AutoskolaID = o.Value<uint>("autoskola_id");
                kurzy.First().Stav = o.Value<string>("stav");
                kurzy.First().DatumOd = o.Value<DateTime>("datum_od");
                kurzy.First().DatumDo = o.Value<DateTime>("datum_do");

                db.SubmitChanges();

                return Get(null, (int?) id);
            }
        }

        public HttpResponseMessage Delete(uint id)
        {
            using (var db = DrivingScoolContextHelper.CreateDataContext2())
            {
                try
                {
                    db.Jizdy.DeleteAllOnSubmit(
                        from j in db.Jizdy
                        where (j.KurzID == id)
                        select j
                        );
                    db.Dochazka.DeleteAllOnSubmit(
                        from d in db.Dochazka
                        join t in db.Teorie on d.TeorieID equals t.TeorieID
                        where (t.KurzID == id)
                        select d
                        );
                    db.Teorie.DeleteAllOnSubmit(
                        from t in db.Teorie
                        where (t.KurzID == id)
                        select t
                        );                    
                    db.Studenti.DeleteAllOnSubmit(
                        from s in db.Studenti
                        where (s.KurzID == id)
                        select s
                        );
                    db.Kurzy.DeleteAllOnSubmit(
                        from u in db.Kurzy
                        where (u.KurzID == id)
                        select u
                        );
                    db.SubmitChanges();
                }
                catch (MySqlException)
                {
                    throw new HttpResponseException(HttpStatusCode.InternalServerError);
                }
            }

            HttpResponseMessage response = Request.CreateResponse();
            return response;
        }
    }
}
