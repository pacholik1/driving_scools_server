﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using DrivingScools.Models;
using MySql.Data.MySqlClient;
using Newtonsoft.Json.Linq;

namespace DrivingScools.Controllers
{
    public class UzivateleController : ApiController
    {        
        //private DrivingScoolContext db = DrivingScoolContextHelper.CreateDataContext();
        //private DrivingScoolContext db = DrivingScoolContextHelper.DataContext;

        [System.Web.Http.HttpPost]
        public JObject Post(JObject o)
        {
            using (var db = DrivingScoolContextHelper.CreateDataContext2())
            {
                Uzivatele newU;

                try
                {
                    db.Uzivatele.InsertOnSubmit(newU = new Uzivatele()
                    {
                        //UzivatelID = o.Value<uint>("uzivatel_id"),
                        UzivatelskeJmeno = o.Value<string>("uzivatelske_jmeno"),
                        Heslo = o.Value<string>("heslo"),
                        Role = o.Value<string>("role")
                    });
                    db.SubmitChanges();

                    return JObject.FromObject(JArray.FromObject(
                        from u in db.Uzivatele
                        where (u.UzivatelID == newU.UzivatelID)
                        select new
                        {
                            uzivatel_id = u.UzivatelID,
                            uzivatelske_jmeno = u.UzivatelskeJmeno,
                            heslo = u.Heslo,
                            role = u.Role,
                        })[0]);
                }
                catch (ArgumentOutOfRangeException)
                {
                    return new JObject();
                }
                catch (MySqlException)
                {
                    throw new HttpResponseException(HttpStatusCode.InternalServerError);
                }
            }
        }

        public HttpResponseMessage Delete(uint id)
        {
            using (var db = DrivingScoolContextHelper.CreateDataContext2())
            {
                try
                {                    
                    db.Uzivatele.DeleteAllOnSubmit(
                        from u in db.Uzivatele
                        where (u.UzivatelID == id)
                        select u
                        );
                    db.SubmitChanges();
                }
                catch (MySqlException)
                {
                    throw new HttpResponseException(HttpStatusCode.InternalServerError);
                }
            }

            HttpResponseMessage response = Request.CreateResponse();
            return response;
        }
    }
}
