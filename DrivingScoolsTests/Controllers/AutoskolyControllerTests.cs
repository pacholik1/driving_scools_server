﻿using System;
using NUnit.Framework;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Web.Http.SelfHost;
using System.Web.Http;
using Newtonsoft.Json.Linq;

namespace DrivingScoolsTests.Controllers
{
    [TestFixture]
    public class AutoskolyControllerTest
    {
        [Test]
        public void Get_RequestCorrectSchoolId_ReturnHttpCodeOk()
        {
            HttpStatusCode actualStatusCode = AssertHelper.SendRequestGetCode
                (HttpMethod.Get, "{controller}/{id}", "http://localhost/autoskoly/1");
            Assert.AreEqual(HttpStatusCode.OK, actualStatusCode);
        }

        [Test]
        [Explicit("Not implemented - low priority")]
        public void Get_RequestNotExistingSchoolId_ReturnHttpCodeNotFound()
        {
            HttpStatusCode actualStatusCode = AssertHelper.SendRequestGetCode
                (HttpMethod.Get, "{controller}/{id}", "http://localhost/autoskoly/2");
            Assert.AreEqual(HttpStatusCode.NotFound, actualStatusCode);
        }

        [Test]
        public void Get_RequestAllSchools_ReturnHttpCodeOk()
        {
            HttpStatusCode actualStatusCode = AssertHelper.SendRequestGetCode
                (HttpMethod.Get, "{controller}", "http://localhost/autoskoly");
            Assert.AreEqual(HttpStatusCode.OK, actualStatusCode);
        }

        [Test]
        public void Get_RequestCorrectId_GetCorrectValues()
        {
            int actualAutoskolaId;
            string actualNazev;
            string actualAdresaMesto;
            string actualAdresaUlice;
            int actualAdresaCP;
            int actualAdresaPSC;
            int actualUzivatelID;

            HttpSelfHostConfiguration config
                = new HttpSelfHostConfiguration("http://localhost/");
            config.Routes.MapHttpRoute(
                name: "Default",
                routeTemplate: "{controller}/{id}"
                );

            using (HttpSelfHostServer server = new HttpSelfHostServer(config))
            using (HttpClient client = new HttpClient())
            {
                server.OpenAsync().Wait();

                using (HttpRequestMessage request =
                    new HttpRequestMessage(HttpMethod.Get, "http://localhost/autoskoly/1"))
                using (HttpResponseMessage response = client.SendAsync(request).Result)
                {
                    string json = response.Content.ReadAsStringAsync().Result;
                    dynamic data = JObject.Parse(json);
                    actualNazev = data.nazev;
                    actualAutoskolaId = data.autoskola_id;
                    actualAdresaMesto = data.adresa_mesto;
                    actualAdresaUlice = data.adresa_ulice;
                    actualAdresaCP = data.adresa_cp;
                    actualAdresaPSC = data.adresa_psc;
                    actualUzivatelID = data.uzivatel_id;
                }
                server.CloseAsync().Wait();
            }
            Assert.AreEqual("Autoškola DPMP", actualNazev);
            Assert.AreEqual(1, actualAutoskolaId);            
            Assert.AreEqual("Pardubice", actualAdresaMesto);
            Assert.AreEqual("Teplého", actualAdresaUlice);
            Assert.AreEqual(2141, actualAdresaCP);
            Assert.AreEqual(53220, actualAdresaPSC);
            Assert.AreEqual(1, actualUzivatelID);
        }

        [Test]
        [Explicit("Just add new record")]
        public void Post_SendNewValues_GetOkHttpCode()
        {
            string json = "{\"nazev\":\"testNazev\",\"adresa_mesto\":\"testAdresa\",\"adresa_ulice\":\"testUlice\",\"adresa_cp\":1234,\"adresa_psc\":12345,\"uzivatel_id\":1}";
            HttpStatusCode actualHttpCode = AssertHelper.SendJsonGetCode(
                "{controller}", "http://localhost/autoskoly", json);
            Assert.AreEqual(HttpStatusCode.OK, actualHttpCode);
        }

        private HttpClient getJsonClient()
        {
            HttpClient c = new HttpClient();
            c.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            return c;
        }

        private HttpRequestMessage getJsonRequest(string uri, string json)
        {
            HttpRequestMessage req = new HttpRequestMessage(HttpMethod.Post, uri);
            req.Content = new StringContent(json, Encoding.UTF8, "application/json");
            return req;
        }
    }
}
