﻿using System;
using NUnit.Framework;
using System.Net;
using System.Net.Http;
using System.Web.Http.SelfHost;
using System.Web.Http;
using Newtonsoft.Json.Linq;

namespace DrivingScoolsTests.Controllers
{
    [TestFixture]
    public class StudentiControllerTests
    {
        [Test]
        public void Get_RequestAllStudents_ReturnHttpCodeOk()
        {
            HttpStatusCode actualStatusCode = AssertHelper.SendRequestGetCode
                (HttpMethod.Get, "{controller}", "http://localhost/studenti");
            Assert.AreEqual(HttpStatusCode.OK, actualStatusCode);
        }

        [Test]
        public void Get_RequestCorrectStudentId_ReturnHttpCodeOk()
        {
            HttpStatusCode actualStatusCode = AssertHelper.SendRequestGetCode
                (HttpMethod.Get, "{controller}/{id}", "http://localhost/studenti/9");
            Assert.AreEqual(HttpStatusCode.OK, actualStatusCode);
        }

        [Test]
        [Explicit("Not implemented - low priority")]
        public void Get_RequestNotExistingStudentId_ReturnHttpCodeNotFound()
        {
            HttpStatusCode actualStatusCode = AssertHelper.SendRequestGetCode
                (HttpMethod.Get, "{controller}/{id}", "http://localhost/studenti/-1");
            Assert.AreEqual(HttpStatusCode.NotFound, actualStatusCode);
        }

        [Test]
        public void Get_RequestCorrectStudentId_GetCorrectValues()
        {
            int actualStudentId;
            int actualMatricniCislo;
            string actualJmeno;
            string actualPrijmeni;
            int actualKurzId;

            HttpSelfHostConfiguration config
                = new HttpSelfHostConfiguration("http://localhost/");
            config.Routes.MapHttpRoute(
                name: "Default",
                routeTemplate: "{controller}/{id}"
                );
            using (HttpSelfHostServer server = new HttpSelfHostServer(config))
            using (HttpClient client = new HttpClient())
            {
                server.OpenAsync().Wait();

                using (HttpRequestMessage request =
                    new HttpRequestMessage(HttpMethod.Get, "http://localhost/studenti/9"))
                using (HttpResponseMessage response = client.SendAsync(request).Result)
                {
                    string json = response.Content.ReadAsStringAsync().Result;
                    dynamic data = JObject.Parse(json);
                    actualStudentId = data.student_id;
                    actualMatricniCislo = data.matricni_cislo;
                    actualJmeno = data.jmeno;
                    actualPrijmeni = data.prijmeni;
                    actualKurzId = data.kurz_id;
                }
                server.CloseAsync().Wait();
            }
            Assert.AreEqual(9, actualStudentId);
            Assert.AreEqual(11, actualMatricniCislo);
            Assert.AreEqual("Jan", actualJmeno);
            Assert.AreEqual("Novák", actualPrijmeni);
            Assert.AreEqual(8, actualKurzId);
        }

        [Test]
        [Explicit("Just add new record")]
        public void Post_SendNewValues_GetOkHttpCode()
        {
            string json = "{\"matricni_cislo\":\"9999\",\"jmeno\":\"testJmeno\",\"prijmeni\":\"testPrijmeni\",\"kurz_id\":8}";
            HttpStatusCode actualHttpCode = AssertHelper.SendJsonGetCode(
                "{controller}", "http://localhost/studenti", json);
            Assert.AreEqual(HttpStatusCode.OK, actualHttpCode);
        }

        [Test]
        [Explicit("Just delete record")]
        public void Delete_SendCorrectStuedntId_GetOkHttpCode()
        {
            HttpStatusCode actualHttpCode = AssertHelper.SendRequestGetCode(
                HttpMethod.Delete, "{controller}/{id}", "http://localhost/studenti/28");
            Assert.AreEqual(HttpStatusCode.OK, actualHttpCode);
        }

        [Test]
        [Explicit("Just add new record and check response")]
        public void Post_CheckReturnedValues_GetCorrectValues()
        {
            // Create new Student a post him
            string json = "{\"matricni_cislo\":\"9999\",\"jmeno\":\"testJmeno\",\"prijmeni\":\"testPrijmeni\",\"kurz_id\":8}";
            JObject response = AssertHelper.SendJsonGetJson(
                "{controller}", "http://localhost/studenti", json);
            //Check response values
            dynamic data = response;
            int actualMatricniCislo = data.matricni_cislo;
            string actualJmeno = data.jmeno;
            string actualPrijmeni = data.prijmeni;
            int actualKurzId = data.kurz_id;
            Assert.AreEqual(9999, actualMatricniCislo);
            Assert.AreEqual("testJmeno", actualJmeno);
            Assert.AreEqual("testPrijmeni", actualPrijmeni);
            Assert.AreEqual(8, actualKurzId);
        }

        [Test]
        public void PostAndDelete_PostStudentCheckResponseValuesDeleteStudent_GetCorrectValuesAndSucessfulDelete()
        {
            // Create new Student a post him
            string json = "{\"matricni_cislo\":\"9999\",\"jmeno\":\"testJmeno\",\"prijmeni\":\"testPrijmeni\",\"kurz_id\":8}";
            JObject response = AssertHelper.SendJsonGetJson(
                "{controller}", "http://localhost/studenti", json);
            //Check response values
            dynamic data = response;
            int actualStudentId = data.student_id;
            int actualMatricniCislo = data.matricni_cislo;
            string actualJmeno = data.jmeno;
            string actualPrijmeni = data.prijmeni;
            int actualKurzId = data.kurz_id;
            Assert.AreEqual(9999, actualMatricniCislo);
            Assert.AreEqual("testJmeno", actualJmeno);
            Assert.AreEqual("testPrijmeni", actualPrijmeni);
            Assert.AreEqual(8, actualKurzId);
            //Delete student
            HttpStatusCode codeAfterDelete = AssertHelper.SendRequestGetCode
                (HttpMethod.Delete, "{controller}/{id}", "http://localhost/studenti/" + actualStudentId.ToString());
            Assert.AreEqual(HttpStatusCode.OK, codeAfterDelete);
        }
    }
}