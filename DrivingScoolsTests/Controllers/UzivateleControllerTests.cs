﻿using System;
using NUnit.Framework;
using System.Net;
using System.Net.Http;
using System.Web.Http.SelfHost;
using System.Web.Http;
using Newtonsoft.Json.Linq;

namespace DrivingScoolsTests.Controllers
{
    [TestFixture]
    class UzivateleControllerTests
    {
        [Test]
        public void PostAndDelete_PostUserCheckResponseValuesDeleteUser_GetCorrectValuesAndSucessfulDelete()
        {
            // Create new User a post him
            string json = "{\"uzivatelske_jmeno\":\"testJmeno\",\"heslo\":\"testHeslo\",\"role\":\"jednatel\"}";
            JObject response = AssertHelper.SendJsonGetJson(
                "{controller}", "http://localhost/uzivatele", json);
            //Check response values
            dynamic data = response;
            int actualUserId = data.uzivatel_id;            
            //Delete student
            HttpStatusCode codeAfterDelete = AssertHelper.SendRequestGetCode
                (HttpMethod.Delete, "{controller}/{id}", "http://localhost/uzivatele/" + actualUserId.ToString());
            Assert.AreEqual(HttpStatusCode.OK, codeAfterDelete);
        }
    }
}
