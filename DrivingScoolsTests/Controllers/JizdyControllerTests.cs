﻿using System;
using NUnit.Framework;
using System.Net;
using System.Net.Http;
using System.Web.Http.SelfHost;
using System.Web.Http;
using Newtonsoft.Json.Linq;

namespace DrivingScoolsTests.Controllers
{
    [TestFixture]
    public class JizdyControllerTests
    {
        [Test]
        public void Get_RequestAllRides_ReturnHttpCodeOk()
        {
            HttpStatusCode actualStatusCode = AssertHelper.SendRequestGetCode
                (HttpMethod.Get, "{controller}", "http://localhost/jizdy");
            Assert.AreEqual(HttpStatusCode.OK, actualStatusCode);
        }

        [Test]
        public void Get_RequestCorrectRideId_ReturnHttpCodeOk()
        {
            HttpStatusCode actualStatusCode = AssertHelper.SendRequestGetCode
                (HttpMethod.Get, "{controller}/{id}", "http://localhost/jizdy/1");
            Assert.AreEqual(HttpStatusCode.OK, actualStatusCode);
        }

        [Test]
        [Explicit("Not implemented - low priority")]
        public void Get_RequestNotExistingRideId_ReturnHttpCodeNotFound()
        {
            HttpStatusCode actualStatusCode = AssertHelper.SendRequestGetCode
                (HttpMethod.Get, "{controller}/{id}", "http://localhost/jizdy/-1");
            Assert.AreEqual(HttpStatusCode.NotFound, actualStatusCode);
        }

        [Test]
        public void Get_RequestCorrectRideId_GetCorrectValues()
        {
            int actualRideId;
            double actualNajeto;
            double actualSpotreba;
            string actualDateOd;
            string actualDateDo;
            int actualVozidloId;
            int actualStudentId;
            int actualUcitelId;
            //int actualKurzId;

            HttpSelfHostConfiguration config
                = new HttpSelfHostConfiguration("http://localhost/");
            config.Routes.MapHttpRoute(
                name: "Default",
                routeTemplate: "{controller}/{id}"
                );
            using (HttpSelfHostServer server = new HttpSelfHostServer(config))
            using (HttpClient client = new HttpClient())
            {
                server.OpenAsync().Wait();

                using (HttpRequestMessage request =
                    new HttpRequestMessage(HttpMethod.Get, "http://localhost/jizdy/16"))
                using (HttpResponseMessage response = client.SendAsync(request).Result)
                {
                    string json = response.Content.ReadAsStringAsync().Result;
                    dynamic data = JObject.Parse(json);
                    actualRideId = data.jizda_id;
                    actualNajeto = data.najeto;
                    actualSpotreba = data.spotreba;
                    actualDateOd = data.cas_od; //databaze: od, model: cas_od
                    actualDateDo = data.cas_do; //databaze: do, model: cas_do
                    actualVozidloId = data.vozidlo_id;
                    actualStudentId = data.student_id;
                    actualUcitelId = data.ucitel_id;
                    //actualKurzId = data.kurz_id; //Controller ho nevraci. Je to zamer?
                }
                server.CloseAsync().Wait();
            }
            Assert.AreEqual(16, actualRideId);
            Assert.AreEqual(16, actualNajeto);
            Assert.AreEqual(7, actualSpotreba);
            Assert.AreEqual(new DateTime(2013, 11, 5, 10, 32, 00).ToString("MM/dd/yyyy HH:mm:ss"), actualDateOd);
            Assert.AreEqual(new DateTime(2013, 11, 5, 12, 32, 00).ToString("MM/dd/yyyy HH:mm:ss"), actualDateDo);
            Assert.AreEqual(9, actualVozidloId);
            Assert.AreEqual(9, actualStudentId);
            Assert.AreEqual(1, actualUcitelId);
            //Assert.AreEqual(8, actualKurzId);
        }

        [Test]
        public void PostAndDelete_PostRideCheckResponseValuesDeleteRide_GetCorrectValuesAndSucessfulDelete()
        {
            // Create new Ride a post him
            string json = "{\"vozidlo_id\":9,\"ucitel_id\":1,\"student_id\":9,\"cas_od\":\"2013-11-05T10:32:00\",\"cas_do\":\"2013-11-05T12:32:00\",\"najeto\":2.0,\"spotreba\":3.0,\"kurz_id\":8}";
            JObject response = AssertHelper.SendJsonGetJson(
                "{controller}", "http://localhost/jizdy", json);
            //Check response values
            dynamic data = response;
            int actualRideId = data.jizda_id;
            double actualNajeto = data.najeto;
            double actualSpotreba = data.spotreba;
            string actualDateOd = data.cas_od; //databaze: od, model: cas_od
            string actualDateDo = data.cas_do; //databaze: do, model: cas_do
            int actualVozidloId = data.vozidlo_id;
            int actualStudentId = data.student_id;
            int actualUcitelId = data.ucitel_id;            
            Assert.AreEqual(2.0, actualNajeto);
            Assert.AreEqual(3.0, actualSpotreba);
            Assert.AreEqual(new DateTime(2013, 11, 5, 10, 32, 00).ToString("MM/dd/yyyy HH:mm:ss"), actualDateOd);
            Assert.AreEqual(new DateTime(2013, 11, 5, 12, 32, 00).ToString("MM/dd/yyyy HH:mm:ss"), actualDateDo);
            Assert.AreEqual(9, actualVozidloId);
            Assert.AreEqual(9, actualStudentId);
            Assert.AreEqual(1, actualUcitelId);            
            //Delete ride
            HttpStatusCode codeAfterDelete = AssertHelper.SendRequestGetCode
                (HttpMethod.Delete, "{controller}/{id}", "http://localhost/jizdy/" + actualRideId.ToString());
            Assert.AreEqual(HttpStatusCode.OK, codeAfterDelete);
        }
    }
}