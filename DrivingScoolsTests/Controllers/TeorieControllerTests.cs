﻿using System;
using NUnit.Framework;
using System.Net;
using System.Net.Http;
using System.Web.Http.SelfHost;
using System.Web.Http;
using Newtonsoft.Json.Linq;

namespace DrivingScoolsTests.Controllers
{
    [TestFixture]
    public class TeorieControllerTests
    {
        [Test]
        public void Get_RequestAllTheories_ReturnHttpCodeOk()
        {
            HttpStatusCode actualStatusCode = AssertHelper.SendRequestGetCode
                (HttpMethod.Get, "{controller}", "http://localhost/teorie");
            Assert.AreEqual(HttpStatusCode.OK, actualStatusCode);
        }

        [Test]
        public void Get_RequestCorrectTheoryId_ReturnHttpCodeOk()
        {
            HttpStatusCode actualStatusCode = AssertHelper.SendRequestGetCode
                (HttpMethod.Get, "{controller}/{id}", "http://localhost/teorie/1");
            Assert.AreEqual(HttpStatusCode.OK, actualStatusCode);
        }

        [Test]
        [Explicit("Not implemented - low priority")]
        public void Get_RequestNotExistingTheoryId_ReturnHttpCodeNotFound()
        {
            HttpStatusCode actualStatusCode = AssertHelper.SendRequestGetCode
                (HttpMethod.Get, "{controller}/{id}", "http://localhost/teorie/-1");
            Assert.AreEqual(HttpStatusCode.NotFound, actualStatusCode);
        }

        [Test]
        public void PostAndDelete_PostTheoryCheckResponseValuesDeleteTheory_GetCorrectValuesAndSucessfulDelete()
        {
            // Create new Theory a post him
            string json = "{\"kurz_id\":8,\"cas_od\":\"2013-11-18T19:27:00\",\"cas_do\":\"2013-11-18T21:27:00\",\"ucitel_id\":1,\"typ\":\"pravidla_provozu\"}";
            JObject response = AssertHelper.SendJsonGetJson(
                "{controller}", "http://localhost/teorie", json);
            //Check response values
            dynamic data = response;
            int actualTeorieId = data.teorie_id;
            int actualKurzId = data.kurz_id;
            string actualCasOd = data.cas_od;
            string actualCasDo = data.cas_do;
            string actualTyp = data.typ;
            int actualUcitel = data.ucitel_id;
            Assert.AreEqual(8, actualKurzId);
            Assert.AreEqual(new DateTime(2013, 11, 18, 19, 27, 00).ToString("MM/dd/yyyy HH:mm:ss"), actualCasOd);
            Assert.AreEqual(new DateTime(2013, 11, 18, 21, 27, 00).ToString("MM/dd/yyyy HH:mm:ss"), actualCasDo);
            Assert.AreEqual("pravidla_provozu", actualTyp);
            Assert.AreEqual(1, actualUcitel);
            //Delete theory
            HttpStatusCode codeAfterDelete = AssertHelper.SendRequestGetCode
                (HttpMethod.Delete, "{controller}/{id}", "http://localhost/teorie/" + actualTeorieId.ToString());
            Assert.AreEqual(HttpStatusCode.OK, codeAfterDelete);
        }
    }
}