﻿using System;
using NUnit.Framework;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Web.Http.SelfHost;
using System.Web.Http;
using Newtonsoft.Json.Linq;

namespace DrivingScoolsTests.Controllers
{
    class UciteleControllerTests
    {
        [Test]
        public void Get_RequestCorrectTeacherId_ReturnHttpCodeOk()
        {
            HttpStatusCode actualStatusCode = AssertHelper.SendRequestGetCode
                (HttpMethod.Get, "{controller}/{id}", "http://localhost/ucitele/1");
            Assert.AreEqual(HttpStatusCode.OK, actualStatusCode);
        }

        [Test]
        [Explicit("Not implemented - low priority")]
        public void Get_RequestNotExistingTeacherId_ReturnHttpCodeNotFound()
        {
            HttpStatusCode actualStatusCode = AssertHelper.SendRequestGetCode
                (HttpMethod.Get, "{controller}/{id}", "http://localhost/ucitele/-1");
            Assert.AreEqual(HttpStatusCode.NotFound, actualStatusCode);
        }

        [Test]
        public void Get_RequestAllTeachers_ReturnHttpCodeOk()
        {
            HttpStatusCode actualStatusCode = AssertHelper.SendRequestGetCode
                (HttpMethod.Get, "{controller}", "http://localhost/ucitele");
            Assert.AreEqual(HttpStatusCode.OK, actualStatusCode);
        }

        [Test]
        public void Get_RequestCorrectId_GetCorrectValues()
        {
            int actualTeacherId;
            string actualOsobniCislo;
            string actualJmeno;
            string actualPrijmeni;
            string actualAdresaMesto;
            string actualAdresaUlice;
            int actualAdresaCP;
            int actualAdresaPSC;
            int actualAutoskolaId;

            HttpSelfHostConfiguration config
                = new HttpSelfHostConfiguration("http://localhost/");
            config.Routes.MapHttpRoute(
                name: "Default",
                routeTemplate: "{controller}/{id}"
                );

            using (HttpSelfHostServer server = new HttpSelfHostServer(config))
            using (HttpClient client = new HttpClient())
            {
                server.OpenAsync().Wait();

                using (HttpRequestMessage request =
                    new HttpRequestMessage(HttpMethod.Get, "http://localhost/ucitele/1"))
                using (HttpResponseMessage response = client.SendAsync(request).Result)
                {
                    string json = response.Content.ReadAsStringAsync().Result;
                    dynamic data = JObject.Parse(json);
                    actualTeacherId = data.ucitel_id;
                    actualOsobniCislo = data.osobni_cislo;
                    actualJmeno = data.jmeno;
                    actualPrijmeni = data.prijmeni;
                    actualAdresaMesto = data.adresa_mesto;
                    actualAdresaUlice = data.adresa_ulice;
                    actualAdresaCP = data.adresa_cp;
                    actualAdresaPSC = data.adresa_psc;
                    actualAutoskolaId = data.autoskola_id;
                }
                server.CloseAsync().Wait();
            }
            Assert.AreEqual(1, actualTeacherId);
            Assert.AreEqual("111", actualOsobniCislo);
            Assert.AreEqual("Jan", actualJmeno);
            Assert.AreEqual("Jandera", actualPrijmeni);
            Assert.AreEqual("Pce", actualAdresaMesto);
            Assert.AreEqual("ul", actualAdresaUlice);
            Assert.AreEqual(123, actualAdresaCP);
            Assert.AreEqual(12345, actualAdresaPSC);
            Assert.AreEqual(1, actualAutoskolaId);
        }
    }
}
