﻿using System;
using NUnit.Framework;
using System.Net;
using System.Net.Http;
using System.Web.Http.SelfHost;
using System.Web.Http;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;

namespace DrivingScoolsTests.Controllers
{
    class VozidlaControllerTests
    {
        [Test]
        public void Get_RequestAllCar_ReturnHttpCodeOk()
        {
            HttpStatusCode actualStatusCode = AssertHelper.SendRequestGetCode
                (HttpMethod.Get, "{controller}", "http://localhost/vozidla");
            Assert.AreEqual(HttpStatusCode.OK, actualStatusCode);
        }

        [Test]
        public void Get_RequestCorrectCarId_ReturnHttpCodeOk()
        {
            HttpStatusCode actualStatusCode = AssertHelper.SendRequestGetCode
                (HttpMethod.Get, "{controller}/{id}", "http://localhost/vozidla/9");
            Assert.AreEqual(HttpStatusCode.OK, actualStatusCode);
        }

        [Test]
        [Explicit("Not implemented - low priority")]
        public void Get_RequestNotExistingCarId_ReturnHttpCodeNotFound()
        {
            HttpStatusCode actualStatusCode = AssertHelper.SendRequestGetCode
                (HttpMethod.Get, "{controller}/{id}", "http://localhost/vozidla/-1");
            Assert.AreEqual(HttpStatusCode.NotFound, actualStatusCode);
        }

        [Test]
        public void Get_RequestCorrectCourseId_GetCorrectValues()
        {
            int actualCarId;            
            string actualZnacka;
            string actualModel;
            string actualSPZ;
            string actualVIN;
            int actualPocatecniStavKm;
            int actualRokVyroby;
            string actualPosledniSTK;
            int actualAutoskolaId;
            string actualTyp;
            int actualNajetoKm;
            int actualPrumernaSpotreba;

            HttpSelfHostConfiguration config
                = new HttpSelfHostConfiguration("http://localhost/");
            config.Routes.MapHttpRoute(
                name: "Default",
                routeTemplate: "{controller}/{id}"
                );
            using (HttpSelfHostServer server = new HttpSelfHostServer(config))
            using (HttpClient client = new HttpClient())
            {
                server.OpenAsync().Wait();
                using (HttpRequestMessage request =
                    new HttpRequestMessage(HttpMethod.Get, "http://localhost/vozidla/9"))
                using (HttpResponseMessage response = client.SendAsync(request).Result)
                {
                    string json = response.Content.ReadAsStringAsync().Result;
                    dynamic data = JObject.Parse(json);
                    actualCarId = data.vozidlo_id;                    
                    actualZnacka = data.znacka;
                    actualModel = data.model;
                    actualSPZ = data.spz;
                    actualVIN = data.vin;
                    actualPocatecniStavKm = data.pocatecni_stav_km;
                    actualRokVyroby = data.rok_vyroby;
                    actualPosledniSTK = data.posledni_stk;
                    actualAutoskolaId = data.autoskola_id;
                    actualTyp = data.vozidlo_typ;
                    actualNajetoKm = data.pocet_km;
                    actualPrumernaSpotreba = data.prumerna_spotreba;
                }
                server.CloseAsync().Wait();
            }
            Assert.AreEqual(9, actualCarId);
            Assert.AreEqual("Škoda",actualZnacka);
            Assert.AreEqual("Octavia", actualModel);
            Assert.AreEqual("3E8 3853", actualSPZ);
            Assert.AreEqual("FG5464RTV5432AF5F", actualVIN);
            Assert.AreEqual(8832, actualPocatecniStavKm);
            Assert.AreEqual(2007, actualRokVyroby);
            Assert.AreEqual(new DateTime(2013, 11, 25).ToString("MM/dd/yyyy HH:mm:ss"), actualPosledniSTK);
            Assert.AreEqual(1, actualAutoskolaId);
            Assert.AreEqual("osobni", actualTyp);
            Assert.AreEqual(8832 + 36, actualNajetoKm);
            Assert.AreEqual(6, actualPrumernaSpotreba);
        }

        [Test]        
        public void PostAndDelete_PostCarCheckResponseValuesDeleteCar_GetCorrectValuesAndSucessfulDelete()
        {
            // Create new Car a post him
            string json = "{\"autoskola_id\":1,\"znacka\":\"TestZnacka\",\"model\":\"TestModel\",\"vin\":\"ABCDEFGHIJKLMNOPQ\",\"posledni_stk\":\"2013-11-25\",\"pocatecni_stav_km\":1234,\"vozidlo_typ\":\"osobni\",\"rok_vyroby\":2007,\"spz\":\"123 TEST\"}";
            JObject response = AssertHelper.SendJsonGetJson(
                "{controller}", "http://localhost/vozidla", json);            
            //Check response values            
            dynamic data = response;            
            int actualCarId = data.vozidlo_id;            
            string actualZnacka = data.znacka;
            string actualModel = data.model;
            string actualSPZ = data.spz;
            string actualVIN = data.vin;
            int actualPocatecniStavKm = data.pocatecni_stav_km;
            int actualRokVyroby = data.rok_vyroby;
            string actualPosledniSTK = data.posledni_stk;
            int actualAutoskolaId = data.autoskola_id;
            string actualTyp = data.vozidlo_typ;
            int actualPrumernaSpotreba = data.prumerna_spotreba;
            int actualpocetKm = data.pocet_km;
            Assert.AreEqual("TestZnacka", actualZnacka);
            Assert.AreEqual("TestModel", actualModel);
            Assert.AreEqual("123 TEST", actualSPZ);
            Assert.AreEqual("ABCDEFGHIJKLMNOPQ", actualVIN);
            Assert.AreEqual(1234, actualPocatecniStavKm);
            Assert.AreEqual(2007, actualRokVyroby);
            Assert.AreEqual(new DateTime(2013, 11, 25).ToString("MM/dd/yyyy HH:mm:ss"), actualPosledniSTK);
            Assert.AreEqual(1, actualAutoskolaId);
            Assert.AreEqual("osobni", actualTyp);            
            //Delete car
            HttpStatusCode codeAfterDelete = AssertHelper.SendRequestGetCode
                (HttpMethod.Delete, "{controller}/{id}", "http://localhost/vozidla/" + actualCarId.ToString());
            Assert.AreEqual(HttpStatusCode.OK, codeAfterDelete);
        }

        [Test]
        public void Put_PostCarPutValuesCheckAndDelete_GetCorrectValuesAndSucessfulDelete()
        {
            // Create new Car a post him
            string json = "{\"autoskola_id\":1,\"znacka\":\"TestZnacka\",\"model\":\"TestModel\",\"vin\":\"ABCDEFGHIJKLMNOPQ\",\"posledni_stk\":\"2013-11-25\",\"pocatecni_stav_km\":1234,\"vozidlo_typ\":\"osobni\",\"rok_vyroby\":2007,\"spz\":\"123 TEST\"}";
            JObject response = AssertHelper.SendJsonGetJson(
                "{controller}", "http://localhost/vozidla", json);
            //Update
            dynamic data = response;
            data.model = "UPDATE";
            json = null;
            JObject o = new JObject(data);
            json = JsonConvert.SerializeObject(data);            
            response = null;
            response = AssertHelper.SendJsonGetJson(HttpMethod.Put, "{controller}", 
                "http://localhost/vozidla", json);
            //Check values
            data = null;
            data = response;
            int actualCarId = data.vozidlo_id;
            string actualZnacka = data.znacka;
            string actualModel = data.model;
            string actualSPZ = data.spz;
            string actualVIN = data.vin;
            int actualPocatecniStavKm = data.pocatecni_stav_km;
            int actualRokVyroby = data.rok_vyroby;
            string actualPosledniSTK = data.posledni_stk;
            int actualAutoskolaId = data.autoskola_id;
            string actualTyp = data.vozidlo_typ;
            int actualPrumernaSpotreba = data.prumerna_spotreba;
            Assert.AreEqual("TestZnacka", actualZnacka);
            Assert.AreEqual("UPDATE", actualModel);
            Assert.AreEqual("123 TEST", actualSPZ);
            Assert.AreEqual("ABCDEFGHIJKLMNOPQ", actualVIN);
            Assert.AreEqual(1234, actualPocatecniStavKm);
            Assert.AreEqual(2007, actualRokVyroby);
            Assert.AreEqual(new DateTime(2013, 11, 25).ToString("MM/dd/yyyy HH:mm:ss"), actualPosledniSTK);
            Assert.AreEqual(1, actualAutoskolaId);
            Assert.AreEqual("osobni", actualTyp);            
            //Delete car
            HttpStatusCode codeAfterDelete = AssertHelper.SendRequestGetCode
                (HttpMethod.Delete, "{controller}/{id}", "http://localhost/vozidla/" + actualCarId.ToString());
            Assert.AreEqual(HttpStatusCode.OK, codeAfterDelete);
        }
    }
}
