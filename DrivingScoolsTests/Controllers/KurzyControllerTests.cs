﻿using System;
using NUnit.Framework;
using System.Net;
using System.Net.Http;
using System.Web.Http.SelfHost;
using System.Web.Http;
using Newtonsoft.Json.Linq;

namespace DrivingScoolsTests.Controllers
{
    [TestFixture]
    public class KurzyControllerTests
    {
        [Test]
        public void Get_RequestAllCourses_ReturnHttpCodeOk()
        {
            HttpStatusCode actualStatusCode = AssertHelper.SendRequestGetCode
                (HttpMethod.Get, "{controller}", "http://localhost/kurzy");
            Assert.AreEqual(HttpStatusCode.OK, actualStatusCode);
        }

        [Test]
        public void Get_RequestCorrectCoursetId_ReturnHttpCodeOk()
        {
            HttpStatusCode actualStatusCode = AssertHelper.SendRequestGetCode
                (HttpMethod.Get, "{controller}/{id}", "http://localhost/kurzy/1");
            Assert.AreEqual(HttpStatusCode.OK, actualStatusCode);
        }

        [Test]
        [Explicit("Not implemented - low priority")]
        public void Get_RequestNotExistingCourseId_ReturnHttpCodeNotFound()
        {
            HttpStatusCode actualStatusCode = AssertHelper.SendRequestGetCode
                (HttpMethod.Get, "{controller}/{id}", "http://localhost/kurzy/-1");
            Assert.AreEqual(HttpStatusCode.NotFound, actualStatusCode);
        }

        [Test]
        public void Get_RequestCorrectCourseId_GetCorrectValues()
        {
            int actualCourseId;
            string actualIdentifikacniCislo;
            string actualDatumOd;
            string actualDatumDo;
            int actualAutoskolaId;
            string actualStav;
            char actualTyp;

            HttpSelfHostConfiguration config
                = new HttpSelfHostConfiguration("http://localhost/");
            config.Routes.MapHttpRoute(
                name: "Default",
                routeTemplate: "{controller}/{id}"
                );
            using (HttpSelfHostServer server = new HttpSelfHostServer(config))
            using (HttpClient client = new HttpClient())
            {
                server.OpenAsync().Wait();

                using (HttpRequestMessage request =
                    new HttpRequestMessage(HttpMethod.Get, "http://localhost/kurzy/8"))
                using (HttpResponseMessage response = client.SendAsync(request).Result)
                {
                    string json = response.Content.ReadAsStringAsync().Result;
                    dynamic data = JObject.Parse(json);
                    actualCourseId = data.kurz_id;
                    actualIdentifikacniCislo = data.identifikacni_cislo;
                    actualDatumOd = data.datum_od;
                    actualDatumDo = data.datum_do;
                    actualAutoskolaId = data.autoskola_id;
                    actualStav = data.stav;
                    actualTyp = data.typ;
                }
                server.CloseAsync().Wait();
            }
            Assert.AreEqual(8, actualCourseId);
            Assert.AreEqual("B201308", actualIdentifikacniCislo);
            Assert.AreEqual(new DateTime(2013, 11, 19).ToString("MM/dd/yyyy HH:mm:ss"), actualDatumOd);
            Assert.AreEqual(new DateTime(2014, 2, 19).ToString("MM/dd/yyyy HH:mm:ss"), actualDatumDo);
            Assert.AreEqual(1, actualAutoskolaId);
            Assert.AreEqual("probihajici", actualStav);
            Assert.AreEqual('B', actualTyp);
        }

        [Test]        
        public void PostAndDelete_PostCourseCheckResponseValuesDeleteCourse_GetCorrectValuesAndSucessfulDelete()
        {
            // Create new Course a post him
            string json = "{\"kurz_id\":1,\"typ\":\"A\",\"autoskola_id\":1,\"stav\":\"probihajici\",\"datum_od\":\"2002-01-20T00:00:00\",\"datum_do\":\"2003-06-20T00:00:00\",\"identifikacni_cislo\":\"testIdNum\"}";
            JObject response = AssertHelper.SendJsonGetJson(
                "{controller}", "http://localhost/kurzy", json);
            //Check response values
            dynamic data = response;
            int actualCourseId = data.kurz_id;
            string actualIdentifikacniCislo = data.identifikacni_cislo;
            string actualDatumOd = data.datum_od;
            string actualDatumDo = data.datum_do;
            int actualAutoskolaId = data.autoskola_id;
            string actualStav = data.stav;
            char actualTyp = data.typ;
            Assert.AreEqual("A2002" + actualCourseId.ToString("00"), actualIdentifikacniCislo);
            Assert.AreEqual(new DateTime(2002, 1, 20).ToString("MM/dd/yyyy HH:mm:ss"), actualDatumOd);
            Assert.AreEqual(new DateTime(2003, 6, 20).ToString("MM/dd/yyyy HH:mm:ss"), actualDatumDo);
            Assert.AreEqual(1, actualAutoskolaId);
            Assert.AreEqual("probihajici", actualStav);
            Assert.AreEqual('A', actualTyp);
            //Delete course
            HttpStatusCode codeAfterDelete = AssertHelper.SendRequestGetCode
                (HttpMethod.Delete, "{controller}/{id}", "http://localhost/kurzy/" + actualCourseId.ToString());
            Assert.AreEqual(HttpStatusCode.OK, codeAfterDelete);
        }
    }
}