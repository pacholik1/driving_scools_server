﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http.SelfHost;
using System.Web.Http;
using System.Net.Http.Headers;
using System.Text;
using Newtonsoft.Json.Linq;

namespace DrivingScoolsTests
{
    static class AssertHelper
    {
        public static HttpStatusCode SendRequestGetCode(HttpMethod method, string routeTemp, string uri)
        {
            HttpStatusCode actualStatusCode;
            HttpSelfHostConfiguration config
                = new HttpSelfHostConfiguration("http://localhost/");
            config.Routes.MapHttpRoute(
                name: "Default",
                routeTemplate: routeTemp
                );

            using (HttpSelfHostServer server = new HttpSelfHostServer(config))
            using (HttpClient client = new HttpClient())
            {
                server.OpenAsync().Wait();
                using (HttpRequestMessage request =
                    new HttpRequestMessage(method, uri))
                using (HttpResponseMessage response = client.SendAsync(request).Result)
                {
                    actualStatusCode = response.StatusCode;
                }
                server.CloseAsync().Wait();
            }
            return actualStatusCode;
        }

        public static HttpStatusCode SendJsonGetCode(string routeTemp, string uri, string json)
        {
            HttpStatusCode actualHttpCode;

            HttpSelfHostConfiguration config
                = new HttpSelfHostConfiguration("http://localhost/");
            config.Routes.MapHttpRoute(
                name: "Default",
                routeTemplate: routeTemp
                );

            using (HttpSelfHostServer server = new HttpSelfHostServer(config))
            using (HttpClient client = getJsonClient())
            {
                server.OpenAsync().Wait();
                using (HttpRequestMessage request =
                    getJsonRequest(HttpMethod.Post, uri, json))
                using (HttpResponseMessage response = client.SendAsync(request).Result)
                {
                    actualHttpCode = response.StatusCode;
                }
                server.CloseAsync().Wait();
            }
            return actualHttpCode;
        }

        public static JObject SendJsonGetJson(string routeTemp, string uri, string json)
        {
            return SendJsonGetJson(HttpMethod.Post, routeTemp, uri, json);
        }

        public static JObject SendJsonGetJson(HttpMethod method, string routeTemp, string uri, string json)
        {
            JObject result;

            HttpSelfHostConfiguration config
                = new HttpSelfHostConfiguration("http://localhost/");
            config.Routes.MapHttpRoute(
                name: "Default",
                routeTemplate: routeTemp
                );

            using (HttpSelfHostServer server = new HttpSelfHostServer(config))
            using (HttpClient client = getJsonClient())
            {
                server.OpenAsync().Wait();
                using (HttpRequestMessage request =
                    getJsonRequest(method, uri, json))
                using (HttpResponseMessage response = client.SendAsync(request).Result)
                {
                    string responseJson = response.Content.ReadAsStringAsync().Result;
                    result = JObject.Parse(responseJson);
                }
                server.CloseAsync().Wait();
            }
            return result;
        }

        private static HttpClient getJsonClient()
        {
            HttpClient c = new HttpClient();
            c.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            return c;
        }

        private static HttpRequestMessage getJsonRequest(HttpMethod method, string uri, string json)
        {
            HttpRequestMessage req = new HttpRequestMessage(method, uri);
            req.Content = new StringContent(json, Encoding.UTF8, "application/json");
            return req;
        }
    }
}
