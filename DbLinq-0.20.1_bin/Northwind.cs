// 
//  ____  _     __  __      _        _ 
// |  _ \| |__ |  \/  | ___| |_ __ _| |
// | | | | '_ \| |\/| |/ _ \ __/ _` | |
// | |_| | |_) | |  | |  __/ || (_| | |
// |____/|_.__/|_|  |_|\___|\__\__,_|_|
//
// Auto-generated from driving_scool on 2013-11-12 00:04:38Z.
// Please visit http://code.google.com/p/dblinq2007/ for more information.
//
namespace nwind
{
	using System;
	using System.ComponentModel;
	using System.Data;
#if MONO_STRICT
	using System.Data.Linq;
#else   // MONO_STRICT
	using DbLinq.Data.Linq;
	using DbLinq.Vendor;
#endif  // MONO_STRICT
	using System.Data.Linq.Mapping;
	using System.Diagnostics;
	
	
	public partial class DrivingSCool : DataContext
	{
		
		#region Extensibility Method Declarations
		partial void OnCreated();
		#endregion
		
		
		public DrivingSCool(string connectionString) : 
				base(connectionString)
		{
			this.OnCreated();
		}
		
		public DrivingSCool(string connection, MappingSource mappingSource) : 
				base(connection, mappingSource)
		{
			this.OnCreated();
		}
		
		public DrivingSCool(IDbConnection connection, MappingSource mappingSource) : 
				base(connection, mappingSource)
		{
			this.OnCreated();
		}
		
		public Table<AutosKoLY> AutosKoLY
		{
			get
			{
				return this.GetTable<AutosKoLY>();
			}
		}
		
		public Table<DoKuMenTYUCiteLu> DoKuMenTYUCiteLu
		{
			get
			{
				return this.GetTable<DoKuMenTYUCiteLu>();
			}
		}
		
		public Table<DoKuMenTYVOzIDel> DoKuMenTYVOzIDel
		{
			get
			{
				return this.GetTable<DoKuMenTYVOzIDel>();
			}
		}
		
		public Table<JIZDy> JIZDy
		{
			get
			{
				return this.GetTable<JIZDy>();
			}
		}
		
		public Table<KuRZY> KuRZY
		{
			get
			{
				return this.GetTable<KuRZY>();
			}
		}
		
		public Table<OprAvNeNiUCiteLu> OprAvNeNiUCiteLu
		{
			get
			{
				return this.GetTable<OprAvNeNiUCiteLu>();
			}
		}
		
		public Table<Role> Role
		{
			get
			{
				return this.GetTable<Role>();
			}
		}
		
		public Table<STAvYKuRZU> STAvYKuRZU
		{
			get
			{
				return this.GetTable<STAvYKuRZU>();
			}
		}
		
		public Table<StudentI> StudentI
		{
			get
			{
				return this.GetTable<StudentI>();
			}
		}
		
		public Table<TeOrIe> TeOrIe
		{
			get
			{
				return this.GetTable<TeOrIe>();
			}
		}
		
		public Table<TYPYKuRZU> TYPYKuRZU
		{
			get
			{
				return this.GetTable<TYPYKuRZU>();
			}
		}
		
		public Table<TYPYOprAvNeNi> TYPYOprAvNeNi
		{
			get
			{
				return this.GetTable<TYPYOprAvNeNi>();
			}
		}
		
		public Table<TYPYtEOrIe> TYPYtEOrIe
		{
			get
			{
				return this.GetTable<TYPYtEOrIe>();
			}
		}
		
		public Table<TYPYVOzIDel> TYPYVOzIDel
		{
			get
			{
				return this.GetTable<TYPYVOzIDel>();
			}
		}
		
		public Table<UCiteLE> UCiteLE
		{
			get
			{
				return this.GetTable<UCiteLE>();
			}
		}
		
		public Table<UziVatELE> UziVatELE
		{
			get
			{
				return this.GetTable<UziVatELE>();
			}
		}
		
		public Table<VOzIDLa> VOzIDLa
		{
			get
			{
				return this.GetTable<VOzIDLa>();
			}
		}
	}
	
	#region Start MONO_STRICT
#if MONO_STRICT

	public partial class DrivingSCool
	{
		
		public DrivingSCool(IDbConnection connection) : 
				base(connection)
		{
			this.OnCreated();
		}
	}
	#region End MONO_STRICT
	#endregion
#else     // MONO_STRICT
	
	public partial class DrivingSCool
	{
		
		public DrivingSCool(IDbConnection connection) : 
				base(connection, new DbLinq.MySql.MySqlVendor())
		{
			this.OnCreated();
		}
		
		public DrivingSCool(IDbConnection connection, IVendor sqlDialect) : 
				base(connection, sqlDialect)
		{
			this.OnCreated();
		}
		
		public DrivingSCool(IDbConnection connection, MappingSource mappingSource, IVendor sqlDialect) : 
				base(connection, mappingSource, sqlDialect)
		{
			this.OnCreated();
		}
	}
	#region End Not MONO_STRICT
	#endregion
#endif     // MONO_STRICT
	#endregion
	
	[Table(Name="driving_scool.AUTOSKOLY")]
	public partial class AutosKoLY : System.ComponentModel.INotifyPropertyChanging, System.ComponentModel.INotifyPropertyChanged
	{
		
		private static System.ComponentModel.PropertyChangingEventArgs emptyChangingEventArgs = new System.ComponentModel.PropertyChangingEventArgs("");
		
		private int _adReSaCp;
		
		private string _adReSaMeStO;
		
		private int _adReSaPsC;
		
		private string _adReSaUlIce;
		
		private uint _autosKolaID;
		
		private string _naZEv;
		
		private uint _uziVatElid;
		
		private EntitySet<KuRZY> _kuRzy;
		
		private EntitySet<UCiteLE> _ucIteLe;
		
		private EntitySet<VOzIDLa> _voZIdlA;
		
		private EntityRef<UziVatELE> _uziVatEle = new EntityRef<UziVatELE>();
		
		#region Extensibility Method Declarations
		partial void OnCreated();
		
		partial void OnAdReSaCPChanged();
		
		partial void OnAdReSaCPChanging(int value);
		
		partial void OnAdReSaMEstOChanged();
		
		partial void OnAdReSaMEstOChanging(string value);
		
		partial void OnAdReSaPsCChanged();
		
		partial void OnAdReSaPsCChanging(int value);
		
		partial void OnAdReSaUlIceChanged();
		
		partial void OnAdReSaUlIceChanging(string value);
		
		partial void OnAutosKolaIDChanged();
		
		partial void OnAutosKolaIDChanging(uint value);
		
		partial void OnNAzEVChanged();
		
		partial void OnNAzEVChanging(string value);
		
		partial void OnUziVatELIDChanged();
		
		partial void OnUziVatELIDChanging(uint value);
		#endregion
		
		
		public AutosKoLY()
		{
			_kuRzy = new EntitySet<KuRZY>(new Action<KuRZY>(this.KuRZY_Attach), new Action<KuRZY>(this.KuRZY_Detach));
			_ucIteLe = new EntitySet<UCiteLE>(new Action<UCiteLE>(this.UCiteLE_Attach), new Action<UCiteLE>(this.UCiteLE_Detach));
			_voZIdlA = new EntitySet<VOzIDLa>(new Action<VOzIDLa>(this.VOzIDLa_Attach), new Action<VOzIDLa>(this.VOzIDLa_Detach));
			this.OnCreated();
		}
		
		[Column(Storage="_adReSaCp", Name="adresa_cp", DbType="int", AutoSync=AutoSync.Never, CanBeNull=false)]
		[DebuggerNonUserCode()]
		public int AdReSaCP
		{
			get
			{
				return this._adReSaCp;
			}
			set
			{
				if ((_adReSaCp != value))
				{
					this.OnAdReSaCPChanging(value);
					this.SendPropertyChanging();
					this._adReSaCp = value;
					this.SendPropertyChanged("AdReSaCP");
					this.OnAdReSaCPChanged();
				}
			}
		}
		
		[Column(Storage="_adReSaMeStO", Name="adresa_mesto", DbType="varchar(33)", AutoSync=AutoSync.Never, CanBeNull=false)]
		[DebuggerNonUserCode()]
		public string AdReSaMEstO
		{
			get
			{
				return this._adReSaMeStO;
			}
			set
			{
				if (((_adReSaMeStO == value) 
							== false))
				{
					this.OnAdReSaMEstOChanging(value);
					this.SendPropertyChanging();
					this._adReSaMeStO = value;
					this.SendPropertyChanged("AdReSaMEstO");
					this.OnAdReSaMEstOChanged();
				}
			}
		}
		
		[Column(Storage="_adReSaPsC", Name="adresa_psc", DbType="int", AutoSync=AutoSync.Never, CanBeNull=false)]
		[DebuggerNonUserCode()]
		public int AdReSaPsC
		{
			get
			{
				return this._adReSaPsC;
			}
			set
			{
				if ((_adReSaPsC != value))
				{
					this.OnAdReSaPsCChanging(value);
					this.SendPropertyChanging();
					this._adReSaPsC = value;
					this.SendPropertyChanged("AdReSaPsC");
					this.OnAdReSaPsCChanged();
				}
			}
		}
		
		[Column(Storage="_adReSaUlIce", Name="adresa_ulice", DbType="varchar(37)", AutoSync=AutoSync.Never, CanBeNull=false)]
		[DebuggerNonUserCode()]
		public string AdReSaUlIce
		{
			get
			{
				return this._adReSaUlIce;
			}
			set
			{
				if (((_adReSaUlIce == value) 
							== false))
				{
					this.OnAdReSaUlIceChanging(value);
					this.SendPropertyChanging();
					this._adReSaUlIce = value;
					this.SendPropertyChanged("AdReSaUlIce");
					this.OnAdReSaUlIceChanged();
				}
			}
		}
		
		[Column(Storage="_autosKolaID", Name="autoskola_id", DbType="int unsigned", IsPrimaryKey=true, IsDbGenerated=true, AutoSync=AutoSync.Never, CanBeNull=false)]
		[DebuggerNonUserCode()]
		public uint AutosKolaID
		{
			get
			{
				return this._autosKolaID;
			}
			set
			{
				if ((_autosKolaID != value))
				{
					this.OnAutosKolaIDChanging(value);
					this.SendPropertyChanging();
					this._autosKolaID = value;
					this.SendPropertyChanged("AutosKolaID");
					this.OnAutosKolaIDChanged();
				}
			}
		}
		
		[Column(Storage="_naZEv", Name="nazev", DbType="varchar(30)", AutoSync=AutoSync.Never, CanBeNull=false)]
		[DebuggerNonUserCode()]
		public string NAzEV
		{
			get
			{
				return this._naZEv;
			}
			set
			{
				if (((_naZEv == value) 
							== false))
				{
					this.OnNAzEVChanging(value);
					this.SendPropertyChanging();
					this._naZEv = value;
					this.SendPropertyChanged("NAzEV");
					this.OnNAzEVChanged();
				}
			}
		}
		
		[Column(Storage="_uziVatElid", Name="uzivatel_id", DbType="int unsigned", AutoSync=AutoSync.Never, CanBeNull=false)]
		[DebuggerNonUserCode()]
		public uint UziVatELID
		{
			get
			{
				return this._uziVatElid;
			}
			set
			{
				if ((_uziVatElid != value))
				{
					this.OnUziVatELIDChanging(value);
					this.SendPropertyChanging();
					this._uziVatElid = value;
					this.SendPropertyChanged("UziVatELID");
					this.OnUziVatELIDChanged();
				}
			}
		}
		
		#region Children
		[Association(Storage="_kuRzy", OtherKey="AutosKolaID", ThisKey="AutosKolaID", Name="KURZY_AUTOSKOLY_FK")]
		[DebuggerNonUserCode()]
		public EntitySet<KuRZY> KuRZY
		{
			get
			{
				return this._kuRzy;
			}
			set
			{
				this._kuRzy = value;
			}
		}
		
		[Association(Storage="_ucIteLe", OtherKey="AutosKolaID", ThisKey="AutosKolaID", Name="UCITELE_AUTOSKOLY_FK")]
		[DebuggerNonUserCode()]
		public EntitySet<UCiteLE> UCiteLE
		{
			get
			{
				return this._ucIteLe;
			}
			set
			{
				this._ucIteLe = value;
			}
		}
		
		[Association(Storage="_voZIdlA", OtherKey="AutosKolaID", ThisKey="AutosKolaID", Name="VOZIDLA_AUTOSKOLY_FK")]
		[DebuggerNonUserCode()]
		public EntitySet<VOzIDLa> VOzIDLa
		{
			get
			{
				return this._voZIdlA;
			}
			set
			{
				this._voZIdlA = value;
			}
		}
		#endregion
		
		#region Parents
		[Association(Storage="_uziVatEle", OtherKey="UziVatELID", ThisKey="UziVatELID", Name="AUTOSKOLA_UZIVATELE_FK", IsForeignKey=true)]
		[DebuggerNonUserCode()]
		public UziVatELE UziVatELE
		{
			get
			{
				return this._uziVatEle.Entity;
			}
			set
			{
				if (((this._uziVatEle.Entity == value) 
							== false))
				{
					if ((this._uziVatEle.Entity != null))
					{
						UziVatELE previousUziVatELE = this._uziVatEle.Entity;
						this._uziVatEle.Entity = null;
						previousUziVatELE.AutosKoLY.Remove(this);
					}
					this._uziVatEle.Entity = value;
					if ((value != null))
					{
						value.AutosKoLY.Add(this);
						_uziVatElid = value.UziVatELID;
					}
					else
					{
						_uziVatElid = default(uint);
					}
				}
			}
		}
		#endregion
		
		public event System.ComponentModel.PropertyChangingEventHandler PropertyChanging;
		
		public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
		
		protected virtual void SendPropertyChanging()
		{
			System.ComponentModel.PropertyChangingEventHandler h = this.PropertyChanging;
			if ((h != null))
			{
				h(this, emptyChangingEventArgs);
			}
		}
		
		protected virtual void SendPropertyChanged(string propertyName)
		{
			System.ComponentModel.PropertyChangedEventHandler h = this.PropertyChanged;
			if ((h != null))
			{
				h(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
			}
		}
		
		#region Attachment handlers
		private void KuRZY_Attach(KuRZY entity)
		{
			this.SendPropertyChanging();
			entity.AutosKoLY = this;
		}
		
		private void KuRZY_Detach(KuRZY entity)
		{
			this.SendPropertyChanging();
			entity.AutosKoLY = null;
		}
		
		private void UCiteLE_Attach(UCiteLE entity)
		{
			this.SendPropertyChanging();
			entity.AutosKoLY = this;
		}
		
		private void UCiteLE_Detach(UCiteLE entity)
		{
			this.SendPropertyChanging();
			entity.AutosKoLY = null;
		}
		
		private void VOzIDLa_Attach(VOzIDLa entity)
		{
			this.SendPropertyChanging();
			entity.AutosKoLY = this;
		}
		
		private void VOzIDLa_Detach(VOzIDLa entity)
		{
			this.SendPropertyChanging();
			entity.AutosKoLY = null;
		}
		#endregion
	}
	
	[Table(Name="driving_scool.DOKUMENTY_UCITELU")]
	public partial class DoKuMenTYUCiteLu : System.ComponentModel.INotifyPropertyChanging, System.ComponentModel.INotifyPropertyChanged
	{
		
		private static System.ComponentModel.PropertyChangingEventArgs emptyChangingEventArgs = new System.ComponentModel.PropertyChangingEventArgs("");
		
		private System.DateTime _datumVlOZenI;
		
		private byte[] _doKuMenT;
		
		private uint _doKuMenTid;
		
		private string _doKuMenTnaZEv;
		
		private uint _ucIteLid;
		
		private EntityRef<UCiteLE> _ucIteLe = new EntityRef<UCiteLE>();
		
		#region Extensibility Method Declarations
		partial void OnCreated();
		
		partial void OnDatumVLoZenIChanged();
		
		partial void OnDatumVLoZenIChanging(System.DateTime value);
		
		partial void OnDoKuMenTChanged();
		
		partial void OnDoKuMenTChanging(byte[] value);
		
		partial void OnDoKuMenTIDChanged();
		
		partial void OnDoKuMenTIDChanging(uint value);
		
		partial void OnDoKuMenTNAzEVChanged();
		
		partial void OnDoKuMenTNAzEVChanging(string value);
		
		partial void OnUCiteLIDChanged();
		
		partial void OnUCiteLIDChanging(uint value);
		#endregion
		
		
		public DoKuMenTYUCiteLu()
		{
			this.OnCreated();
		}
		
		[Column(Storage="_datumVlOZenI", Name="datum_vlozeni", DbType="date", AutoSync=AutoSync.Never, CanBeNull=false)]
		[DebuggerNonUserCode()]
		public System.DateTime DatumVLoZenI
		{
			get
			{
				return this._datumVlOZenI;
			}
			set
			{
				if ((_datumVlOZenI != value))
				{
					this.OnDatumVLoZenIChanging(value);
					this.SendPropertyChanging();
					this._datumVlOZenI = value;
					this.SendPropertyChanged("DatumVLoZenI");
					this.OnDatumVLoZenIChanged();
				}
			}
		}
		
		[Column(Storage="_doKuMenT", Name="dokument", DbType="blob", AutoSync=AutoSync.Never, CanBeNull=false)]
		[DebuggerNonUserCode()]
		public byte[] DoKuMenT
		{
			get
			{
				return this._doKuMenT;
			}
			set
			{
				if (((_doKuMenT == value) 
							== false))
				{
					this.OnDoKuMenTChanging(value);
					this.SendPropertyChanging();
					this._doKuMenT = value;
					this.SendPropertyChanged("DoKuMenT");
					this.OnDoKuMenTChanged();
				}
			}
		}
		
		[Column(Storage="_doKuMenTid", Name="dokument_id", DbType="int unsigned", IsPrimaryKey=true, IsDbGenerated=true, AutoSync=AutoSync.Never, CanBeNull=false)]
		[DebuggerNonUserCode()]
		public uint DoKuMenTID
		{
			get
			{
				return this._doKuMenTid;
			}
			set
			{
				if ((_doKuMenTid != value))
				{
					this.OnDoKuMenTIDChanging(value);
					this.SendPropertyChanging();
					this._doKuMenTid = value;
					this.SendPropertyChanged("DoKuMenTID");
					this.OnDoKuMenTIDChanged();
				}
			}
		}
		
		[Column(Storage="_doKuMenTnaZEv", Name="dokument_nazev", DbType="varchar(20)", AutoSync=AutoSync.Never, CanBeNull=false)]
		[DebuggerNonUserCode()]
		public string DoKuMenTNAzEV
		{
			get
			{
				return this._doKuMenTnaZEv;
			}
			set
			{
				if (((_doKuMenTnaZEv == value) 
							== false))
				{
					this.OnDoKuMenTNAzEVChanging(value);
					this.SendPropertyChanging();
					this._doKuMenTnaZEv = value;
					this.SendPropertyChanged("DoKuMenTNAzEV");
					this.OnDoKuMenTNAzEVChanged();
				}
			}
		}
		
		[Column(Storage="_ucIteLid", Name="ucitel_id", DbType="int unsigned", AutoSync=AutoSync.Never, CanBeNull=false)]
		[DebuggerNonUserCode()]
		public uint UCiteLID
		{
			get
			{
				return this._ucIteLid;
			}
			set
			{
				if ((_ucIteLid != value))
				{
					this.OnUCiteLIDChanging(value);
					this.SendPropertyChanging();
					this._ucIteLid = value;
					this.SendPropertyChanged("UCiteLID");
					this.OnUCiteLIDChanged();
				}
			}
		}
		
		#region Parents
		[Association(Storage="_ucIteLe", OtherKey="UCiteLID", ThisKey="UCiteLID", Name="UCITELE_LEKARSKE_PROHLIDKY_FK", IsForeignKey=true)]
		[DebuggerNonUserCode()]
		public UCiteLE UCiteLE
		{
			get
			{
				return this._ucIteLe.Entity;
			}
			set
			{
				if (((this._ucIteLe.Entity == value) 
							== false))
				{
					if ((this._ucIteLe.Entity != null))
					{
						UCiteLE previousUCiteLE = this._ucIteLe.Entity;
						this._ucIteLe.Entity = null;
						previousUCiteLE.DoKuMenTYUCiteLu.Remove(this);
					}
					this._ucIteLe.Entity = value;
					if ((value != null))
					{
						value.DoKuMenTYUCiteLu.Add(this);
						_ucIteLid = value.UCiteLID;
					}
					else
					{
						_ucIteLid = default(uint);
					}
				}
			}
		}
		#endregion
		
		public event System.ComponentModel.PropertyChangingEventHandler PropertyChanging;
		
		public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
		
		protected virtual void SendPropertyChanging()
		{
			System.ComponentModel.PropertyChangingEventHandler h = this.PropertyChanging;
			if ((h != null))
			{
				h(this, emptyChangingEventArgs);
			}
		}
		
		protected virtual void SendPropertyChanged(string propertyName)
		{
			System.ComponentModel.PropertyChangedEventHandler h = this.PropertyChanged;
			if ((h != null))
			{
				h(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
			}
		}
	}
	
	[Table(Name="driving_scool.DOKUMENTY_VOZIDEL")]
	public partial class DoKuMenTYVOzIDel : System.ComponentModel.INotifyPropertyChanging, System.ComponentModel.INotifyPropertyChanged
	{
		
		private static System.ComponentModel.PropertyChangingEventArgs emptyChangingEventArgs = new System.ComponentModel.PropertyChangingEventArgs("");
		
		private System.DateTime _datumVlOZenI;
		
		private byte[] _doKuMenT;
		
		private uint _doKuMenTid;
		
		private string _doKuMenTnaZEv;
		
		private uint _voZIdlOID;
		
		private EntityRef<VOzIDLa> _voZIdlA = new EntityRef<VOzIDLa>();
		
		#region Extensibility Method Declarations
		partial void OnCreated();
		
		partial void OnDatumVLoZenIChanged();
		
		partial void OnDatumVLoZenIChanging(System.DateTime value);
		
		partial void OnDoKuMenTChanged();
		
		partial void OnDoKuMenTChanging(byte[] value);
		
		partial void OnDoKuMenTIDChanged();
		
		partial void OnDoKuMenTIDChanging(uint value);
		
		partial void OnDoKuMenTNAzEVChanged();
		
		partial void OnDoKuMenTNAzEVChanging(string value);
		
		partial void OnVOzIDLoIDChanged();
		
		partial void OnVOzIDLoIDChanging(uint value);
		#endregion
		
		
		public DoKuMenTYVOzIDel()
		{
			this.OnCreated();
		}
		
		[Column(Storage="_datumVlOZenI", Name="datum_vlozeni", DbType="datetime", AutoSync=AutoSync.Never, CanBeNull=false)]
		[DebuggerNonUserCode()]
		public System.DateTime DatumVLoZenI
		{
			get
			{
				return this._datumVlOZenI;
			}
			set
			{
				if ((_datumVlOZenI != value))
				{
					this.OnDatumVLoZenIChanging(value);
					this.SendPropertyChanging();
					this._datumVlOZenI = value;
					this.SendPropertyChanged("DatumVLoZenI");
					this.OnDatumVLoZenIChanged();
				}
			}
		}
		
		[Column(Storage="_doKuMenT", Name="dokument", DbType="blob", AutoSync=AutoSync.Never, CanBeNull=false)]
		[DebuggerNonUserCode()]
		public byte[] DoKuMenT
		{
			get
			{
				return this._doKuMenT;
			}
			set
			{
				if (((_doKuMenT == value) 
							== false))
				{
					this.OnDoKuMenTChanging(value);
					this.SendPropertyChanging();
					this._doKuMenT = value;
					this.SendPropertyChanged("DoKuMenT");
					this.OnDoKuMenTChanged();
				}
			}
		}
		
		[Column(Storage="_doKuMenTid", Name="dokument_id", DbType="int unsigned", IsPrimaryKey=true, IsDbGenerated=true, AutoSync=AutoSync.Never, CanBeNull=false)]
		[DebuggerNonUserCode()]
		public uint DoKuMenTID
		{
			get
			{
				return this._doKuMenTid;
			}
			set
			{
				if ((_doKuMenTid != value))
				{
					this.OnDoKuMenTIDChanging(value);
					this.SendPropertyChanging();
					this._doKuMenTid = value;
					this.SendPropertyChanged("DoKuMenTID");
					this.OnDoKuMenTIDChanged();
				}
			}
		}
		
		[Column(Storage="_doKuMenTnaZEv", Name="dokument_nazev", DbType="varchar(20)", AutoSync=AutoSync.Never, CanBeNull=false)]
		[DebuggerNonUserCode()]
		public string DoKuMenTNAzEV
		{
			get
			{
				return this._doKuMenTnaZEv;
			}
			set
			{
				if (((_doKuMenTnaZEv == value) 
							== false))
				{
					this.OnDoKuMenTNAzEVChanging(value);
					this.SendPropertyChanging();
					this._doKuMenTnaZEv = value;
					this.SendPropertyChanged("DoKuMenTNAzEV");
					this.OnDoKuMenTNAzEVChanged();
				}
			}
		}
		
		[Column(Storage="_voZIdlOID", Name="vozidlo_id", DbType="int unsigned", AutoSync=AutoSync.Never, CanBeNull=false)]
		[DebuggerNonUserCode()]
		public uint VOzIDLoID
		{
			get
			{
				return this._voZIdlOID;
			}
			set
			{
				if ((_voZIdlOID != value))
				{
					this.OnVOzIDLoIDChanging(value);
					this.SendPropertyChanging();
					this._voZIdlOID = value;
					this.SendPropertyChanged("VOzIDLoID");
					this.OnVOzIDLoIDChanged();
				}
			}
		}
		
		#region Parents
		[Association(Storage="_voZIdlA", OtherKey="VOzIDLoID", ThisKey="VOzIDLoID", Name="STK_VOZIDLA_FK", IsForeignKey=true)]
		[DebuggerNonUserCode()]
		public VOzIDLa VOzIDLa
		{
			get
			{
				return this._voZIdlA.Entity;
			}
			set
			{
				if (((this._voZIdlA.Entity == value) 
							== false))
				{
					if ((this._voZIdlA.Entity != null))
					{
						VOzIDLa previousVOzIDLa = this._voZIdlA.Entity;
						this._voZIdlA.Entity = null;
						previousVOzIDLa.DoKuMenTYVOzIDel.Remove(this);
					}
					this._voZIdlA.Entity = value;
					if ((value != null))
					{
						value.DoKuMenTYVOzIDel.Add(this);
						_voZIdlOID = value.VOzIDLoID;
					}
					else
					{
						_voZIdlOID = default(uint);
					}
				}
			}
		}
		#endregion
		
		public event System.ComponentModel.PropertyChangingEventHandler PropertyChanging;
		
		public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
		
		protected virtual void SendPropertyChanging()
		{
			System.ComponentModel.PropertyChangingEventHandler h = this.PropertyChanging;
			if ((h != null))
			{
				h(this, emptyChangingEventArgs);
			}
		}
		
		protected virtual void SendPropertyChanged(string propertyName)
		{
			System.ComponentModel.PropertyChangedEventHandler h = this.PropertyChanged;
			if ((h != null))
			{
				h(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
			}
		}
	}
	
	[Table(Name="driving_scool.JIZDY")]
	public partial class JIZDy : System.ComponentModel.INotifyPropertyChanging, System.ComponentModel.INotifyPropertyChanged
	{
		
		private static System.ComponentModel.PropertyChangingEventArgs emptyChangingEventArgs = new System.ComponentModel.PropertyChangingEventArgs("");
		
		private System.DateTime _do;
		
		private uint _jizdAID;
		
		private uint _kuRzid;
		
		private double _najEtO;
		
		private System.DateTime _od;
		
		private double _spOtReBa;
		
		private uint _studentID;
		
		private uint _ucIteLid;
		
		private uint _voZIdlOID;
		
		private EntityRef<KuRZY> _kuRzy = new EntityRef<KuRZY>();
		
		private EntityRef<StudentI> _studentI = new EntityRef<StudentI>();
		
		private EntityRef<UCiteLE> _ucIteLe = new EntityRef<UCiteLE>();
		
		private EntityRef<VOzIDLa> _voZIdlA = new EntityRef<VOzIDLa>();
		
		#region Extensibility Method Declarations
		partial void OnCreated();
		
		partial void OnDoChanged();
		
		partial void OnDoChanging(System.DateTime value);
		
		partial void OnJIZDaIDChanged();
		
		partial void OnJIZDaIDChanging(uint value);
		
		partial void OnKuRZIDChanged();
		
		partial void OnKuRZIDChanging(uint value);
		
		partial void OnNAJetOChanged();
		
		partial void OnNAJetOChanging(double value);
		
		partial void OnOdChanged();
		
		partial void OnOdChanging(System.DateTime value);
		
		partial void OnSpOtReBaChanged();
		
		partial void OnSpOtReBaChanging(double value);
		
		partial void OnStudentIDChanged();
		
		partial void OnStudentIDChanging(uint value);
		
		partial void OnUCiteLIDChanged();
		
		partial void OnUCiteLIDChanging(uint value);
		
		partial void OnVOzIDLoIDChanged();
		
		partial void OnVOzIDLoIDChanging(uint value);
		#endregion
		
		
		public JIZDy()
		{
			this.OnCreated();
		}
		
		[Column(Storage="_do", Name="do", DbType="datetime", AutoSync=AutoSync.Never, CanBeNull=false)]
		[DebuggerNonUserCode()]
		public System.DateTime Do
		{
			get
			{
				return this._do;
			}
			set
			{
				if ((_do != value))
				{
					this.OnDoChanging(value);
					this.SendPropertyChanging();
					this._do = value;
					this.SendPropertyChanged("Do");
					this.OnDoChanged();
				}
			}
		}
		
		[Column(Storage="_jizdAID", Name="jizda_id", DbType="int unsigned", IsPrimaryKey=true, IsDbGenerated=true, AutoSync=AutoSync.Never, CanBeNull=false)]
		[DebuggerNonUserCode()]
		public uint JIZDaID
		{
			get
			{
				return this._jizdAID;
			}
			set
			{
				if ((_jizdAID != value))
				{
					this.OnJIZDaIDChanging(value);
					this.SendPropertyChanging();
					this._jizdAID = value;
					this.SendPropertyChanged("JIZDaID");
					this.OnJIZDaIDChanged();
				}
			}
		}
		
		[Column(Storage="_kuRzid", Name="kurz_id", DbType="int unsigned", AutoSync=AutoSync.Never, CanBeNull=false)]
		[DebuggerNonUserCode()]
		public uint KuRZID
		{
			get
			{
				return this._kuRzid;
			}
			set
			{
				if ((_kuRzid != value))
				{
					this.OnKuRZIDChanging(value);
					this.SendPropertyChanging();
					this._kuRzid = value;
					this.SendPropertyChanged("KuRZID");
					this.OnKuRZIDChanged();
				}
			}
		}
		
		[Column(Storage="_najEtO", Name="najeto", DbType="double", AutoSync=AutoSync.Never, CanBeNull=false)]
		[DebuggerNonUserCode()]
		public double NAJetO
		{
			get
			{
				return this._najEtO;
			}
			set
			{
				if ((_najEtO != value))
				{
					this.OnNAJetOChanging(value);
					this.SendPropertyChanging();
					this._najEtO = value;
					this.SendPropertyChanged("NAJetO");
					this.OnNAJetOChanged();
				}
			}
		}
		
		[Column(Storage="_od", Name="od", DbType="datetime", AutoSync=AutoSync.Never, CanBeNull=false)]
		[DebuggerNonUserCode()]
		public System.DateTime Od
		{
			get
			{
				return this._od;
			}
			set
			{
				if ((_od != value))
				{
					this.OnOdChanging(value);
					this.SendPropertyChanging();
					this._od = value;
					this.SendPropertyChanged("Od");
					this.OnOdChanged();
				}
			}
		}
		
		[Column(Storage="_spOtReBa", Name="spotreba", DbType="double", AutoSync=AutoSync.Never, CanBeNull=false)]
		[DebuggerNonUserCode()]
		public double SpOtReBa
		{
			get
			{
				return this._spOtReBa;
			}
			set
			{
				if ((_spOtReBa != value))
				{
					this.OnSpOtReBaChanging(value);
					this.SendPropertyChanging();
					this._spOtReBa = value;
					this.SendPropertyChanged("SpOtReBa");
					this.OnSpOtReBaChanged();
				}
			}
		}
		
		[Column(Storage="_studentID", Name="student_id", DbType="int unsigned", AutoSync=AutoSync.Never, CanBeNull=false)]
		[DebuggerNonUserCode()]
		public uint StudentID
		{
			get
			{
				return this._studentID;
			}
			set
			{
				if ((_studentID != value))
				{
					this.OnStudentIDChanging(value);
					this.SendPropertyChanging();
					this._studentID = value;
					this.SendPropertyChanged("StudentID");
					this.OnStudentIDChanged();
				}
			}
		}
		
		[Column(Storage="_ucIteLid", Name="ucitel_id", DbType="int unsigned", AutoSync=AutoSync.Never, CanBeNull=false)]
		[DebuggerNonUserCode()]
		public uint UCiteLID
		{
			get
			{
				return this._ucIteLid;
			}
			set
			{
				if ((_ucIteLid != value))
				{
					this.OnUCiteLIDChanging(value);
					this.SendPropertyChanging();
					this._ucIteLid = value;
					this.SendPropertyChanged("UCiteLID");
					this.OnUCiteLIDChanged();
				}
			}
		}
		
		[Column(Storage="_voZIdlOID", Name="vozidlo_id", DbType="int unsigned", AutoSync=AutoSync.Never, CanBeNull=false)]
		[DebuggerNonUserCode()]
		public uint VOzIDLoID
		{
			get
			{
				return this._voZIdlOID;
			}
			set
			{
				if ((_voZIdlOID != value))
				{
					this.OnVOzIDLoIDChanging(value);
					this.SendPropertyChanging();
					this._voZIdlOID = value;
					this.SendPropertyChanged("VOzIDLoID");
					this.OnVOzIDLoIDChanged();
				}
			}
		}
		
		#region Parents
		[Association(Storage="_kuRzy", OtherKey="KuRZID", ThisKey="KuRZID", Name="JIZDY_KURZY_FK", IsForeignKey=true)]
		[DebuggerNonUserCode()]
		public KuRZY KuRZY
		{
			get
			{
				return this._kuRzy.Entity;
			}
			set
			{
				if (((this._kuRzy.Entity == value) 
							== false))
				{
					if ((this._kuRzy.Entity != null))
					{
						KuRZY previousKuRZY = this._kuRzy.Entity;
						this._kuRzy.Entity = null;
						previousKuRZY.JIZDy.Remove(this);
					}
					this._kuRzy.Entity = value;
					if ((value != null))
					{
						value.JIZDy.Add(this);
						_kuRzid = value.KuRZID;
					}
					else
					{
						_kuRzid = default(uint);
					}
				}
			}
		}
		
		[Association(Storage="_studentI", OtherKey="StudentID", ThisKey="StudentID", Name="JIZDY_STUDENTI_FK", IsForeignKey=true)]
		[DebuggerNonUserCode()]
		public StudentI StudentI
		{
			get
			{
				return this._studentI.Entity;
			}
			set
			{
				if (((this._studentI.Entity == value) 
							== false))
				{
					if ((this._studentI.Entity != null))
					{
						StudentI previousStudentI = this._studentI.Entity;
						this._studentI.Entity = null;
						previousStudentI.JIZDy.Remove(this);
					}
					this._studentI.Entity = value;
					if ((value != null))
					{
						value.JIZDy.Add(this);
						_studentID = value.StudentID;
					}
					else
					{
						_studentID = default(uint);
					}
				}
			}
		}
		
		[Association(Storage="_ucIteLe", OtherKey="UCiteLID", ThisKey="UCiteLID", Name="JIZDY_UCITELE_FK", IsForeignKey=true)]
		[DebuggerNonUserCode()]
		public UCiteLE UCiteLE
		{
			get
			{
				return this._ucIteLe.Entity;
			}
			set
			{
				if (((this._ucIteLe.Entity == value) 
							== false))
				{
					if ((this._ucIteLe.Entity != null))
					{
						UCiteLE previousUCiteLE = this._ucIteLe.Entity;
						this._ucIteLe.Entity = null;
						previousUCiteLE.JIZDy.Remove(this);
					}
					this._ucIteLe.Entity = value;
					if ((value != null))
					{
						value.JIZDy.Add(this);
						_ucIteLid = value.UCiteLID;
					}
					else
					{
						_ucIteLid = default(uint);
					}
				}
			}
		}
		
		[Association(Storage="_voZIdlA", OtherKey="VOzIDLoID", ThisKey="VOzIDLoID", Name="JIZDY_VOZIDLA_FK", IsForeignKey=true)]
		[DebuggerNonUserCode()]
		public VOzIDLa VOzIDLa
		{
			get
			{
				return this._voZIdlA.Entity;
			}
			set
			{
				if (((this._voZIdlA.Entity == value) 
							== false))
				{
					if ((this._voZIdlA.Entity != null))
					{
						VOzIDLa previousVOzIDLa = this._voZIdlA.Entity;
						this._voZIdlA.Entity = null;
						previousVOzIDLa.JIZDy.Remove(this);
					}
					this._voZIdlA.Entity = value;
					if ((value != null))
					{
						value.JIZDy.Add(this);
						_voZIdlOID = value.VOzIDLoID;
					}
					else
					{
						_voZIdlOID = default(uint);
					}
				}
			}
		}
		#endregion
		
		public event System.ComponentModel.PropertyChangingEventHandler PropertyChanging;
		
		public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
		
		protected virtual void SendPropertyChanging()
		{
			System.ComponentModel.PropertyChangingEventHandler h = this.PropertyChanging;
			if ((h != null))
			{
				h(this, emptyChangingEventArgs);
			}
		}
		
		protected virtual void SendPropertyChanged(string propertyName)
		{
			System.ComponentModel.PropertyChangedEventHandler h = this.PropertyChanged;
			if ((h != null))
			{
				h(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
			}
		}
	}
	
	[Table(Name="driving_scool.KURZY")]
	public partial class KuRZY : System.ComponentModel.INotifyPropertyChanging, System.ComponentModel.INotifyPropertyChanged
	{
		
		private static System.ComponentModel.PropertyChangingEventArgs emptyChangingEventArgs = new System.ComponentModel.PropertyChangingEventArgs("");
		
		private uint _autosKolaID;
		
		private System.DateTime _datumDo;
		
		private System.DateTime _datumOd;
		
		private string _idEntIfIkaCNiCiSLo;
		
		private uint _kuRzid;
		
		private string _staV;
		
		private string _typ;
		
		private EntitySet<JIZDy> _jizdY;
		
		private EntitySet<StudentI> _studentI;
		
		private EntitySet<TeOrIe> _teOrIe;
		
		private EntityRef<AutosKoLY> _autosKoLy = new EntityRef<AutosKoLY>();
		
		private EntityRef<STAvYKuRZU> _staVYkURzu = new EntityRef<STAvYKuRZU>();
		
		private EntityRef<TYPYKuRZU> _typykURzu = new EntityRef<TYPYKuRZU>();
		
		#region Extensibility Method Declarations
		partial void OnCreated();
		
		partial void OnAutosKolaIDChanged();
		
		partial void OnAutosKolaIDChanging(uint value);
		
		partial void OnDatumDoChanged();
		
		partial void OnDatumDoChanging(System.DateTime value);
		
		partial void OnDatumOdChanged();
		
		partial void OnDatumOdChanging(System.DateTime value);
		
		partial void OnIDentIfIKAcNiCIsLoChanged();
		
		partial void OnIDentIfIKAcNiCIsLoChanging(string value);
		
		partial void OnKuRZIDChanged();
		
		partial void OnKuRZIDChanging(uint value);
		
		partial void OnSTAvChanged();
		
		partial void OnSTAvChanging(string value);
		
		partial void OnTYPChanged();
		
		partial void OnTYPChanging(string value);
		#endregion
		
		
		public KuRZY()
		{
			_jizdY = new EntitySet<JIZDy>(new Action<JIZDy>(this.JIZDy_Attach), new Action<JIZDy>(this.JIZDy_Detach));
			_studentI = new EntitySet<StudentI>(new Action<StudentI>(this.StudentI_Attach), new Action<StudentI>(this.StudentI_Detach));
			_teOrIe = new EntitySet<TeOrIe>(new Action<TeOrIe>(this.TeOrIe_Attach), new Action<TeOrIe>(this.TeOrIe_Detach));
			this.OnCreated();
		}
		
		[Column(Storage="_autosKolaID", Name="autoskola_id", DbType="int unsigned", AutoSync=AutoSync.Never, CanBeNull=false)]
		[DebuggerNonUserCode()]
		public uint AutosKolaID
		{
			get
			{
				return this._autosKolaID;
			}
			set
			{
				if ((_autosKolaID != value))
				{
					this.OnAutosKolaIDChanging(value);
					this.SendPropertyChanging();
					this._autosKolaID = value;
					this.SendPropertyChanged("AutosKolaID");
					this.OnAutosKolaIDChanged();
				}
			}
		}
		
		[Column(Storage="_datumDo", Name="datum_do", DbType="date", AutoSync=AutoSync.Never, CanBeNull=false)]
		[DebuggerNonUserCode()]
		public System.DateTime DatumDo
		{
			get
			{
				return this._datumDo;
			}
			set
			{
				if ((_datumDo != value))
				{
					this.OnDatumDoChanging(value);
					this.SendPropertyChanging();
					this._datumDo = value;
					this.SendPropertyChanged("DatumDo");
					this.OnDatumDoChanged();
				}
			}
		}
		
		[Column(Storage="_datumOd", Name="datum_od", DbType="date", AutoSync=AutoSync.Never, CanBeNull=false)]
		[DebuggerNonUserCode()]
		public System.DateTime DatumOd
		{
			get
			{
				return this._datumOd;
			}
			set
			{
				if ((_datumOd != value))
				{
					this.OnDatumOdChanging(value);
					this.SendPropertyChanging();
					this._datumOd = value;
					this.SendPropertyChanged("DatumOd");
					this.OnDatumOdChanged();
				}
			}
		}
		
		[Column(Storage="_idEntIfIkaCNiCiSLo", Name="identifikacni_cislo", DbType="varchar(7)", AutoSync=AutoSync.Never, CanBeNull=false)]
		[DebuggerNonUserCode()]
		public string IDentIfIKAcNiCIsLo
		{
			get
			{
				return this._idEntIfIkaCNiCiSLo;
			}
			set
			{
				if (((_idEntIfIkaCNiCiSLo == value) 
							== false))
				{
					this.OnIDentIfIKAcNiCIsLoChanging(value);
					this.SendPropertyChanging();
					this._idEntIfIkaCNiCiSLo = value;
					this.SendPropertyChanged("IDentIfIKAcNiCIsLo");
					this.OnIDentIfIKAcNiCIsLoChanged();
				}
			}
		}
		
		[Column(Storage="_kuRzid", Name="kurz_id", DbType="int unsigned", IsPrimaryKey=true, IsDbGenerated=true, AutoSync=AutoSync.Never, CanBeNull=false)]
		[DebuggerNonUserCode()]
		public uint KuRZID
		{
			get
			{
				return this._kuRzid;
			}
			set
			{
				if ((_kuRzid != value))
				{
					this.OnKuRZIDChanging(value);
					this.SendPropertyChanging();
					this._kuRzid = value;
					this.SendPropertyChanged("KuRZID");
					this.OnKuRZIDChanged();
				}
			}
		}
		
		[Column(Storage="_staV", Name="stav", DbType="varchar(10)", AutoSync=AutoSync.Never, CanBeNull=false)]
		[DebuggerNonUserCode()]
		public string STAv
		{
			get
			{
				return this._staV;
			}
			set
			{
				if (((_staV == value) 
							== false))
				{
					this.OnSTAvChanging(value);
					this.SendPropertyChanging();
					this._staV = value;
					this.SendPropertyChanged("STAv");
					this.OnSTAvChanged();
				}
			}
		}
		
		[Column(Storage="_typ", Name="typ", DbType="varchar(1)", AutoSync=AutoSync.Never, CanBeNull=false)]
		[DebuggerNonUserCode()]
		public string TYP
		{
			get
			{
				return this._typ;
			}
			set
			{
				if (((_typ == value) 
							== false))
				{
					this.OnTYPChanging(value);
					this.SendPropertyChanging();
					this._typ = value;
					this.SendPropertyChanged("TYP");
					this.OnTYPChanged();
				}
			}
		}
		
		#region Children
		[Association(Storage="_jizdY", OtherKey="KuRZID", ThisKey="KuRZID", Name="JIZDY_KURZY_FK")]
		[DebuggerNonUserCode()]
		public EntitySet<JIZDy> JIZDy
		{
			get
			{
				return this._jizdY;
			}
			set
			{
				this._jizdY = value;
			}
		}
		
		[Association(Storage="_studentI", OtherKey="KuRZID", ThisKey="KuRZID", Name="STUDENTI_KURZY_FK")]
		[DebuggerNonUserCode()]
		public EntitySet<StudentI> StudentI
		{
			get
			{
				return this._studentI;
			}
			set
			{
				this._studentI = value;
			}
		}
		
		[Association(Storage="_teOrIe", OtherKey="KuRZID", ThisKey="KuRZID", Name="TEORIE_KURZY_FK")]
		[DebuggerNonUserCode()]
		public EntitySet<TeOrIe> TeOrIe
		{
			get
			{
				return this._teOrIe;
			}
			set
			{
				this._teOrIe = value;
			}
		}
		#endregion
		
		#region Parents
		[Association(Storage="_autosKoLy", OtherKey="AutosKolaID", ThisKey="AutosKolaID", Name="KURZY_AUTOSKOLY_FK", IsForeignKey=true)]
		[DebuggerNonUserCode()]
		public AutosKoLY AutosKoLY
		{
			get
			{
				return this._autosKoLy.Entity;
			}
			set
			{
				if (((this._autosKoLy.Entity == value) 
							== false))
				{
					if ((this._autosKoLy.Entity != null))
					{
						AutosKoLY previousAutosKoLY = this._autosKoLy.Entity;
						this._autosKoLy.Entity = null;
						previousAutosKoLY.KuRZY.Remove(this);
					}
					this._autosKoLy.Entity = value;
					if ((value != null))
					{
						value.KuRZY.Add(this);
						_autosKolaID = value.AutosKolaID;
					}
					else
					{
						_autosKolaID = default(uint);
					}
				}
			}
		}
		
		[Association(Storage="_staVYkURzu", OtherKey="NAzEV", ThisKey="STAv", Name="KURZY_STAVY_KURZU_FK", IsForeignKey=true)]
		[DebuggerNonUserCode()]
		public STAvYKuRZU STAvYKuRZU
		{
			get
			{
				return this._staVYkURzu.Entity;
			}
			set
			{
				if (((this._staVYkURzu.Entity == value) 
							== false))
				{
					if ((this._staVYkURzu.Entity != null))
					{
						STAvYKuRZU previousSTAvYKuRZU = this._staVYkURzu.Entity;
						this._staVYkURzu.Entity = null;
						previousSTAvYKuRZU.KuRZY.Remove(this);
					}
					this._staVYkURzu.Entity = value;
					if ((value != null))
					{
						value.KuRZY.Add(this);
						_staV = value.NAzEV;
					}
					else
					{
						_staV = default(string);
					}
				}
			}
		}
		
		[Association(Storage="_typykURzu", OtherKey="NAzEV", ThisKey="TYP", Name="KURZY_TYPY_KURZU_FK", IsForeignKey=true)]
		[DebuggerNonUserCode()]
		public TYPYKuRZU TYPYKuRZU
		{
			get
			{
				return this._typykURzu.Entity;
			}
			set
			{
				if (((this._typykURzu.Entity == value) 
							== false))
				{
					if ((this._typykURzu.Entity != null))
					{
						TYPYKuRZU previousTYPYKuRZU = this._typykURzu.Entity;
						this._typykURzu.Entity = null;
						previousTYPYKuRZU.KuRZY.Remove(this);
					}
					this._typykURzu.Entity = value;
					if ((value != null))
					{
						value.KuRZY.Add(this);
						_typ = value.NAzEV;
					}
					else
					{
						_typ = default(string);
					}
				}
			}
		}
		#endregion
		
		public event System.ComponentModel.PropertyChangingEventHandler PropertyChanging;
		
		public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
		
		protected virtual void SendPropertyChanging()
		{
			System.ComponentModel.PropertyChangingEventHandler h = this.PropertyChanging;
			if ((h != null))
			{
				h(this, emptyChangingEventArgs);
			}
		}
		
		protected virtual void SendPropertyChanged(string propertyName)
		{
			System.ComponentModel.PropertyChangedEventHandler h = this.PropertyChanged;
			if ((h != null))
			{
				h(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
			}
		}
		
		#region Attachment handlers
		private void JIZDy_Attach(JIZDy entity)
		{
			this.SendPropertyChanging();
			entity.KuRZY = this;
		}
		
		private void JIZDy_Detach(JIZDy entity)
		{
			this.SendPropertyChanging();
			entity.KuRZY = null;
		}
		
		private void StudentI_Attach(StudentI entity)
		{
			this.SendPropertyChanging();
			entity.KuRZY = this;
		}
		
		private void StudentI_Detach(StudentI entity)
		{
			this.SendPropertyChanging();
			entity.KuRZY = null;
		}
		
		private void TeOrIe_Attach(TeOrIe entity)
		{
			this.SendPropertyChanging();
			entity.KuRZY = this;
		}
		
		private void TeOrIe_Detach(TeOrIe entity)
		{
			this.SendPropertyChanging();
			entity.KuRZY = null;
		}
		#endregion
	}
	
	[Table(Name="driving_scool.OPRAVNENI_UCITELU")]
	public partial class OprAvNeNiUCiteLu : System.ComponentModel.INotifyPropertyChanging, System.ComponentModel.INotifyPropertyChanged
	{
		
		private static System.ComponentModel.PropertyChangingEventArgs emptyChangingEventArgs = new System.ComponentModel.PropertyChangingEventArgs("");
		
		private System.DateTime _platNoSt;
		
		private string _typ;
		
		private uint _ucIteLid;
		
		private EntityRef<TYPYOprAvNeNi> _typyoPrAvNeNi = new EntityRef<TYPYOprAvNeNi>();
		
		private EntityRef<UCiteLE> _ucIteLe = new EntityRef<UCiteLE>();
		
		#region Extensibility Method Declarations
		partial void OnCreated();
		
		partial void OnPlatNoSTChanged();
		
		partial void OnPlatNoSTChanging(System.DateTime value);
		
		partial void OnTYPChanged();
		
		partial void OnTYPChanging(string value);
		
		partial void OnUCiteLIDChanged();
		
		partial void OnUCiteLIDChanging(uint value);
		#endregion
		
		
		public OprAvNeNiUCiteLu()
		{
			this.OnCreated();
		}
		
		[Column(Storage="_platNoSt", Name="platnost", DbType="date", AutoSync=AutoSync.Never, CanBeNull=false)]
		[DebuggerNonUserCode()]
		public System.DateTime PlatNoST
		{
			get
			{
				return this._platNoSt;
			}
			set
			{
				if ((_platNoSt != value))
				{
					this.OnPlatNoSTChanging(value);
					this.SendPropertyChanging();
					this._platNoSt = value;
					this.SendPropertyChanged("PlatNoST");
					this.OnPlatNoSTChanged();
				}
			}
		}
		
		[Column(Storage="_typ", Name="typ", DbType="varchar(15)", IsPrimaryKey=true, AutoSync=AutoSync.Never, CanBeNull=false)]
		[DebuggerNonUserCode()]
		public string TYP
		{
			get
			{
				return this._typ;
			}
			set
			{
				if (((_typ == value) 
							== false))
				{
					this.OnTYPChanging(value);
					this.SendPropertyChanging();
					this._typ = value;
					this.SendPropertyChanged("TYP");
					this.OnTYPChanged();
				}
			}
		}
		
		[Column(Storage="_ucIteLid", Name="ucitel_id", DbType="int unsigned", IsPrimaryKey=true, AutoSync=AutoSync.Never, CanBeNull=false)]
		[DebuggerNonUserCode()]
		public uint UCiteLID
		{
			get
			{
				return this._ucIteLid;
			}
			set
			{
				if ((_ucIteLid != value))
				{
					this.OnUCiteLIDChanging(value);
					this.SendPropertyChanging();
					this._ucIteLid = value;
					this.SendPropertyChanged("UCiteLID");
					this.OnUCiteLIDChanged();
				}
			}
		}
		
		#region Parents
		[Association(Storage="_typyoPrAvNeNi", OtherKey="NAzEV", ThisKey="TYP", Name="OPRAVNENI_UCITELU_TYPY_OPRAVNENI_FK", IsForeignKey=true)]
		[DebuggerNonUserCode()]
		public TYPYOprAvNeNi TYPYOprAvNeNi
		{
			get
			{
				return this._typyoPrAvNeNi.Entity;
			}
			set
			{
				if (((this._typyoPrAvNeNi.Entity == value) 
							== false))
				{
					if ((this._typyoPrAvNeNi.Entity != null))
					{
						TYPYOprAvNeNi previousTYPYOprAvNeNi = this._typyoPrAvNeNi.Entity;
						this._typyoPrAvNeNi.Entity = null;
						previousTYPYOprAvNeNi.OprAvNeNiUCiteLu.Remove(this);
					}
					this._typyoPrAvNeNi.Entity = value;
					if ((value != null))
					{
						value.OprAvNeNiUCiteLu.Add(this);
						_typ = value.NAzEV;
					}
					else
					{
						_typ = default(string);
					}
				}
			}
		}
		
		[Association(Storage="_ucIteLe", OtherKey="UCiteLID", ThisKey="UCiteLID", Name="OPRAVNENI_UCITELU_UCITELE_FK", IsForeignKey=true)]
		[DebuggerNonUserCode()]
		public UCiteLE UCiteLE
		{
			get
			{
				return this._ucIteLe.Entity;
			}
			set
			{
				if (((this._ucIteLe.Entity == value) 
							== false))
				{
					if ((this._ucIteLe.Entity != null))
					{
						UCiteLE previousUCiteLE = this._ucIteLe.Entity;
						this._ucIteLe.Entity = null;
						previousUCiteLE.OprAvNeNiUCiteLu.Remove(this);
					}
					this._ucIteLe.Entity = value;
					if ((value != null))
					{
						value.OprAvNeNiUCiteLu.Add(this);
						_ucIteLid = value.UCiteLID;
					}
					else
					{
						_ucIteLid = default(uint);
					}
				}
			}
		}
		#endregion
		
		public event System.ComponentModel.PropertyChangingEventHandler PropertyChanging;
		
		public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
		
		protected virtual void SendPropertyChanging()
		{
			System.ComponentModel.PropertyChangingEventHandler h = this.PropertyChanging;
			if ((h != null))
			{
				h(this, emptyChangingEventArgs);
			}
		}
		
		protected virtual void SendPropertyChanged(string propertyName)
		{
			System.ComponentModel.PropertyChangedEventHandler h = this.PropertyChanged;
			if ((h != null))
			{
				h(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
			}
		}
	}
	
	[Table(Name="driving_scool.ROLE")]
	public partial class Role : System.ComponentModel.INotifyPropertyChanging, System.ComponentModel.INotifyPropertyChanged
	{
		
		private static System.ComponentModel.PropertyChangingEventArgs emptyChangingEventArgs = new System.ComponentModel.PropertyChangingEventArgs("");
		
		private string _naZEv;
		
		private EntitySet<UziVatELE> _uziVatEle;
		
		#region Extensibility Method Declarations
		partial void OnCreated();
		
		partial void OnNAzEVChanged();
		
		partial void OnNAzEVChanging(string value);
		#endregion
		
		
		public Role()
		{
			_uziVatEle = new EntitySet<UziVatELE>(new Action<UziVatELE>(this.UziVatELE_Attach), new Action<UziVatELE>(this.UziVatELE_Detach));
			this.OnCreated();
		}
		
		[Column(Storage="_naZEv", Name="nazev", DbType="varchar(18)", IsPrimaryKey=true, AutoSync=AutoSync.Never, CanBeNull=false)]
		[DebuggerNonUserCode()]
		public string NAzEV
		{
			get
			{
				return this._naZEv;
			}
			set
			{
				if (((_naZEv == value) 
							== false))
				{
					this.OnNAzEVChanging(value);
					this.SendPropertyChanging();
					this._naZEv = value;
					this.SendPropertyChanged("NAzEV");
					this.OnNAzEVChanged();
				}
			}
		}
		
		#region Children
		[Association(Storage="_uziVatEle", OtherKey="Role", ThisKey="NAzEV", Name="UZIVATELE_ROLE_FK")]
		[DebuggerNonUserCode()]
		public EntitySet<UziVatELE> UziVatELE
		{
			get
			{
				return this._uziVatEle;
			}
			set
			{
				this._uziVatEle = value;
			}
		}
		#endregion
		
		public event System.ComponentModel.PropertyChangingEventHandler PropertyChanging;
		
		public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
		
		protected virtual void SendPropertyChanging()
		{
			System.ComponentModel.PropertyChangingEventHandler h = this.PropertyChanging;
			if ((h != null))
			{
				h(this, emptyChangingEventArgs);
			}
		}
		
		protected virtual void SendPropertyChanged(string propertyName)
		{
			System.ComponentModel.PropertyChangedEventHandler h = this.PropertyChanged;
			if ((h != null))
			{
				h(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
			}
		}
		
		#region Attachment handlers
		private void UziVatELE_Attach(UziVatELE entity)
		{
			this.SendPropertyChanging();
			entity.RoleRole = this;
		}
		
		private void UziVatELE_Detach(UziVatELE entity)
		{
			this.SendPropertyChanging();
			entity.RoleRole = null;
		}
		#endregion
	}
	
	[Table(Name="driving_scool.STAVY_KURZU")]
	public partial class STAvYKuRZU : System.ComponentModel.INotifyPropertyChanging, System.ComponentModel.INotifyPropertyChanged
	{
		
		private static System.ComponentModel.PropertyChangingEventArgs emptyChangingEventArgs = new System.ComponentModel.PropertyChangingEventArgs("");
		
		private string _naZEv;
		
		private EntitySet<KuRZY> _kuRzy;
		
		#region Extensibility Method Declarations
		partial void OnCreated();
		
		partial void OnNAzEVChanged();
		
		partial void OnNAzEVChanging(string value);
		#endregion
		
		
		public STAvYKuRZU()
		{
			_kuRzy = new EntitySet<KuRZY>(new Action<KuRZY>(this.KuRZY_Attach), new Action<KuRZY>(this.KuRZY_Detach));
			this.OnCreated();
		}
		
		[Column(Storage="_naZEv", Name="nazev", DbType="varchar(10)", IsPrimaryKey=true, AutoSync=AutoSync.Never, CanBeNull=false)]
		[DebuggerNonUserCode()]
		public string NAzEV
		{
			get
			{
				return this._naZEv;
			}
			set
			{
				if (((_naZEv == value) 
							== false))
				{
					this.OnNAzEVChanging(value);
					this.SendPropertyChanging();
					this._naZEv = value;
					this.SendPropertyChanged("NAzEV");
					this.OnNAzEVChanged();
				}
			}
		}
		
		#region Children
		[Association(Storage="_kuRzy", OtherKey="STAv", ThisKey="NAzEV", Name="KURZY_STAVY_KURZU_FK")]
		[DebuggerNonUserCode()]
		public EntitySet<KuRZY> KuRZY
		{
			get
			{
				return this._kuRzy;
			}
			set
			{
				this._kuRzy = value;
			}
		}
		#endregion
		
		public event System.ComponentModel.PropertyChangingEventHandler PropertyChanging;
		
		public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
		
		protected virtual void SendPropertyChanging()
		{
			System.ComponentModel.PropertyChangingEventHandler h = this.PropertyChanging;
			if ((h != null))
			{
				h(this, emptyChangingEventArgs);
			}
		}
		
		protected virtual void SendPropertyChanged(string propertyName)
		{
			System.ComponentModel.PropertyChangedEventHandler h = this.PropertyChanged;
			if ((h != null))
			{
				h(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
			}
		}
		
		#region Attachment handlers
		private void KuRZY_Attach(KuRZY entity)
		{
			this.SendPropertyChanging();
			entity.STAvYKuRZU = this;
		}
		
		private void KuRZY_Detach(KuRZY entity)
		{
			this.SendPropertyChanging();
			entity.STAvYKuRZU = null;
		}
		#endregion
	}
	
	[Table(Name="driving_scool.STUDENTI")]
	public partial class StudentI : System.ComponentModel.INotifyPropertyChanging, System.ComponentModel.INotifyPropertyChanged
	{
		
		private static System.ComponentModel.PropertyChangingEventArgs emptyChangingEventArgs = new System.ComponentModel.PropertyChangingEventArgs("");
		
		private string _jmEnO;
		
		private uint _kuRzid;
		
		private string _matRiCnICiSLo;
		
		private string _prIJmEnI;
		
		private uint _studentID;
		
		private EntitySet<JIZDy> _jizdY;
		
		private EntityRef<KuRZY> _kuRzy = new EntityRef<KuRZY>();
		
		#region Extensibility Method Declarations
		partial void OnCreated();
		
		partial void OnJMenOChanged();
		
		partial void OnJMenOChanging(string value);
		
		partial void OnKuRZIDChanged();
		
		partial void OnKuRZIDChanging(uint value);
		
		partial void OnMatRiCNiCIsLoChanged();
		
		partial void OnMatRiCNiCIsLoChanging(string value);
		
		partial void OnPRiJMenIChanged();
		
		partial void OnPRiJMenIChanging(string value);
		
		partial void OnStudentIDChanged();
		
		partial void OnStudentIDChanging(uint value);
		#endregion
		
		
		public StudentI()
		{
			_jizdY = new EntitySet<JIZDy>(new Action<JIZDy>(this.JIZDy_Attach), new Action<JIZDy>(this.JIZDy_Detach));
			this.OnCreated();
		}
		
		[Column(Storage="_jmEnO", Name="jmeno", DbType="varchar(30)", AutoSync=AutoSync.Never, CanBeNull=false)]
		[DebuggerNonUserCode()]
		public string JMenO
		{
			get
			{
				return this._jmEnO;
			}
			set
			{
				if (((_jmEnO == value) 
							== false))
				{
					this.OnJMenOChanging(value);
					this.SendPropertyChanging();
					this._jmEnO = value;
					this.SendPropertyChanged("JMenO");
					this.OnJMenOChanged();
				}
			}
		}
		
		[Column(Storage="_kuRzid", Name="kurz_id", DbType="int unsigned", AutoSync=AutoSync.Never, CanBeNull=false)]
		[DebuggerNonUserCode()]
		public uint KuRZID
		{
			get
			{
				return this._kuRzid;
			}
			set
			{
				if ((_kuRzid != value))
				{
					this.OnKuRZIDChanging(value);
					this.SendPropertyChanging();
					this._kuRzid = value;
					this.SendPropertyChanged("KuRZID");
					this.OnKuRZIDChanged();
				}
			}
		}
		
		[Column(Storage="_matRiCnICiSLo", Name="matricni_cislo", DbType="varchar(30)", AutoSync=AutoSync.Never, CanBeNull=false)]
		[DebuggerNonUserCode()]
		public string MatRiCNiCIsLo
		{
			get
			{
				return this._matRiCnICiSLo;
			}
			set
			{
				if (((_matRiCnICiSLo == value) 
							== false))
				{
					this.OnMatRiCNiCIsLoChanging(value);
					this.SendPropertyChanging();
					this._matRiCnICiSLo = value;
					this.SendPropertyChanged("MatRiCNiCIsLo");
					this.OnMatRiCNiCIsLoChanged();
				}
			}
		}
		
		[Column(Storage="_prIJmEnI", Name="prijmeni", DbType="varchar(30)", AutoSync=AutoSync.Never, CanBeNull=false)]
		[DebuggerNonUserCode()]
		public string PRiJMenI
		{
			get
			{
				return this._prIJmEnI;
			}
			set
			{
				if (((_prIJmEnI == value) 
							== false))
				{
					this.OnPRiJMenIChanging(value);
					this.SendPropertyChanging();
					this._prIJmEnI = value;
					this.SendPropertyChanged("PRiJMenI");
					this.OnPRiJMenIChanged();
				}
			}
		}
		
		[Column(Storage="_studentID", Name="student_id", DbType="int unsigned", IsPrimaryKey=true, IsDbGenerated=true, AutoSync=AutoSync.Never, CanBeNull=false)]
		[DebuggerNonUserCode()]
		public uint StudentID
		{
			get
			{
				return this._studentID;
			}
			set
			{
				if ((_studentID != value))
				{
					this.OnStudentIDChanging(value);
					this.SendPropertyChanging();
					this._studentID = value;
					this.SendPropertyChanged("StudentID");
					this.OnStudentIDChanged();
				}
			}
		}
		
		#region Children
		[Association(Storage="_jizdY", OtherKey="StudentID", ThisKey="StudentID", Name="JIZDY_STUDENTI_FK")]
		[DebuggerNonUserCode()]
		public EntitySet<JIZDy> JIZDy
		{
			get
			{
				return this._jizdY;
			}
			set
			{
				this._jizdY = value;
			}
		}
		#endregion
		
		#region Parents
		[Association(Storage="_kuRzy", OtherKey="KuRZID", ThisKey="KuRZID", Name="STUDENTI_KURZY_FK", IsForeignKey=true)]
		[DebuggerNonUserCode()]
		public KuRZY KuRZY
		{
			get
			{
				return this._kuRzy.Entity;
			}
			set
			{
				if (((this._kuRzy.Entity == value) 
							== false))
				{
					if ((this._kuRzy.Entity != null))
					{
						KuRZY previousKuRZY = this._kuRzy.Entity;
						this._kuRzy.Entity = null;
						previousKuRZY.StudentI.Remove(this);
					}
					this._kuRzy.Entity = value;
					if ((value != null))
					{
						value.StudentI.Add(this);
						_kuRzid = value.KuRZID;
					}
					else
					{
						_kuRzid = default(uint);
					}
				}
			}
		}
		#endregion
		
		public event System.ComponentModel.PropertyChangingEventHandler PropertyChanging;
		
		public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
		
		protected virtual void SendPropertyChanging()
		{
			System.ComponentModel.PropertyChangingEventHandler h = this.PropertyChanging;
			if ((h != null))
			{
				h(this, emptyChangingEventArgs);
			}
		}
		
		protected virtual void SendPropertyChanged(string propertyName)
		{
			System.ComponentModel.PropertyChangedEventHandler h = this.PropertyChanged;
			if ((h != null))
			{
				h(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
			}
		}
		
		#region Attachment handlers
		private void JIZDy_Attach(JIZDy entity)
		{
			this.SendPropertyChanging();
			entity.StudentI = this;
		}
		
		private void JIZDy_Detach(JIZDy entity)
		{
			this.SendPropertyChanging();
			entity.StudentI = null;
		}
		#endregion
	}
	
	[Table(Name="driving_scool.TEORIE")]
	public partial class TeOrIe : System.ComponentModel.INotifyPropertyChanging, System.ComponentModel.INotifyPropertyChanged
	{
		
		private static System.ComponentModel.PropertyChangingEventArgs emptyChangingEventArgs = new System.ComponentModel.PropertyChangingEventArgs("");
		
		private System.DateTime _do;
		
		private uint _kuRzid;
		
		private System.DateTime _od;
		
		private uint _teOrIeID;
		
		private string _typ;
		
		private uint _ucIteLid;
		
		private EntityRef<KuRZY> _kuRzy = new EntityRef<KuRZY>();
		
		private EntityRef<TYPYtEOrIe> _typyTEoRIe = new EntityRef<TYPYtEOrIe>();
		
		private EntityRef<UCiteLE> _ucIteLe = new EntityRef<UCiteLE>();
		
		#region Extensibility Method Declarations
		partial void OnCreated();
		
		partial void OnDoChanged();
		
		partial void OnDoChanging(System.DateTime value);
		
		partial void OnKuRZIDChanged();
		
		partial void OnKuRZIDChanging(uint value);
		
		partial void OnOdChanged();
		
		partial void OnOdChanging(System.DateTime value);
		
		partial void OnTeOrIeIDChanged();
		
		partial void OnTeOrIeIDChanging(uint value);
		
		partial void OnTYPChanged();
		
		partial void OnTYPChanging(string value);
		
		partial void OnUCiteLIDChanged();
		
		partial void OnUCiteLIDChanging(uint value);
		#endregion
		
		
		public TeOrIe()
		{
			this.OnCreated();
		}
		
		[Column(Storage="_do", Name="do", DbType="datetime", AutoSync=AutoSync.Never, CanBeNull=false)]
		[DebuggerNonUserCode()]
		public System.DateTime Do
		{
			get
			{
				return this._do;
			}
			set
			{
				if ((_do != value))
				{
					this.OnDoChanging(value);
					this.SendPropertyChanging();
					this._do = value;
					this.SendPropertyChanged("Do");
					this.OnDoChanged();
				}
			}
		}
		
		[Column(Storage="_kuRzid", Name="kurz_id", DbType="int unsigned", AutoSync=AutoSync.Never, CanBeNull=false)]
		[DebuggerNonUserCode()]
		public uint KuRZID
		{
			get
			{
				return this._kuRzid;
			}
			set
			{
				if ((_kuRzid != value))
				{
					this.OnKuRZIDChanging(value);
					this.SendPropertyChanging();
					this._kuRzid = value;
					this.SendPropertyChanged("KuRZID");
					this.OnKuRZIDChanged();
				}
			}
		}
		
		[Column(Storage="_od", Name="od", DbType="datetime", AutoSync=AutoSync.Never, CanBeNull=false)]
		[DebuggerNonUserCode()]
		public System.DateTime Od
		{
			get
			{
				return this._od;
			}
			set
			{
				if ((_od != value))
				{
					this.OnOdChanging(value);
					this.SendPropertyChanging();
					this._od = value;
					this.SendPropertyChanged("Od");
					this.OnOdChanged();
				}
			}
		}
		
		[Column(Storage="_teOrIeID", Name="teorie_id", DbType="int unsigned", IsPrimaryKey=true, IsDbGenerated=true, AutoSync=AutoSync.Never, CanBeNull=false)]
		[DebuggerNonUserCode()]
		public uint TeOrIeID
		{
			get
			{
				return this._teOrIeID;
			}
			set
			{
				if ((_teOrIeID != value))
				{
					this.OnTeOrIeIDChanging(value);
					this.SendPropertyChanging();
					this._teOrIeID = value;
					this.SendPropertyChanged("TeOrIeID");
					this.OnTeOrIeIDChanged();
				}
			}
		}
		
		[Column(Storage="_typ", Name="typ", DbType="varchar(15)", AutoSync=AutoSync.Never, CanBeNull=false)]
		[DebuggerNonUserCode()]
		public string TYP
		{
			get
			{
				return this._typ;
			}
			set
			{
				if (((_typ == value) 
							== false))
				{
					this.OnTYPChanging(value);
					this.SendPropertyChanging();
					this._typ = value;
					this.SendPropertyChanged("TYP");
					this.OnTYPChanged();
				}
			}
		}
		
		[Column(Storage="_ucIteLid", Name="ucitel_id", DbType="int unsigned", AutoSync=AutoSync.Never, CanBeNull=false)]
		[DebuggerNonUserCode()]
		public uint UCiteLID
		{
			get
			{
				return this._ucIteLid;
			}
			set
			{
				if ((_ucIteLid != value))
				{
					this.OnUCiteLIDChanging(value);
					this.SendPropertyChanging();
					this._ucIteLid = value;
					this.SendPropertyChanged("UCiteLID");
					this.OnUCiteLIDChanged();
				}
			}
		}
		
		#region Parents
		[Association(Storage="_kuRzy", OtherKey="KuRZID", ThisKey="KuRZID", Name="TEORIE_KURZY_FK", IsForeignKey=true)]
		[DebuggerNonUserCode()]
		public KuRZY KuRZY
		{
			get
			{
				return this._kuRzy.Entity;
			}
			set
			{
				if (((this._kuRzy.Entity == value) 
							== false))
				{
					if ((this._kuRzy.Entity != null))
					{
						KuRZY previousKuRZY = this._kuRzy.Entity;
						this._kuRzy.Entity = null;
						previousKuRZY.TeOrIe.Remove(this);
					}
					this._kuRzy.Entity = value;
					if ((value != null))
					{
						value.TeOrIe.Add(this);
						_kuRzid = value.KuRZID;
					}
					else
					{
						_kuRzid = default(uint);
					}
				}
			}
		}
		
		[Association(Storage="_typyTEoRIe", OtherKey="NAzEV", ThisKey="TYP", Name="TEORIE_TYPY_TEORIE_FK", IsForeignKey=true)]
		[DebuggerNonUserCode()]
		public TYPYtEOrIe TYPYtEOrIe
		{
			get
			{
				return this._typyTEoRIe.Entity;
			}
			set
			{
				if (((this._typyTEoRIe.Entity == value) 
							== false))
				{
					if ((this._typyTEoRIe.Entity != null))
					{
						TYPYtEOrIe previousTYPYtEOrIe = this._typyTEoRIe.Entity;
						this._typyTEoRIe.Entity = null;
						previousTYPYtEOrIe.TeOrIe.Remove(this);
					}
					this._typyTEoRIe.Entity = value;
					if ((value != null))
					{
						value.TeOrIe.Add(this);
						_typ = value.NAzEV;
					}
					else
					{
						_typ = default(string);
					}
				}
			}
		}
		
		[Association(Storage="_ucIteLe", OtherKey="UCiteLID", ThisKey="UCiteLID", Name="TEORIE_UCITELE_FK", IsForeignKey=true)]
		[DebuggerNonUserCode()]
		public UCiteLE UCiteLE
		{
			get
			{
				return this._ucIteLe.Entity;
			}
			set
			{
				if (((this._ucIteLe.Entity == value) 
							== false))
				{
					if ((this._ucIteLe.Entity != null))
					{
						UCiteLE previousUCiteLE = this._ucIteLe.Entity;
						this._ucIteLe.Entity = null;
						previousUCiteLE.TeOrIe.Remove(this);
					}
					this._ucIteLe.Entity = value;
					if ((value != null))
					{
						value.TeOrIe.Add(this);
						_ucIteLid = value.UCiteLID;
					}
					else
					{
						_ucIteLid = default(uint);
					}
				}
			}
		}
		#endregion
		
		public event System.ComponentModel.PropertyChangingEventHandler PropertyChanging;
		
		public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
		
		protected virtual void SendPropertyChanging()
		{
			System.ComponentModel.PropertyChangingEventHandler h = this.PropertyChanging;
			if ((h != null))
			{
				h(this, emptyChangingEventArgs);
			}
		}
		
		protected virtual void SendPropertyChanged(string propertyName)
		{
			System.ComponentModel.PropertyChangedEventHandler h = this.PropertyChanged;
			if ((h != null))
			{
				h(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
			}
		}
	}
	
	[Table(Name="driving_scool.TYPY_KURZU")]
	public partial class TYPYKuRZU : System.ComponentModel.INotifyPropertyChanging, System.ComponentModel.INotifyPropertyChanged
	{
		
		private static System.ComponentModel.PropertyChangingEventArgs emptyChangingEventArgs = new System.ComponentModel.PropertyChangingEventArgs("");
		
		private string _naZEv;
		
		private EntitySet<KuRZY> _kuRzy;
		
		#region Extensibility Method Declarations
		partial void OnCreated();
		
		partial void OnNAzEVChanged();
		
		partial void OnNAzEVChanging(string value);
		#endregion
		
		
		public TYPYKuRZU()
		{
			_kuRzy = new EntitySet<KuRZY>(new Action<KuRZY>(this.KuRZY_Attach), new Action<KuRZY>(this.KuRZY_Detach));
			this.OnCreated();
		}
		
		[Column(Storage="_naZEv", Name="nazev", DbType="varchar(1)", IsPrimaryKey=true, AutoSync=AutoSync.Never, CanBeNull=false)]
		[DebuggerNonUserCode()]
		public string NAzEV
		{
			get
			{
				return this._naZEv;
			}
			set
			{
				if (((_naZEv == value) 
							== false))
				{
					this.OnNAzEVChanging(value);
					this.SendPropertyChanging();
					this._naZEv = value;
					this.SendPropertyChanged("NAzEV");
					this.OnNAzEVChanged();
				}
			}
		}
		
		#region Children
		[Association(Storage="_kuRzy", OtherKey="TYP", ThisKey="NAzEV", Name="KURZY_TYPY_KURZU_FK")]
		[DebuggerNonUserCode()]
		public EntitySet<KuRZY> KuRZY
		{
			get
			{
				return this._kuRzy;
			}
			set
			{
				this._kuRzy = value;
			}
		}
		#endregion
		
		public event System.ComponentModel.PropertyChangingEventHandler PropertyChanging;
		
		public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
		
		protected virtual void SendPropertyChanging()
		{
			System.ComponentModel.PropertyChangingEventHandler h = this.PropertyChanging;
			if ((h != null))
			{
				h(this, emptyChangingEventArgs);
			}
		}
		
		protected virtual void SendPropertyChanged(string propertyName)
		{
			System.ComponentModel.PropertyChangedEventHandler h = this.PropertyChanged;
			if ((h != null))
			{
				h(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
			}
		}
		
		#region Attachment handlers
		private void KuRZY_Attach(KuRZY entity)
		{
			this.SendPropertyChanging();
			entity.TYPYKuRZU = this;
		}
		
		private void KuRZY_Detach(KuRZY entity)
		{
			this.SendPropertyChanging();
			entity.TYPYKuRZU = null;
		}
		#endregion
	}
	
	[Table(Name="driving_scool.TYPY_OPRAVNENI")]
	public partial class TYPYOprAvNeNi : System.ComponentModel.INotifyPropertyChanging, System.ComponentModel.INotifyPropertyChanged
	{
		
		private static System.ComponentModel.PropertyChangingEventArgs emptyChangingEventArgs = new System.ComponentModel.PropertyChangingEventArgs("");
		
		private string _naZEv;
		
		private EntitySet<OprAvNeNiUCiteLu> _oprAvNeNiUcIteLu;
		
		#region Extensibility Method Declarations
		partial void OnCreated();
		
		partial void OnNAzEVChanged();
		
		partial void OnNAzEVChanging(string value);
		#endregion
		
		
		public TYPYOprAvNeNi()
		{
			_oprAvNeNiUcIteLu = new EntitySet<OprAvNeNiUCiteLu>(new Action<OprAvNeNiUCiteLu>(this.OprAvNeNiUCiteLu_Attach), new Action<OprAvNeNiUCiteLu>(this.OprAvNeNiUCiteLu_Detach));
			this.OnCreated();
		}
		
		[Column(Storage="_naZEv", Name="nazev", DbType="varchar(15)", IsPrimaryKey=true, AutoSync=AutoSync.Never, CanBeNull=false)]
		[DebuggerNonUserCode()]
		public string NAzEV
		{
			get
			{
				return this._naZEv;
			}
			set
			{
				if (((_naZEv == value) 
							== false))
				{
					this.OnNAzEVChanging(value);
					this.SendPropertyChanging();
					this._naZEv = value;
					this.SendPropertyChanged("NAzEV");
					this.OnNAzEVChanged();
				}
			}
		}
		
		#region Children
		[Association(Storage="_oprAvNeNiUcIteLu", OtherKey="TYP", ThisKey="NAzEV", Name="OPRAVNENI_UCITELU_TYPY_OPRAVNENI_FK")]
		[DebuggerNonUserCode()]
		public EntitySet<OprAvNeNiUCiteLu> OprAvNeNiUCiteLu
		{
			get
			{
				return this._oprAvNeNiUcIteLu;
			}
			set
			{
				this._oprAvNeNiUcIteLu = value;
			}
		}
		#endregion
		
		public event System.ComponentModel.PropertyChangingEventHandler PropertyChanging;
		
		public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
		
		protected virtual void SendPropertyChanging()
		{
			System.ComponentModel.PropertyChangingEventHandler h = this.PropertyChanging;
			if ((h != null))
			{
				h(this, emptyChangingEventArgs);
			}
		}
		
		protected virtual void SendPropertyChanged(string propertyName)
		{
			System.ComponentModel.PropertyChangedEventHandler h = this.PropertyChanged;
			if ((h != null))
			{
				h(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
			}
		}
		
		#region Attachment handlers
		private void OprAvNeNiUCiteLu_Attach(OprAvNeNiUCiteLu entity)
		{
			this.SendPropertyChanging();
			entity.TYPYOprAvNeNi = this;
		}
		
		private void OprAvNeNiUCiteLu_Detach(OprAvNeNiUCiteLu entity)
		{
			this.SendPropertyChanging();
			entity.TYPYOprAvNeNi = null;
		}
		#endregion
	}
	
	[Table(Name="driving_scool.TYPY_TEORIE")]
	public partial class TYPYtEOrIe : System.ComponentModel.INotifyPropertyChanging, System.ComponentModel.INotifyPropertyChanged
	{
		
		private static System.ComponentModel.PropertyChangingEventArgs emptyChangingEventArgs = new System.ComponentModel.PropertyChangingEventArgs("");
		
		private string _naZEv;
		
		private EntitySet<TeOrIe> _teOrIe;
		
		#region Extensibility Method Declarations
		partial void OnCreated();
		
		partial void OnNAzEVChanged();
		
		partial void OnNAzEVChanging(string value);
		#endregion
		
		
		public TYPYtEOrIe()
		{
			_teOrIe = new EntitySet<TeOrIe>(new Action<TeOrIe>(this.TeOrIe_Attach), new Action<TeOrIe>(this.TeOrIe_Detach));
			this.OnCreated();
		}
		
		[Column(Storage="_naZEv", Name="nazev", DbType="varchar(15)", IsPrimaryKey=true, AutoSync=AutoSync.Never, CanBeNull=false)]
		[DebuggerNonUserCode()]
		public string NAzEV
		{
			get
			{
				return this._naZEv;
			}
			set
			{
				if (((_naZEv == value) 
							== false))
				{
					this.OnNAzEVChanging(value);
					this.SendPropertyChanging();
					this._naZEv = value;
					this.SendPropertyChanged("NAzEV");
					this.OnNAzEVChanged();
				}
			}
		}
		
		#region Children
		[Association(Storage="_teOrIe", OtherKey="TYP", ThisKey="NAzEV", Name="TEORIE_TYPY_TEORIE_FK")]
		[DebuggerNonUserCode()]
		public EntitySet<TeOrIe> TeOrIe
		{
			get
			{
				return this._teOrIe;
			}
			set
			{
				this._teOrIe = value;
			}
		}
		#endregion
		
		public event System.ComponentModel.PropertyChangingEventHandler PropertyChanging;
		
		public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
		
		protected virtual void SendPropertyChanging()
		{
			System.ComponentModel.PropertyChangingEventHandler h = this.PropertyChanging;
			if ((h != null))
			{
				h(this, emptyChangingEventArgs);
			}
		}
		
		protected virtual void SendPropertyChanged(string propertyName)
		{
			System.ComponentModel.PropertyChangedEventHandler h = this.PropertyChanged;
			if ((h != null))
			{
				h(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
			}
		}
		
		#region Attachment handlers
		private void TeOrIe_Attach(TeOrIe entity)
		{
			this.SendPropertyChanging();
			entity.TYPYtEOrIe = this;
		}
		
		private void TeOrIe_Detach(TeOrIe entity)
		{
			this.SendPropertyChanging();
			entity.TYPYtEOrIe = null;
		}
		#endregion
	}
	
	[Table(Name="driving_scool.TYPY_VOZIDEL")]
	public partial class TYPYVOzIDel : System.ComponentModel.INotifyPropertyChanging, System.ComponentModel.INotifyPropertyChanged
	{
		
		private static System.ComponentModel.PropertyChangingEventArgs emptyChangingEventArgs = new System.ComponentModel.PropertyChangingEventArgs("");
		
		private string _naZEv;
		
		private EntitySet<VOzIDLa> _voZIdlA;
		
		#region Extensibility Method Declarations
		partial void OnCreated();
		
		partial void OnNAzEVChanged();
		
		partial void OnNAzEVChanging(string value);
		#endregion
		
		
		public TYPYVOzIDel()
		{
			_voZIdlA = new EntitySet<VOzIDLa>(new Action<VOzIDLa>(this.VOzIDLa_Attach), new Action<VOzIDLa>(this.VOzIDLa_Detach));
			this.OnCreated();
		}
		
		[Column(Storage="_naZEv", Name="nazev", DbType="varchar(10)", IsPrimaryKey=true, AutoSync=AutoSync.Never, CanBeNull=false)]
		[DebuggerNonUserCode()]
		public string NAzEV
		{
			get
			{
				return this._naZEv;
			}
			set
			{
				if (((_naZEv == value) 
							== false))
				{
					this.OnNAzEVChanging(value);
					this.SendPropertyChanging();
					this._naZEv = value;
					this.SendPropertyChanged("NAzEV");
					this.OnNAzEVChanged();
				}
			}
		}
		
		#region Children
		[Association(Storage="_voZIdlA", OtherKey="VOzIDLoTYP", ThisKey="NAzEV", Name="VOZIDLA_TYPY_VOZIDEL_FK")]
		[DebuggerNonUserCode()]
		public EntitySet<VOzIDLa> VOzIDLa
		{
			get
			{
				return this._voZIdlA;
			}
			set
			{
				this._voZIdlA = value;
			}
		}
		#endregion
		
		public event System.ComponentModel.PropertyChangingEventHandler PropertyChanging;
		
		public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
		
		protected virtual void SendPropertyChanging()
		{
			System.ComponentModel.PropertyChangingEventHandler h = this.PropertyChanging;
			if ((h != null))
			{
				h(this, emptyChangingEventArgs);
			}
		}
		
		protected virtual void SendPropertyChanged(string propertyName)
		{
			System.ComponentModel.PropertyChangedEventHandler h = this.PropertyChanged;
			if ((h != null))
			{
				h(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
			}
		}
		
		#region Attachment handlers
		private void VOzIDLa_Attach(VOzIDLa entity)
		{
			this.SendPropertyChanging();
			entity.TYPYVOzIDel = this;
		}
		
		private void VOzIDLa_Detach(VOzIDLa entity)
		{
			this.SendPropertyChanging();
			entity.TYPYVOzIDel = null;
		}
		#endregion
	}
	
	[Table(Name="driving_scool.UCITELE")]
	public partial class UCiteLE : System.ComponentModel.INotifyPropertyChanging, System.ComponentModel.INotifyPropertyChanged
	{
		
		private static System.ComponentModel.PropertyChangingEventArgs emptyChangingEventArgs = new System.ComponentModel.PropertyChangingEventArgs("");
		
		private int _adReSaCp;
		
		private string _adReSaMeStO;
		
		private int _adReSaPsC;
		
		private string _adReSaUlIce;
		
		private uint _autosKolaID;
		
		private string _jmEnO;
		
		private string _osObNiCiSLo;
		
		private string _prIJmEnI;
		
		private uint _ucIteLid;
		
		private EntitySet<JIZDy> _jizdY;
		
		private EntitySet<OprAvNeNiUCiteLu> _oprAvNeNiUcIteLu;
		
		private EntitySet<TeOrIe> _teOrIe;
		
		private EntitySet<DoKuMenTYUCiteLu> _doKuMenTyucIteLu;
		
		private EntityRef<AutosKoLY> _autosKoLy = new EntityRef<AutosKoLY>();
		
		#region Extensibility Method Declarations
		partial void OnCreated();
		
		partial void OnAdReSaCPChanged();
		
		partial void OnAdReSaCPChanging(int value);
		
		partial void OnAdReSaMEstOChanged();
		
		partial void OnAdReSaMEstOChanging(string value);
		
		partial void OnAdReSaPsCChanged();
		
		partial void OnAdReSaPsCChanging(int value);
		
		partial void OnAdReSaUlIceChanged();
		
		partial void OnAdReSaUlIceChanging(string value);
		
		partial void OnAutosKolaIDChanged();
		
		partial void OnAutosKolaIDChanging(uint value);
		
		partial void OnJMenOChanged();
		
		partial void OnJMenOChanging(string value);
		
		partial void OnOSobNiCIsLoChanged();
		
		partial void OnOSobNiCIsLoChanging(string value);
		
		partial void OnPRiJMenIChanged();
		
		partial void OnPRiJMenIChanging(string value);
		
		partial void OnUCiteLIDChanged();
		
		partial void OnUCiteLIDChanging(uint value);
		#endregion
		
		
		public UCiteLE()
		{
			_jizdY = new EntitySet<JIZDy>(new Action<JIZDy>(this.JIZDy_Attach), new Action<JIZDy>(this.JIZDy_Detach));
			_oprAvNeNiUcIteLu = new EntitySet<OprAvNeNiUCiteLu>(new Action<OprAvNeNiUCiteLu>(this.OprAvNeNiUCiteLu_Attach), new Action<OprAvNeNiUCiteLu>(this.OprAvNeNiUCiteLu_Detach));
			_teOrIe = new EntitySet<TeOrIe>(new Action<TeOrIe>(this.TeOrIe_Attach), new Action<TeOrIe>(this.TeOrIe_Detach));
			_doKuMenTyucIteLu = new EntitySet<DoKuMenTYUCiteLu>(new Action<DoKuMenTYUCiteLu>(this.DoKuMenTYUCiteLu_Attach), new Action<DoKuMenTYUCiteLu>(this.DoKuMenTYUCiteLu_Detach));
			this.OnCreated();
		}
		
		[Column(Storage="_adReSaCp", Name="adresa_cp", DbType="int", AutoSync=AutoSync.Never, CanBeNull=false)]
		[DebuggerNonUserCode()]
		public int AdReSaCP
		{
			get
			{
				return this._adReSaCp;
			}
			set
			{
				if ((_adReSaCp != value))
				{
					this.OnAdReSaCPChanging(value);
					this.SendPropertyChanging();
					this._adReSaCp = value;
					this.SendPropertyChanged("AdReSaCP");
					this.OnAdReSaCPChanged();
				}
			}
		}
		
		[Column(Storage="_adReSaMeStO", Name="adresa_mesto", DbType="varchar(33)", AutoSync=AutoSync.Never, CanBeNull=false)]
		[DebuggerNonUserCode()]
		public string AdReSaMEstO
		{
			get
			{
				return this._adReSaMeStO;
			}
			set
			{
				if (((_adReSaMeStO == value) 
							== false))
				{
					this.OnAdReSaMEstOChanging(value);
					this.SendPropertyChanging();
					this._adReSaMeStO = value;
					this.SendPropertyChanged("AdReSaMEstO");
					this.OnAdReSaMEstOChanged();
				}
			}
		}
		
		[Column(Storage="_adReSaPsC", Name="adresa_psc", DbType="int", AutoSync=AutoSync.Never, CanBeNull=false)]
		[DebuggerNonUserCode()]
		public int AdReSaPsC
		{
			get
			{
				return this._adReSaPsC;
			}
			set
			{
				if ((_adReSaPsC != value))
				{
					this.OnAdReSaPsCChanging(value);
					this.SendPropertyChanging();
					this._adReSaPsC = value;
					this.SendPropertyChanged("AdReSaPsC");
					this.OnAdReSaPsCChanged();
				}
			}
		}
		
		[Column(Storage="_adReSaUlIce", Name="adresa_ulice", DbType="varchar(37)", AutoSync=AutoSync.Never, CanBeNull=false)]
		[DebuggerNonUserCode()]
		public string AdReSaUlIce
		{
			get
			{
				return this._adReSaUlIce;
			}
			set
			{
				if (((_adReSaUlIce == value) 
							== false))
				{
					this.OnAdReSaUlIceChanging(value);
					this.SendPropertyChanging();
					this._adReSaUlIce = value;
					this.SendPropertyChanged("AdReSaUlIce");
					this.OnAdReSaUlIceChanged();
				}
			}
		}
		
		[Column(Storage="_autosKolaID", Name="autoskola_id", DbType="int unsigned", AutoSync=AutoSync.Never, CanBeNull=false)]
		[DebuggerNonUserCode()]
		public uint AutosKolaID
		{
			get
			{
				return this._autosKolaID;
			}
			set
			{
				if ((_autosKolaID != value))
				{
					this.OnAutosKolaIDChanging(value);
					this.SendPropertyChanging();
					this._autosKolaID = value;
					this.SendPropertyChanged("AutosKolaID");
					this.OnAutosKolaIDChanged();
				}
			}
		}
		
		[Column(Storage="_jmEnO", Name="jmeno", DbType="varchar(30)", AutoSync=AutoSync.Never, CanBeNull=false)]
		[DebuggerNonUserCode()]
		public string JMenO
		{
			get
			{
				return this._jmEnO;
			}
			set
			{
				if (((_jmEnO == value) 
							== false))
				{
					this.OnJMenOChanging(value);
					this.SendPropertyChanging();
					this._jmEnO = value;
					this.SendPropertyChanged("JMenO");
					this.OnJMenOChanged();
				}
			}
		}
		
		[Column(Storage="_osObNiCiSLo", Name="osobni_cislo", DbType="varchar(7)", AutoSync=AutoSync.Never, CanBeNull=false)]
		[DebuggerNonUserCode()]
		public string OSobNiCIsLo
		{
			get
			{
				return this._osObNiCiSLo;
			}
			set
			{
				if (((_osObNiCiSLo == value) 
							== false))
				{
					this.OnOSobNiCIsLoChanging(value);
					this.SendPropertyChanging();
					this._osObNiCiSLo = value;
					this.SendPropertyChanged("OSobNiCIsLo");
					this.OnOSobNiCIsLoChanged();
				}
			}
		}
		
		[Column(Storage="_prIJmEnI", Name="prijmeni", DbType="varchar(30)", AutoSync=AutoSync.Never, CanBeNull=false)]
		[DebuggerNonUserCode()]
		public string PRiJMenI
		{
			get
			{
				return this._prIJmEnI;
			}
			set
			{
				if (((_prIJmEnI == value) 
							== false))
				{
					this.OnPRiJMenIChanging(value);
					this.SendPropertyChanging();
					this._prIJmEnI = value;
					this.SendPropertyChanged("PRiJMenI");
					this.OnPRiJMenIChanged();
				}
			}
		}
		
		[Column(Storage="_ucIteLid", Name="ucitel_id", DbType="int unsigned", IsPrimaryKey=true, IsDbGenerated=true, AutoSync=AutoSync.Never, CanBeNull=false)]
		[DebuggerNonUserCode()]
		public uint UCiteLID
		{
			get
			{
				return this._ucIteLid;
			}
			set
			{
				if ((_ucIteLid != value))
				{
					this.OnUCiteLIDChanging(value);
					this.SendPropertyChanging();
					this._ucIteLid = value;
					this.SendPropertyChanged("UCiteLID");
					this.OnUCiteLIDChanged();
				}
			}
		}
		
		#region Children
		[Association(Storage="_jizdY", OtherKey="UCiteLID", ThisKey="UCiteLID", Name="JIZDY_UCITELE_FK")]
		[DebuggerNonUserCode()]
		public EntitySet<JIZDy> JIZDy
		{
			get
			{
				return this._jizdY;
			}
			set
			{
				this._jizdY = value;
			}
		}
		
		[Association(Storage="_oprAvNeNiUcIteLu", OtherKey="UCiteLID", ThisKey="UCiteLID", Name="OPRAVNENI_UCITELU_UCITELE_FK")]
		[DebuggerNonUserCode()]
		public EntitySet<OprAvNeNiUCiteLu> OprAvNeNiUCiteLu
		{
			get
			{
				return this._oprAvNeNiUcIteLu;
			}
			set
			{
				this._oprAvNeNiUcIteLu = value;
			}
		}
		
		[Association(Storage="_teOrIe", OtherKey="UCiteLID", ThisKey="UCiteLID", Name="TEORIE_UCITELE_FK")]
		[DebuggerNonUserCode()]
		public EntitySet<TeOrIe> TeOrIe
		{
			get
			{
				return this._teOrIe;
			}
			set
			{
				this._teOrIe = value;
			}
		}
		
		[Association(Storage="_doKuMenTyucIteLu", OtherKey="UCiteLID", ThisKey="UCiteLID", Name="UCITELE_LEKARSKE_PROHLIDKY_FK")]
		[DebuggerNonUserCode()]
		public EntitySet<DoKuMenTYUCiteLu> DoKuMenTYUCiteLu
		{
			get
			{
				return this._doKuMenTyucIteLu;
			}
			set
			{
				this._doKuMenTyucIteLu = value;
			}
		}
		#endregion
		
		#region Parents
		[Association(Storage="_autosKoLy", OtherKey="AutosKolaID", ThisKey="AutosKolaID", Name="UCITELE_AUTOSKOLY_FK", IsForeignKey=true)]
		[DebuggerNonUserCode()]
		public AutosKoLY AutosKoLY
		{
			get
			{
				return this._autosKoLy.Entity;
			}
			set
			{
				if (((this._autosKoLy.Entity == value) 
							== false))
				{
					if ((this._autosKoLy.Entity != null))
					{
						AutosKoLY previousAutosKoLY = this._autosKoLy.Entity;
						this._autosKoLy.Entity = null;
						previousAutosKoLY.UCiteLE.Remove(this);
					}
					this._autosKoLy.Entity = value;
					if ((value != null))
					{
						value.UCiteLE.Add(this);
						_autosKolaID = value.AutosKolaID;
					}
					else
					{
						_autosKolaID = default(uint);
					}
				}
			}
		}
		#endregion
		
		public event System.ComponentModel.PropertyChangingEventHandler PropertyChanging;
		
		public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
		
		protected virtual void SendPropertyChanging()
		{
			System.ComponentModel.PropertyChangingEventHandler h = this.PropertyChanging;
			if ((h != null))
			{
				h(this, emptyChangingEventArgs);
			}
		}
		
		protected virtual void SendPropertyChanged(string propertyName)
		{
			System.ComponentModel.PropertyChangedEventHandler h = this.PropertyChanged;
			if ((h != null))
			{
				h(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
			}
		}
		
		#region Attachment handlers
		private void JIZDy_Attach(JIZDy entity)
		{
			this.SendPropertyChanging();
			entity.UCiteLE = this;
		}
		
		private void JIZDy_Detach(JIZDy entity)
		{
			this.SendPropertyChanging();
			entity.UCiteLE = null;
		}
		
		private void OprAvNeNiUCiteLu_Attach(OprAvNeNiUCiteLu entity)
		{
			this.SendPropertyChanging();
			entity.UCiteLE = this;
		}
		
		private void OprAvNeNiUCiteLu_Detach(OprAvNeNiUCiteLu entity)
		{
			this.SendPropertyChanging();
			entity.UCiteLE = null;
		}
		
		private void TeOrIe_Attach(TeOrIe entity)
		{
			this.SendPropertyChanging();
			entity.UCiteLE = this;
		}
		
		private void TeOrIe_Detach(TeOrIe entity)
		{
			this.SendPropertyChanging();
			entity.UCiteLE = null;
		}
		
		private void DoKuMenTYUCiteLu_Attach(DoKuMenTYUCiteLu entity)
		{
			this.SendPropertyChanging();
			entity.UCiteLE = this;
		}
		
		private void DoKuMenTYUCiteLu_Detach(DoKuMenTYUCiteLu entity)
		{
			this.SendPropertyChanging();
			entity.UCiteLE = null;
		}
		#endregion
	}
	
	[Table(Name="driving_scool.UZIVATELE")]
	public partial class UziVatELE : System.ComponentModel.INotifyPropertyChanging, System.ComponentModel.INotifyPropertyChanged
	{
		
		private static System.ComponentModel.PropertyChangingEventArgs emptyChangingEventArgs = new System.ComponentModel.PropertyChangingEventArgs("");
		
		private string _heSlO;
		
		private string _role;
		
		private uint _uziVatElid;
		
		private string _uziVatElsKEjmEnO;
		
		private EntitySet<AutosKoLY> _autosKoLy;
		
		private EntityRef<Role> _roleRole = new EntityRef<Role>();
		
		#region Extensibility Method Declarations
		partial void OnCreated();
		
		partial void OnHeSlOChanged();
		
		partial void OnHeSlOChanging(string value);
		
		partial void OnRoleChanged();
		
		partial void OnRoleChanging(string value);
		
		partial void OnUziVatELIDChanged();
		
		partial void OnUziVatELIDChanging(uint value);
		
		partial void OnUziVatELSkEJMenOChanged();
		
		partial void OnUziVatELSkEJMenOChanging(string value);
		#endregion
		
		
		public UziVatELE()
		{
			_autosKoLy = new EntitySet<AutosKoLY>(new Action<AutosKoLY>(this.AutosKoLY_Attach), new Action<AutosKoLY>(this.AutosKoLY_Detach));
			this.OnCreated();
		}
		
		[Column(Storage="_heSlO", Name="heslo", DbType="varchar(30)", AutoSync=AutoSync.Never, CanBeNull=false)]
		[DebuggerNonUserCode()]
		public string HeSlO
		{
			get
			{
				return this._heSlO;
			}
			set
			{
				if (((_heSlO == value) 
							== false))
				{
					this.OnHeSlOChanging(value);
					this.SendPropertyChanging();
					this._heSlO = value;
					this.SendPropertyChanged("HeSlO");
					this.OnHeSlOChanged();
				}
			}
		}
		
		[Column(Storage="_role", Name="role", DbType="varchar(18)", AutoSync=AutoSync.Never, CanBeNull=false)]
		[DebuggerNonUserCode()]
		public string Role
		{
			get
			{
				return this._role;
			}
			set
			{
				if (((_role == value) 
							== false))
				{
					this.OnRoleChanging(value);
					this.SendPropertyChanging();
					this._role = value;
					this.SendPropertyChanged("Role");
					this.OnRoleChanged();
				}
			}
		}
		
		[Column(Storage="_uziVatElid", Name="uzivatel_id", DbType="int unsigned", IsPrimaryKey=true, IsDbGenerated=true, AutoSync=AutoSync.Never, CanBeNull=false)]
		[DebuggerNonUserCode()]
		public uint UziVatELID
		{
			get
			{
				return this._uziVatElid;
			}
			set
			{
				if ((_uziVatElid != value))
				{
					this.OnUziVatELIDChanging(value);
					this.SendPropertyChanging();
					this._uziVatElid = value;
					this.SendPropertyChanged("UziVatELID");
					this.OnUziVatELIDChanged();
				}
			}
		}
		
		[Column(Storage="_uziVatElsKEjmEnO", Name="uzivatelske_jmeno", DbType="varchar(15)", AutoSync=AutoSync.Never, CanBeNull=false)]
		[DebuggerNonUserCode()]
		public string UziVatELSkEJMenO
		{
			get
			{
				return this._uziVatElsKEjmEnO;
			}
			set
			{
				if (((_uziVatElsKEjmEnO == value) 
							== false))
				{
					this.OnUziVatELSkEJMenOChanging(value);
					this.SendPropertyChanging();
					this._uziVatElsKEjmEnO = value;
					this.SendPropertyChanged("UziVatELSkEJMenO");
					this.OnUziVatELSkEJMenOChanged();
				}
			}
		}
		
		#region Children
		[Association(Storage="_autosKoLy", OtherKey="UziVatELID", ThisKey="UziVatELID", Name="AUTOSKOLA_UZIVATELE_FK")]
		[DebuggerNonUserCode()]
		public EntitySet<AutosKoLY> AutosKoLY
		{
			get
			{
				return this._autosKoLy;
			}
			set
			{
				this._autosKoLy = value;
			}
		}
		#endregion
		
		#region Parents
		[Association(Storage="_roleRole", OtherKey="NAzEV", ThisKey="Role", Name="UZIVATELE_ROLE_FK", IsForeignKey=true)]
		[DebuggerNonUserCode()]
		public Role RoleRole
		{
			get
			{
				return this._roleRole.Entity;
			}
			set
			{
				if (((this._roleRole.Entity == value) 
							== false))
				{
					if ((this._roleRole.Entity != null))
					{
						Role previousRole = this._roleRole.Entity;
						this._roleRole.Entity = null;
						previousRole.UziVatELE.Remove(this);
					}
					this._roleRole.Entity = value;
					if ((value != null))
					{
						value.UziVatELE.Add(this);
						_role = value.NAzEV;
					}
					else
					{
						_role = default(string);
					}
				}
			}
		}
		#endregion
		
		public event System.ComponentModel.PropertyChangingEventHandler PropertyChanging;
		
		public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
		
		protected virtual void SendPropertyChanging()
		{
			System.ComponentModel.PropertyChangingEventHandler h = this.PropertyChanging;
			if ((h != null))
			{
				h(this, emptyChangingEventArgs);
			}
		}
		
		protected virtual void SendPropertyChanged(string propertyName)
		{
			System.ComponentModel.PropertyChangedEventHandler h = this.PropertyChanged;
			if ((h != null))
			{
				h(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
			}
		}
		
		#region Attachment handlers
		private void AutosKoLY_Attach(AutosKoLY entity)
		{
			this.SendPropertyChanging();
			entity.UziVatELE = this;
		}
		
		private void AutosKoLY_Detach(AutosKoLY entity)
		{
			this.SendPropertyChanging();
			entity.UziVatELE = null;
		}
		#endregion
	}
	
	[Table(Name="driving_scool.VOZIDLA")]
	public partial class VOzIDLa : System.ComponentModel.INotifyPropertyChanging, System.ComponentModel.INotifyPropertyChanged
	{
		
		private static System.ComponentModel.PropertyChangingEventArgs emptyChangingEventArgs = new System.ComponentModel.PropertyChangingEventArgs("");
		
		private uint _autosKolaID;
		
		private string _model;
		
		private uint _pocAtEcNiStaVKm;
		
		private System.DateTime _poSLedNiStk;
		
		private uint _roKVyrObY;
		
		private string _spZ;
		
		private string _viN;
		
		private uint _voZIdlOID;
		
		private string _voZIdlOTyp;
		
		private string _znAcKa;
		
		private EntitySet<JIZDy> _jizdY;
		
		private EntitySet<DoKuMenTYVOzIDel> _doKuMenTyvoZIDEl;
		
		private EntityRef<AutosKoLY> _autosKoLy = new EntityRef<AutosKoLY>();
		
		private EntityRef<TYPYVOzIDel> _typyvoZIDEl = new EntityRef<TYPYVOzIDel>();
		
		#region Extensibility Method Declarations
		partial void OnCreated();
		
		partial void OnAutosKolaIDChanged();
		
		partial void OnAutosKolaIDChanging(uint value);
		
		partial void OnModelChanged();
		
		partial void OnModelChanging(string value);
		
		partial void OnPOCatEcNiSTAvKMChanged();
		
		partial void OnPOCatEcNiSTAvKMChanging(uint value);
		
		partial void OnPOsLedNiSTKChanged();
		
		partial void OnPOsLedNiSTKChanging(System.DateTime value);
		
		partial void OnROkVYRobYChanged();
		
		partial void OnROkVYRobYChanging(uint value);
		
		partial void OnSpZChanged();
		
		partial void OnSpZChanging(string value);
		
		partial void OnVInChanged();
		
		partial void OnVInChanging(string value);
		
		partial void OnVOzIDLoIDChanged();
		
		partial void OnVOzIDLoIDChanging(uint value);
		
		partial void OnVOzIDLoTYPChanged();
		
		partial void OnVOzIDLoTYPChanging(string value);
		
		partial void OnZnAcKAChanged();
		
		partial void OnZnAcKAChanging(string value);
		#endregion
		
		
		public VOzIDLa()
		{
			_jizdY = new EntitySet<JIZDy>(new Action<JIZDy>(this.JIZDy_Attach), new Action<JIZDy>(this.JIZDy_Detach));
			_doKuMenTyvoZIDEl = new EntitySet<DoKuMenTYVOzIDel>(new Action<DoKuMenTYVOzIDel>(this.DoKuMenTYVOzIDel_Attach), new Action<DoKuMenTYVOzIDel>(this.DoKuMenTYVOzIDel_Detach));
			this.OnCreated();
		}
		
		[Column(Storage="_autosKolaID", Name="autoskola_id", DbType="int unsigned", AutoSync=AutoSync.Never, CanBeNull=false)]
		[DebuggerNonUserCode()]
		public uint AutosKolaID
		{
			get
			{
				return this._autosKolaID;
			}
			set
			{
				if ((_autosKolaID != value))
				{
					this.OnAutosKolaIDChanging(value);
					this.SendPropertyChanging();
					this._autosKolaID = value;
					this.SendPropertyChanged("AutosKolaID");
					this.OnAutosKolaIDChanged();
				}
			}
		}
		
		[Column(Storage="_model", Name="model", DbType="varchar(15)", AutoSync=AutoSync.Never, CanBeNull=false)]
		[DebuggerNonUserCode()]
		public string Model
		{
			get
			{
				return this._model;
			}
			set
			{
				if (((_model == value) 
							== false))
				{
					this.OnModelChanging(value);
					this.SendPropertyChanging();
					this._model = value;
					this.SendPropertyChanged("Model");
					this.OnModelChanged();
				}
			}
		}
		
		[Column(Storage="_pocAtEcNiStaVKm", Name="pocatecni_stav_km", DbType="int unsigned", AutoSync=AutoSync.Never, CanBeNull=false)]
		[DebuggerNonUserCode()]
		public uint POCatEcNiSTAvKM
		{
			get
			{
				return this._pocAtEcNiStaVKm;
			}
			set
			{
				if ((_pocAtEcNiStaVKm != value))
				{
					this.OnPOCatEcNiSTAvKMChanging(value);
					this.SendPropertyChanging();
					this._pocAtEcNiStaVKm = value;
					this.SendPropertyChanged("POCatEcNiSTAvKM");
					this.OnPOCatEcNiSTAvKMChanged();
				}
			}
		}
		
		[Column(Storage="_poSLedNiStk", Name="posledni_stk", DbType="date", AutoSync=AutoSync.Never, CanBeNull=false)]
		[DebuggerNonUserCode()]
		public System.DateTime POsLedNiSTK
		{
			get
			{
				return this._poSLedNiStk;
			}
			set
			{
				if ((_poSLedNiStk != value))
				{
					this.OnPOsLedNiSTKChanging(value);
					this.SendPropertyChanging();
					this._poSLedNiStk = value;
					this.SendPropertyChanged("POsLedNiSTK");
					this.OnPOsLedNiSTKChanged();
				}
			}
		}
		
		[Column(Storage="_roKVyrObY", Name="rok_vyroby", DbType="int unsigned", AutoSync=AutoSync.Never, CanBeNull=false)]
		[DebuggerNonUserCode()]
		public uint ROkVYRobY
		{
			get
			{
				return this._roKVyrObY;
			}
			set
			{
				if ((_roKVyrObY != value))
				{
					this.OnROkVYRobYChanging(value);
					this.SendPropertyChanging();
					this._roKVyrObY = value;
					this.SendPropertyChanged("ROkVYRobY");
					this.OnROkVYRobYChanged();
				}
			}
		}
		
		[Column(Storage="_spZ", Name="spz", DbType="varchar(7)", AutoSync=AutoSync.Never, CanBeNull=false)]
		[DebuggerNonUserCode()]
		public string SpZ
		{
			get
			{
				return this._spZ;
			}
			set
			{
				if (((_spZ == value) 
							== false))
				{
					this.OnSpZChanging(value);
					this.SendPropertyChanging();
					this._spZ = value;
					this.SendPropertyChanged("SpZ");
					this.OnSpZChanged();
				}
			}
		}
		
		[Column(Storage="_viN", Name="vin", DbType="varchar(17)", AutoSync=AutoSync.Never, CanBeNull=false)]
		[DebuggerNonUserCode()]
		public string VIn
		{
			get
			{
				return this._viN;
			}
			set
			{
				if (((_viN == value) 
							== false))
				{
					this.OnVInChanging(value);
					this.SendPropertyChanging();
					this._viN = value;
					this.SendPropertyChanged("VIn");
					this.OnVInChanged();
				}
			}
		}
		
		[Column(Storage="_voZIdlOID", Name="vozidlo_id", DbType="int unsigned", IsPrimaryKey=true, IsDbGenerated=true, AutoSync=AutoSync.Never, CanBeNull=false)]
		[DebuggerNonUserCode()]
		public uint VOzIDLoID
		{
			get
			{
				return this._voZIdlOID;
			}
			set
			{
				if ((_voZIdlOID != value))
				{
					this.OnVOzIDLoIDChanging(value);
					this.SendPropertyChanging();
					this._voZIdlOID = value;
					this.SendPropertyChanged("VOzIDLoID");
					this.OnVOzIDLoIDChanged();
				}
			}
		}
		
		[Column(Storage="_voZIdlOTyp", Name="vozidlo_typ", DbType="varchar(10)", AutoSync=AutoSync.Never, CanBeNull=false)]
		[DebuggerNonUserCode()]
		public string VOzIDLoTYP
		{
			get
			{
				return this._voZIdlOTyp;
			}
			set
			{
				if (((_voZIdlOTyp == value) 
							== false))
				{
					this.OnVOzIDLoTYPChanging(value);
					this.SendPropertyChanging();
					this._voZIdlOTyp = value;
					this.SendPropertyChanged("VOzIDLoTYP");
					this.OnVOzIDLoTYPChanged();
				}
			}
		}
		
		[Column(Storage="_znAcKa", Name="znacka", DbType="varchar(15)", AutoSync=AutoSync.Never, CanBeNull=false)]
		[DebuggerNonUserCode()]
		public string ZnAcKA
		{
			get
			{
				return this._znAcKa;
			}
			set
			{
				if (((_znAcKa == value) 
							== false))
				{
					this.OnZnAcKAChanging(value);
					this.SendPropertyChanging();
					this._znAcKa = value;
					this.SendPropertyChanged("ZnAcKA");
					this.OnZnAcKAChanged();
				}
			}
		}
		
		#region Children
		[Association(Storage="_jizdY", OtherKey="VOzIDLoID", ThisKey="VOzIDLoID", Name="JIZDY_VOZIDLA_FK")]
		[DebuggerNonUserCode()]
		public EntitySet<JIZDy> JIZDy
		{
			get
			{
				return this._jizdY;
			}
			set
			{
				this._jizdY = value;
			}
		}
		
		[Association(Storage="_doKuMenTyvoZIDEl", OtherKey="VOzIDLoID", ThisKey="VOzIDLoID", Name="STK_VOZIDLA_FK")]
		[DebuggerNonUserCode()]
		public EntitySet<DoKuMenTYVOzIDel> DoKuMenTYVOzIDel
		{
			get
			{
				return this._doKuMenTyvoZIDEl;
			}
			set
			{
				this._doKuMenTyvoZIDEl = value;
			}
		}
		#endregion
		
		#region Parents
		[Association(Storage="_autosKoLy", OtherKey="AutosKolaID", ThisKey="AutosKolaID", Name="VOZIDLA_AUTOSKOLY_FK", IsForeignKey=true)]
		[DebuggerNonUserCode()]
		public AutosKoLY AutosKoLY
		{
			get
			{
				return this._autosKoLy.Entity;
			}
			set
			{
				if (((this._autosKoLy.Entity == value) 
							== false))
				{
					if ((this._autosKoLy.Entity != null))
					{
						AutosKoLY previousAutosKoLY = this._autosKoLy.Entity;
						this._autosKoLy.Entity = null;
						previousAutosKoLY.VOzIDLa.Remove(this);
					}
					this._autosKoLy.Entity = value;
					if ((value != null))
					{
						value.VOzIDLa.Add(this);
						_autosKolaID = value.AutosKolaID;
					}
					else
					{
						_autosKolaID = default(uint);
					}
				}
			}
		}
		
		[Association(Storage="_typyvoZIDEl", OtherKey="NAzEV", ThisKey="VOzIDLoTYP", Name="VOZIDLA_TYPY_VOZIDEL_FK", IsForeignKey=true)]
		[DebuggerNonUserCode()]
		public TYPYVOzIDel TYPYVOzIDel
		{
			get
			{
				return this._typyvoZIDEl.Entity;
			}
			set
			{
				if (((this._typyvoZIDEl.Entity == value) 
							== false))
				{
					if ((this._typyvoZIDEl.Entity != null))
					{
						TYPYVOzIDel previousTYPYVOzIDel = this._typyvoZIDEl.Entity;
						this._typyvoZIDEl.Entity = null;
						previousTYPYVOzIDel.VOzIDLa.Remove(this);
					}
					this._typyvoZIDEl.Entity = value;
					if ((value != null))
					{
						value.VOzIDLa.Add(this);
						_voZIdlOTyp = value.NAzEV;
					}
					else
					{
						_voZIdlOTyp = default(string);
					}
				}
			}
		}
		#endregion
		
		public event System.ComponentModel.PropertyChangingEventHandler PropertyChanging;
		
		public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
		
		protected virtual void SendPropertyChanging()
		{
			System.ComponentModel.PropertyChangingEventHandler h = this.PropertyChanging;
			if ((h != null))
			{
				h(this, emptyChangingEventArgs);
			}
		}
		
		protected virtual void SendPropertyChanged(string propertyName)
		{
			System.ComponentModel.PropertyChangedEventHandler h = this.PropertyChanged;
			if ((h != null))
			{
				h(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
			}
		}
		
		#region Attachment handlers
		private void JIZDy_Attach(JIZDy entity)
		{
			this.SendPropertyChanging();
			entity.VOzIDLa = this;
		}
		
		private void JIZDy_Detach(JIZDy entity)
		{
			this.SendPropertyChanging();
			entity.VOzIDLa = null;
		}
		
		private void DoKuMenTYVOzIDel_Attach(DoKuMenTYVOzIDel entity)
		{
			this.SendPropertyChanging();
			entity.VOzIDLa = this;
		}
		
		private void DoKuMenTYVOzIDel_Detach(DoKuMenTYVOzIDel entity)
		{
			this.SendPropertyChanging();
			entity.VOzIDLa = null;
		}
		#endregion
	}
}
